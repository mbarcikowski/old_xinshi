/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.extender.internal;

import fr.matoeil.xinshi.api.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.google.common.base.Preconditions.checkState;

public class Registry
    implements RouterListener
{

    private static final Logger LOGGER = LoggerFactory.getLogger( Registry.class );

    private static final String DIFFERENT_ROUTERS_MESSAGE =
        "Unavailable router [{}] is not equal with prior available router [{}]";

    private static final String ERROR_DURING_REGISTRATION_MESSAGE =
        "Registration skipped for [%s] due to error during registration";

    private final List<RouteRegistration> routeRegistrations_;

    private final ReadWriteLock routeRegistrationsLock_;

    private final ReadWriteLock routerLock_;

    private Router router_;

    public Registry()
    {
        routeRegistrations_ = new ArrayList<>();
        routerLock_ = new ReentrantReadWriteLock();
        routeRegistrationsLock_ = new ReentrantReadWriteLock();
    }

    public void addRouteRegistration( RouteRegistration aRouteRegistration )
    {
        routeRegistrationsLock_.writeLock().lock();
        try
        {
            routeRegistrations_.add( aRouteRegistration );
        }
        finally
        {
            routeRegistrationsLock_.writeLock().unlock();
        }
        routerLock_.readLock().lock();
        try
        {
            registerRouteRegistration( aRouteRegistration );
        }
        finally
        {
            routerLock_.readLock().unlock();
        }
    }

    public void removeRouteRegistration( RouteRegistration aRouteRegistration )
    {
        routeRegistrationsLock_.writeLock().lock();
        try
        {
            routeRegistrations_.remove( aRouteRegistration );
        }
        finally
        {
            routeRegistrationsLock_.writeLock().unlock();
        }
        routerLock_.readLock().lock();
        try
        {
            unregisterRouteRegistration( aRouteRegistration );
        }
        finally
        {
            routerLock_.readLock().unlock();
        }
    }

    @Override
    public void available( Router aRouter )
    {
        routerLock_.writeLock().lock();
        try
        {
            if ( router_ != null )
            {
                unavailable( router_ );
            }
            router_ = aRouter;
            registerRouteRegistrations();
        }
        finally
        {
            routerLock_.writeLock().unlock();
        }
    }

    @Override
    public void unavailable( Router aRouter )
    {
        routerLock_.writeLock().lock();
        try
        {
            checkState( aRouter == router_, DIFFERENT_ROUTERS_MESSAGE, aRouter, router_ );
            if ( aRouter != router_ )
            {
                throw new IllegalStateException(

                );
            }
            unregisterRouteRegistrations();
            router_ = null;
        }
        finally
        {
            routerLock_.writeLock().unlock();
        }
    }

    private void registerRouteRegistration( final RouteRegistration aRouteRegistration )
    {
        try
        {
            if ( router_ != null )
            {
                aRouteRegistration.register( router_ );
            }
        }
        catch ( Exception e )
        {
            LOGGER.error( String.format( ERROR_DURING_REGISTRATION_MESSAGE, aRouteRegistration ), e );
        }
    }

    private void unregisterRouteRegistration( final RouteRegistration aRouteRegistration )
    {
        if ( router_ != null )
        {
            aRouteRegistration.unregister( router_ );
        }
    }

    private void registerRouteRegistrations()
    {
        routeRegistrationsLock_.readLock().lock();
        try
        {
            if ( router_ != null )
            {
                for ( RouteRegistration routeRegistration : routeRegistrations_ )
                {
                    registerRouteRegistration( routeRegistration );
                }
            }
        }
        finally
        {
            routeRegistrationsLock_.readLock().unlock();
        }
    }

    private void unregisterRouteRegistrations()
    {
        routeRegistrationsLock_.readLock().lock();
        try
        {
            if ( router_ != null )
            {
                for ( RouteRegistration routeRegistration : routeRegistrations_ )
                {
                    unregisterRouteRegistration( routeRegistration );
                }
            }
        }
        finally
        {
            routeRegistrationsLock_.readLock().unlock();
        }
    }


}
