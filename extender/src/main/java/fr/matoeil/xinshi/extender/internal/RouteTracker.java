/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.extender.internal;

import fr.matoeil.xinshi.spi.Route;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class RouteTracker
    extends ServiceTracker
{

    private final static Logger LOGGER = LoggerFactory.getLogger( RouteTracker.class );

    private final static String FILTER =
        "(&(" + org.osgi.framework.Constants.OBJECTCLASS + "=" + Route.class.getName() + ")(" + Route.ROUTE_SUBDOMAIN
            + "=*))";

    private static final String ROUTE_AVAILABLE_MESSAGE = "Route available {}";

    private static final String ROUTE_REMOVED_MESSAGE = "Route removed {}";

    private static final String INVALID_ROUTE_SUBDOMAIN_PROPERTY_MESSAGE =
        "Registered route [{}] did not contain a valid '" + Route.ROUTE_SUBDOMAIN + "' property.";

    private final Registry registry_;

    @Inject
    public RouteTracker( final BundleContext aBundleContext, Registry aRegistry )
    {
        super( aBundleContext, createFilter( aBundleContext ), null );
        registry_ = aRegistry;
    }

    private static Filter createFilter( final BundleContext bundleContext )
    {
        try
        {
            return bundleContext.createFilter( FILTER );
        }
        catch ( InvalidSyntaxException e )
        {
            throw new IllegalArgumentException( e.getMessage(), e );
        }
    }

    @Override
    public Object addingService( final ServiceReference serviceReference )
    {
        LOGGER.debug( ROUTE_AVAILABLE_MESSAGE, serviceReference );
        Route route = (Route) super.addingService( serviceReference );
        RouteRegistration routeRegistration = createRouteRegistration( serviceReference, route );
        if ( routeRegistration != null )
        {
            registry_.addRouteRegistration( routeRegistration );
            return routeRegistration;
        }
        else
        {
            super.remove( serviceReference );
            return null;
        }
    }

    @Override
    public void removedService( final ServiceReference serviceReference, final Object service )
    {
        LOGGER.debug( ROUTE_REMOVED_MESSAGE, serviceReference );
        super.removedService( serviceReference, service );
        final RouteRegistration routeRegistration = (RouteRegistration) service;
        registry_.removeRouteRegistration( routeRegistration );
    }

    private RouteRegistration createRouteRegistration( final ServiceReference serviceReference, final Route aRoute )
    {
        final Object subDomain = serviceReference.getProperty( Route.ROUTE_SUBDOMAIN );
        if ( !( subDomain instanceof String ) )
        {
            LOGGER.warn( INVALID_ROUTE_SUBDOMAIN_PROPERTY_MESSAGE, aRoute );
            return null;
        }
        return new RouteRegistration( aRoute, (String) subDomain );
    }

}
