/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.extender.internal.activation;

import com.google.inject.AbstractModule;
import fr.matoeil.xinshi.extender.internal.Registry;
import fr.matoeil.xinshi.extender.internal.RouteTracker;
import fr.matoeil.xinshi.extender.internal.RouterListener;
import fr.matoeil.xinshi.extender.internal.RouterTracker;

public class BundleModule
    extends AbstractModule
{
    @Override
    protected void configure()
    {
        configureRegistry();
        configureTrackers();
    }

    private void configureRegistry()
    {
        bind( Registry.class ).asEagerSingleton();
        bind( RouterListener.class ).to( Registry.class ).asEagerSingleton();
    }

    private void configureTrackers()
    {
        bind( RouterTracker.class ).asEagerSingleton();
        bind( RouteTracker.class ).asEagerSingleton();
    }
}
