/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.extender.internal;

import fr.matoeil.xinshi.api.Router;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RouterTracker
    extends ServiceTracker
{

    private final static Logger LOGGER = LoggerFactory.getLogger( RouterTracker.class );

    private static final String ROUTER_AVAILABLE_MESSAGE = "Router available {}";

    private static final String ROUTER_REMOVED_MESSAGE = "Router removed {}";

    private final RouterListener routerListener_;

    private Router router_;

    private Lock lock_;

    @Inject
    public RouterTracker( final BundleContext aBundleContext, RouterListener aRouterListener )
    {
        super( aBundleContext, Router.class.getName(), null );
        routerListener_ = aRouterListener;
        lock_ = new ReentrantLock();
    }

    @Override
    public Object addingService( final ServiceReference serviceReference )
    {
        LOGGER.debug( ROUTER_AVAILABLE_MESSAGE, serviceReference );
        lock_.lock();
        Router router = null;
        try
        {
            if ( router_ != null )
            {
                return super.addingService( serviceReference );
            }
            router_ = (Router) super.addingService( serviceReference );
            router = router_;
        }
        finally
        {
            lock_.unlock();
        }

        routerListener_.available( router );
        return router;
    }

    @Override
    public void removedService( final ServiceReference serviceReference, final Object service )
    {
        LOGGER.debug( ROUTER_REMOVED_MESSAGE, serviceReference );
        lock_.lock();
        Router router = null;
        try
        {
            super.removedService( serviceReference, service );
            if ( router_ != service )
            {
                return;
            }
            router = router_;
            router_ = null;
        }
        finally
        {
            lock_.unlock();
        }
        routerListener_.unavailable( router );
    }


}
