/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableArrayAttrMap;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNamedNodeMap;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableText;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public class Thread
    extends ImmutableChildElement
    implements Element
{
    private static final String LOCAL_NAME = "thread";

    private static final int COMMON_ATTRIBUTES_COUNT = 1;

    private static final String PARENT = "parent";

    private final String parentId_;

    private final String id_;

    Thread( final ImmutableNode aParentNode, final int aElementIndex, final String aNamespaceURI,
            @Nullable final String aParentId, final String aId )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, LOCAL_NAME );
        parentId_ = aParentId;
        id_ = aId;
        setImmutableAttributes( generateAttributes() );
        setImmutableChildNodes( generateChildNodes() );
    }

    @Nullable
    public String getParentId()
    {
        return parentId_;
    }

    public String getId()
    {
        return id_;
    }

    @Override
    public String getTextContent()
    {
        return id_;
    }

    @Override
    protected final ImmutableNamedNodeMap generateAttributes()
    {
        List<ImmutableAttribute> attributes = new ArrayList<>( COMMON_ATTRIBUTES_COUNT );
        generateAttribute( attributes, PARENT, parentId_ );
        return new ImmutableArrayAttrMap( attributes.toArray( new Attr[attributes.size()] ) );
    }

    private void generateAttribute( List<ImmutableAttribute> aAttributes, String aLocalName, Object aValue )
    {
        if ( aValue != null )
        {
            aAttributes.add( new ImmutableAttribute( this, null, null, aLocalName, aValue.toString() ) );
        }
    }

    @Override
    protected final ImmutableNodeList generateChildNodes()
    {
        ImmutableText text = new ImmutableText( this, id_ );
        return new ImmutableOneNodeList( text );
    }

    //TODO check nmtoken for parentId_ & id_, @see http://www.w3.org/TR/REC-xml/#NT-Nmtoken

    public static final class Builder
        extends ImmutableChildElement.Builder<Thread>
    {

        private String parentId_;

        private String id_;

        @Override
        public Builder isChildOf( ImmutableNode aParentNode )
        {
            return (Builder) super.isChildOf( aParentNode );
        }

        @Override
        public Builder atIndex( int aElementIndex )
        {
            return (Builder) super.atIndex( aElementIndex );
        }

        @Override
        public Builder inNamespace( String aNamespaceURI )
        {
            return (Builder) super.inNamespace( aNamespaceURI );
        }

        public Builder hasParentThread( String aParentId )
        {
            checkArgument( !isNullOrEmpty( aParentId ), "must not be null or empty" );
            parentId_ = aParentId;
            return this;
        }

        public Builder identifiedBy( String aId )
        {
            checkArgument( !isNullOrEmpty( aId ), "must not be null or empty" );
            id_ = aId;
            return this;
        }

        @Override
        protected Thread newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex,
                                                   String aNamespaceURI )
        {
            checkState( !isNullOrEmpty( id_ ), "must not be null or empty" );
            return new Thread( aParentNode, aElementIndex, aNamespaceURI, parentId_, id_ );
        }

    }
}
