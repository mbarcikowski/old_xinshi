/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.address;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * @see <a href="http://xmpp.org/extensions/xep-0106.html">xep-0106</a>
 */
public class Xep0106
{

    private final static char SPACE = '\u0020';// (" ") [3]

    private final static char QUOTATION_MARK = '"'; // (")

    private final static char AMPERSAND = '&'; // (&)

    private final static char APOSTROPHE = '\''; // (')

    private final static char SOLIDUS = '/'; // (/)

    private final static char COLON = ':'; // (:)

    private final static char LESS_THAN = '<'; // (<)

    private final static char GREATER_THAN = '>'; // (>)

    private final static char COMMERCIAL_AT = '@'; // (@)

    private final static char REVERSE_SOLIDUS = '\\'; // (\)

    private final static String ESCAPED_SPACE = "\\20";// (" ") [3]

    private final static String ESCAPED_QUOTATION_MARK = "\\22"; // (")

    private final static String ESCAPED_AMPERSAND = "\\26"; // (&)

    private final static String ESCAPED_APOSTROPHE = "\\27"; // (')

    private final static String ESCAPED_SOLIDUS = "\\2f"; // (/)

    private final static String ESCAPED_COLON = "\\3a"; // (:)

    private final static String ESCAPED_LESS_THAN = "\\3c"; // (<)

    private final static String ESCAPED_GREATER_THAN = "\\3e"; // (>)

    private final static String ESCAPED_COMMERCIAL_AT = "\\40"; // (@)

    private final static String ESCAPED_REVERSE_SOLIDUS = "\\5c"; // (\)

    private static final char TWO = '2';

    private static final char THREE = '3';

    private static final char FOUR = '4';

    private static final char FIVE = '5';

    private static final char ZERO = '0';

    private static final char SIX = '6';

    private static final char SEVEN = '7';

    private static final char F = 'f';

    private static final char A = 'a';

    private static final char C = 'c';

    private static final char E = 'e';

    private static final String INVALID_LAST_CHARACTER_ERROR = "node part can't end with a space character";

    private static final String INVALID_FIRST_CHARACTER_ERROR = "node part can't start with a space character";

    public static String escapeNodePart( String aNodePart )
    {
        int length = aNodePart.length();
        if ( length == 0 )
        {
            return "";
        }
        else
        {
            char[] nodeChars = aNodePart.toCharArray();
            verifyFirstAndLastCharacter( nodeChars, length );
            final StringBuilder builder = new StringBuilder( length + 8 );
            for ( int index = 0; index < length; index++ )
            {
                final char currentChar = nodeChars[index];
                switch ( currentChar )
                {
                    case SPACE:
                    {
                        builder.append( ESCAPED_SPACE );
                        break;
                    }
                    case QUOTATION_MARK:
                    {
                        builder.append( ESCAPED_QUOTATION_MARK );
                        break;
                    }
                    case AMPERSAND:
                    {
                        builder.append( ESCAPED_AMPERSAND );
                        break;
                    }
                    case APOSTROPHE:
                    {
                        builder.append( ESCAPED_APOSTROPHE );
                        break;
                    }
                    case SOLIDUS:
                    {
                        builder.append( ESCAPED_SOLIDUS );
                        break;
                    }
                    case COLON:
                    {
                        builder.append( ESCAPED_COLON );
                        break;
                    }
                    case LESS_THAN:
                    {
                        builder.append( ESCAPED_LESS_THAN );
                        break;
                    }
                    case GREATER_THAN:
                    {
                        builder.append( ESCAPED_GREATER_THAN );
                        break;
                    }
                    case COMMERCIAL_AT:
                    {
                        builder.append( ESCAPED_COMMERCIAL_AT );
                        break;
                    }
                    case REVERSE_SOLIDUS:
                    {
                        if ( index + 2 < length )
                        {
                            final char firstChar = nodeChars[index + 1];
                            final char secondChar = nodeChars[index + 2];
                            switch ( firstChar )
                            {
                                case TWO:
                                {
                                    testSecondCharacterWith2BeforeForEscaping( builder, currentChar, secondChar );
                                    break;
                                }
                                case THREE:
                                {
                                    testSecondCharacterWith3BeforeForEscaping( builder, currentChar, secondChar );
                                    break;
                                }
                                case FOUR:
                                {
                                    testSecondCharacterWith4BeforeForEscaping( builder, currentChar, secondChar );
                                    break;
                                }
                                case FIVE:
                                {
                                    testSecondCharacterWith5BeforeForEscaping( builder, currentChar, secondChar );
                                    break;
                                }
                                default:
                                {
                                    builder.append( currentChar );
                                }
                            }
                        }
                        else
                        {
                            builder.append( currentChar );
                        }
                        break;
                    }
                    default:
                    {
                        builder.append( currentChar );
                    }
                }
            }
            return builder.toString();
        }
    }

    private static void verifyFirstAndLastCharacter( char[] aNodeChars, int aLength )
    {
        if ( aLength > 1 )
        {
            checkArgument( aNodeChars[aLength - 1] != SPACE, INVALID_LAST_CHARACTER_ERROR );
        }
        checkArgument( aNodeChars[0] != SPACE, INVALID_FIRST_CHARACTER_ERROR );
    }

    private static void testSecondCharacterWith2BeforeForEscaping( StringBuilder aBuilder, char aCurrentCharacter,
                                                                   char aSecondCharacter )
    {
        switch ( aSecondCharacter )
        {
            case ZERO:
            case TWO:
            case SIX:
            case SEVEN:
            case F:
            {
                aBuilder.append( ESCAPED_REVERSE_SOLIDUS );
                break;
            }

            default:
            {
                aBuilder.append( aCurrentCharacter );
            }
        }
    }

    private static void testSecondCharacterWith3BeforeForEscaping( StringBuilder aBuilder, char aCurrentCharacter,
                                                                   char aSecondCharacter )
    {
        switch ( aSecondCharacter )
        {
            case A:
            case C:
            case E:
            {
                aBuilder.append( ESCAPED_REVERSE_SOLIDUS );
                break;
            }
            default:
            {
                aBuilder.append( aCurrentCharacter );
            }
        }
    }

    private static void testSecondCharacterWith4BeforeForEscaping( StringBuilder aBuilder, char aCurrentCharacter,
                                                                   char aSecondCharacter )
    {
        switch ( aSecondCharacter )
        {
            case ZERO:
            {
                aBuilder.append( ESCAPED_REVERSE_SOLIDUS );
                break;
            }
            default:
            {
                aBuilder.append( aCurrentCharacter );
            }
        }
    }

    private static void testSecondCharacterWith5BeforeForEscaping( StringBuilder aBuilder, char aCurrentCharacter,
                                                                   char aSecondCharacter )
    {
        switch ( aSecondCharacter )
        {
            case C:
            {
                aBuilder.append( ESCAPED_REVERSE_SOLIDUS );
                break;
            }
            default:
            {
                aBuilder.append( aCurrentCharacter );
            }
        }
    }

    public static String unescapeNodePart( String aNodePart )
    {
        char[] nodeChars = aNodePart.toCharArray();
        int length = nodeChars.length;
        StringBuilder builder = new StringBuilder( length );
        for ( int index = 0; index < length; index++ )
        {
            char currentChar = nodeChars[index];
            if ( currentChar == REVERSE_SOLIDUS && index + 2 < length )
            {
                char firstChar = nodeChars[index + 1];
                char secondChar = nodeChars[index + 2];
                switch ( firstChar )
                {
                    case TWO:
                    {
                        index = testSecondCharacterWith2BeforeForUnescaping( builder, index, currentChar, secondChar );
                        break;
                    }
                    case THREE:
                    {
                        index = testSecondCharacterWith3BeforeForUnescaping( builder, index, currentChar, secondChar );
                        break;
                    }
                    case FOUR:
                    {
                        index = testSecondCharacterWith4BeforeForUnescaping( builder, index, currentChar, secondChar );
                        break;
                    }
                    case FIVE:
                    {
                        index = testSecondCharacterWith5BeforeForUnescaping( builder, index, currentChar, secondChar );
                        break;
                    }
                    default:
                    {
                        builder.append( currentChar );
                    }
                }
            }
            else
            {
                builder.append( currentChar );
            }
        }
        return builder.toString();
    }

    private static int testSecondCharacterWith5BeforeForUnescaping( StringBuilder aBuilder, int aIndex,
                                                                    char aCurrentChar, char aSecondChar )
    {
        switch ( aSecondChar )
        {
            case C:
            {
                aBuilder.append( REVERSE_SOLIDUS );
                aIndex += 2;
                break;
            }

            default:
            {
                aBuilder.append( aCurrentChar );
            }
        }
        return aIndex;
    }

    private static int testSecondCharacterWith4BeforeForUnescaping( StringBuilder aBuilder, int aIndex,
                                                                    char aCurrentChar, char aSecondChar )
    {
        switch ( aSecondChar )
        {
            case ZERO:
            {
                aBuilder.append( COMMERCIAL_AT );
                aIndex += 2;
                break;
            }

            default:
            {
                aBuilder.append( aCurrentChar );
            }
        }
        return aIndex;
    }

    private static int testSecondCharacterWith3BeforeForUnescaping( StringBuilder aBuilder, int aIndex,
                                                                    char aCurrentChar, char aSecondChar )
    {
        switch ( aSecondChar )
        {
            case A:
            {
                aBuilder.append( COLON );
                aIndex += 2;
                break;
            }
            case C:
            {
                aBuilder.append( LESS_THAN );
                aIndex += 2;
                break;
            }
            case E:
            {
                aBuilder.append( GREATER_THAN );
                aIndex += 2;
                break;
            }
            default:
            {
                aBuilder.append( aCurrentChar );
            }
        }
        return aIndex;
    }

    private static int testSecondCharacterWith2BeforeForUnescaping( StringBuilder aBuilder, int aIndex,
                                                                    char aCurrentChar, char aSecondChar )
    {
        switch ( aSecondChar )
        {
            case ZERO:
            {
                aBuilder.append( SPACE );
                aIndex += 2;
                break;
            }
            case TWO:
            {
                aBuilder.append( QUOTATION_MARK );
                aIndex += 2;
                break;
            }
            case SIX:
            {
                aBuilder.append( AMPERSAND );
                aIndex += 2;
                break;
            }
            case SEVEN:
            {
                aBuilder.append( APOSTROPHE );
                aIndex += 2;
                break;
            }

            case F:
            {
                aBuilder.append( SOLIDUS );
                aIndex += 2;
                break;
            }
            default:
            {
                aBuilder.append( aCurrentChar );
            }
        }
        return aIndex;
    }


}
