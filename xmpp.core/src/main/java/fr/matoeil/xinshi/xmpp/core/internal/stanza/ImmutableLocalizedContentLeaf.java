/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Attr;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public class ImmutableLocalizedContentLeaf
    extends ImmutableChildElement
{
    private static final int COMMON_ATTRIBUTES_COUNT = 1;

    private final String content_;
    //TODO lang

    public ImmutableLocalizedContentLeaf( final ImmutableNode aParentNode, final int aElementIndex,
                                          final String aNamespaceURI, final String aLocalName,
                                          @Nullable final String aContent )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, aLocalName );
        content_ = aContent;
        setImmutableAttributes( generateAttributes() );
        setImmutableChildNodes( generateChildNodes() );
    }

    @Nullable
    public final String getContent()
    {
        return content_;
    }

    @Override
    public final String getTextContent()
    {
        return content_;
    }

    @Override
    protected final ImmutableNamedNodeMap generateAttributes()
    {
        List<ImmutableAttribute> attributes = new ArrayList<>( COMMON_ATTRIBUTES_COUNT );
        //TODO lang
        /*if (language_ != null) {
            attributes.add(new ImmutableAttribute(this, XML_NS_URI, "xml", "lang", language_));
        } */

        return new ImmutableArrayAttrMap( attributes.toArray( new Attr[attributes.size()] ) );
    }

    @Override
    protected final ImmutableNodeList generateChildNodes()
    {
        if ( content_ == null )
        {
            return Nodes.EMPTY_CHILD_NODES;
        }
        else
        {
            ImmutableText text = new ImmutableText( this, content_ );
            return new ImmutableOneNodeList( text );
        }
    }

    public abstract static class Builder<T extends ImmutableLocalizedContentLeaf>
        extends ImmutableChildElement.Builder<T>
    {

        private String content_;

        @Override
        public final Builder<T> isChildOf( ImmutableNode aParentNode )
        {
            return (Builder<T>) super.isChildOf( aParentNode );
        }

        @Override
        public final Builder<T> atIndex( int aElementIndex )
        {
            return (Builder<T>) super.atIndex( aElementIndex );
        }

        @Override
        public final Builder<T> inNamespace( String aNamespaceURI )
        {
            return (Builder<T>) super.inNamespace( aNamespaceURI );
        }

        public final Builder<T> withContent( String aContent )
        {
            content_ = aContent;
            return this;
        }

        @Override
        protected final T newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex,
                                                    String aNamespaceURI )
        {
            return newImmutableLocalizedContentLeaf( aParentNode, aElementIndex, aNamespaceURI, content_ );
        }

        protected abstract T newImmutableLocalizedContentLeaf( ImmutableNode aParentNode, Integer aElementIndex,
                                                               String aNamespaceURI, @Nullable String aContent );
    }
}
