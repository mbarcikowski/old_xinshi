/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Node;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class ImmutableArrayNodeList
    implements ImmutableNodeList
{

    private final ImmutableNode[] nodes_;

    public ImmutableArrayNodeList( final ImmutableNode[] aNodes )
    {
        nodes_ = aNodes;
    }

    @Override
    public Node item( final int index )
    {
        if ( index >= 0 && index < nodes_.length )
        {
            return nodes_[index];
        }
        return null;
    }

    @Override
    public int getLength()
    {
        return nodes_.length;
    }
}
