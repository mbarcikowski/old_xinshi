/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Attr;
import org.w3c.dom.Node;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Strings.isNullOrEmpty;

@Immutable
public final class ImmutableOneAttrMap
    implements ImmutableNamedNodeMap
{

    private final Attr attribute_;

    public ImmutableOneAttrMap( ImmutableAttribute aAttribute )
    {
        attribute_ = aAttribute;
    }

    @Override
    public Attr getNamedItem( String aName )
    {
        if ( attribute_.getNodeName().equals( aName ) )
        {
            return attribute_;
        }
        else
        {
            return null;
        }
    }

    @Override
    public Attr setNamedItem( Node arg )
    {
        throw new UnsupportedOperationException( "ImmutableArrayAttrMap#setNamedItem not supported" );
    }

    @Override
    public Attr removeNamedItem( String name )
    {
        throw new UnsupportedOperationException( "ImmutableArrayAttrMap#removeNamedItem not supported" );
    }

    @Override
    public Attr item( int index )
    {
        if ( index == 0 )
        {
            return attribute_;
        }
        else
        {
            return null;
        }

    }

    @Override
    public int getLength()
    {
        return 1;
    }

    @Override
    public Attr getNamedItemNS( String aNamespaceURI, String aLocalName )
    {
        String namespaceURI = attribute_.getNamespaceURI();
        String localName = attribute_.getLocalName();
        if ( localName.equals( aLocalName ) && ( ( isNullOrEmpty( namespaceURI ) && isNullOrEmpty( aNamespaceURI ) )
            || namespaceURI.equals( aNamespaceURI ) ) )
        {
            return attribute_;
        }
        else
        {
            return null;
        }
    }

    @Override
    public Attr setNamedItemNS( Node arg )
    {
        throw new UnsupportedOperationException( "ImmutableArrayAttrMap#setNamedItemNS not supported" );
    }

    @Override
    public Attr removeNamedItemNS( String namespaceURI, String localName )
    {
        throw new UnsupportedOperationException( "ImmutableArrayAttrMap#removeNamedItemNS not supported" );
    }
}
