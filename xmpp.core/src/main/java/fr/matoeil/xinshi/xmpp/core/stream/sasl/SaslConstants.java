/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.sasl;

public class SaslConstants
{
    public static final String SASL_NAMESPACE_URI = "urn:ietf:params:xml:ns:xmpp-sasl";

    public static final String MECHANISMS = "mechanisms";

    public static final String MECHANISM = "mechanism";

    public static final String ABORT = "abort";

    public static final String AUTH = "auth";

    public static final String CHALLENGE = "challenge";

    public static final String RESPONSE = "response";

    public static final String SUCCESS = "success";

    public static final String FAILURE = "failure";

    public static enum Failure
    {
        ABORTED
            {
                @Override
                public String toString()
                {
                    return "aborted";
                }
            },
        ACCOUNT_DISABLED
            {
                @Override
                public String toString()
                {
                    return "account-disabled";
                }
            },
        CREDENTIALS_EXPIRED
            {
                @Override
                public String toString()
                {
                    return "credentials-expired";
                }
            },
        ENCRYPTION_REQUIRED
            {
                @Override
                public String toString()
                {
                    return "encryption-required";
                }
            },
        INCORRECT_ENCODING
            {
                @Override
                public String toString()
                {
                    return "incorrect-encoding";
                }
            },
        INVALID_AUTHZID
            {
                @Override
                public String toString()
                {
                    return "invalid-authzid";
                }
            },
        INVALID_MECHANISM
            {
                @Override
                public String toString()
                {
                    return "invalid-mechanism";
                }
            },
        MALFORMED_REQUEST
            {
                @Override
                public String toString()
                {
                    return "malformed-request";
                }
            },
        MECHANISM_TOO_WEAK
            {
                @Override
                public String toString()
                {
                    return "mechanism-too-weak";
                }
            },
        NOT_AUTHORIZED
            {
                @Override
                public String toString()
                {
                    return "not-authorized";
                }
            },
        TEMPORARY_AUTH_FAILURE
            {
                @Override
                public String toString()
                {
                    return "temporary-auth-failure";
                }
            }
    }

}
