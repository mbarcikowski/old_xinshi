/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.concurrent.Immutable;

import static fr.matoeil.xinshi.xmpp.core.internal.stanza.Nodes.EMPTY_CHILD_NODES;

@Immutable
public abstract class ImmutableCharacterData
    extends ImmutableNode
    implements CharacterData
{

    private final String data_;

    public ImmutableCharacterData( final String aData )
    {
        data_ = aData;
    }

    @Override
    public final String getNodeValue()
    {
        return data_;
    }

    @Override
    public final String getData()
    {
        return data_;
    }

    @Override
    public final void setData( final String data )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#setData not supported" );
    }

    @Override
    public final int getLength()
    {
        return data_.length();
    }

    @Override
    public final String substringData( final int offset, final int count )
    {
        int length = data_.length();
        if ( count < 0 || offset < 0 || offset > length - 1 )
        {
            throw new DOMException( DOMException.INDEX_SIZE_ERR,
                                    "The index or size is negative, or greater than the allowed value." );
        }

        int tailIndex = Math.min( offset + count, length );

        return data_.substring( offset, tailIndex );
    }

    @Override
    public final void appendData( final String arg )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#appendData not supported" );
    }

    @Override
    public final void insertData( final int offset, final String arg )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#insertData not supported" );
    }

    @Override
    public final void deleteData( final int offset, final int count )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#deleteData not supported" );
    }

    @Override
    public final void replaceData( final int offset, final int count, final String arg )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#replaceData not supported" );
    }


    @Override
    public final void setNodeValue( final String nodeValue )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#setNodeValue not supported" );
    }


    @Override
    public final NodeList getChildNodes()
    {
        return EMPTY_CHILD_NODES;
    }

    @Override
    public final Node getFirstChild()
    {
        return null;
    }

    @Override
    public final Node getLastChild()
    {
        return null;
    }

    @Override
    public final NamedNodeMap getAttributes()
    {
        return null;
    }

    @Override
    public final boolean hasChildNodes()
    {
        return false;
    }

    @Override
    public final String getNamespaceURI()
    {
        return null;
    }

    @Override
    public final String getPrefix()
    {
        return null;
    }

    @Override
    public final String getLocalName()
    {
        return null;
    }

    @Override
    public final boolean hasAttributes()
    {
        return false;
    }

    @Override
    public final String getTextContent()
    {
        return data_;
    }

    @Override
    public final boolean isEqualNode( final Node arg )
    {
        if ( arg == this )
        {
            return true;
        }
        if ( arg.getNodeType() != getNodeType() )
        {
            return false;
        }
        if ( getNodeValue() == null )
        {
            if ( arg.getNodeValue() != null )
            {
                return false;
            }
        }
        else if ( !getNodeValue().equals( arg.getNodeValue() ) )
        {
            return false;
        }
        return true;
    }
}
