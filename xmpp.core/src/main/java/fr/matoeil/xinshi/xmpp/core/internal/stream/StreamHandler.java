/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream;

import com.ctc.wstx.stax.WstxOutputFactory;
import com.google.common.base.Strings;
import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.stream.StreamListener;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import uk.org.retep.niosax.NioSaxParserHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;

public class StreamHandler
    implements ContentHandler, LexicalHandler, NioSaxParserHandler
{
    private final static WstxOutputFactory XML_OUTPUT_FACTORY = new WstxOutputFactory();

    private final static DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();

    private final static DocumentBuilder DOCUMENT_BUILDER;

    private final static int STANZA_DEPTH = 2;

    static
    {
        XML_OUTPUT_FACTORY.configureForSpeed();
        try
        {
            DOCUMENT_BUILDER = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
        }
        catch ( ParserConfigurationException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }


    private final StreamListener streamListener_;

    private Document document_ = null;

    private XMLStreamWriter writer_ = null;

    private int depth_ = 0;

    private boolean isCDATA_ = false;

    private Element streamStart_ = null;

    public StreamHandler( final StreamListener aStreamListener )
    {
        streamListener_ = aStreamListener;
        resetXmlStreamWriter();
    }

    public void reset()
    {
        document_ = null;
        writer_ = null;
        depth_ = 0;
        isCDATA_ = false;
        streamStart_ = null;
        resetXmlStreamWriter();
    }


    @Override
    public void xmlDeclaration( String versionInfo, String encoding, boolean standalone )
    {
        if ( encoding != null && !StreamConstants.XML_ENCODING.equals( encoding ) )
        {
            throw new StreamException( StreamErrorConstants.Error.UNSUPPORTED_ENCODING );
        }
    }

    @Override
    public void startElement( final String uri, final String localName, final String qName,
                              final Attributes attributes )
        throws SAXException
    {
        try
        {
            if ( Strings.isNullOrEmpty( uri ) )
            {

                writer_.writeStartElement( localName );
            }
            else
            {
                String[] split = qName.split( ":" );
                if ( split.length == 2 )
                {
                    writer_.writeStartElement( split[0], localName, uri );
                }
                else
                {
                    writer_.writeStartElement( "", localName, uri );
                }

            }
            final int count = attributes.getLength();
            if ( count > 0 )
            {
                for ( int i = 0; i < count; i++ )
                {
                    String attributeNamespaceURI = attributes.getURI( i );
                    String attributesQName = attributes.getQName( i );
                    String attributeLocalName = attributes.getLocalName( i );
                    String attributeValue = attributes.getValue( i );
                    String[] split = attributesQName.split( ":" );
                    if ( split.length == 2 )
                    {
                        String prefix = split[0];
                        if ( "xmlns".equals( prefix ) )
                        {
                            writer_.writeNamespace( split[1], attributeValue );
                        }
                        else
                        {
                            writer_.writeAttribute( split[0], attributeNamespaceURI, attributeLocalName,
                                                    attributeValue );
                        }
                    }
                    else
                    {
                        if ( "xmlns".equals( attributeLocalName ) )
                        {
                            writer_.writeNamespace( "", attributeValue );
                        }
                        else
                        {
                            writer_.writeAttribute( "", attributeNamespaceURI, attributeLocalName, attributeValue );
                        }
                    }

                }
            }
            verifyStreamOpening();
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.NOT_WELL_FORMED, e.getMessage(), e );
        }
    }

    @Override
    public void startDTD( String name, String publicId, String systemId )
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void endDTD()
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void startEntity( String name )
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void endEntity( String name )
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void comment( char[] ch, int start, int length )
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void processingInstruction( String target, String data )
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void skippedEntity( String name )
        throws SAXException
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public void setDocumentLocator( Locator locator )
    {
        //no op
    }

    @Override
    public void startDocument()
        throws SAXException
    {
        //no op
    }

    @Override
    public void endDocument()
        throws SAXException
    {
        //no op
    }

    @Override
    public void startPrefixMapping( String prefix, String uri )
        throws SAXException
    {
        if ( depth_ == 0 && StreamConstants.STREAM_NAMESPACE_URI.equals( uri ) && ( prefix.length() > 0
            && !StreamConstants.STREAM_PREFIX.equals( prefix ) ) )
        {
            throw new StreamException( StreamErrorConstants.Error.BAD_NAMESPACE_PREFIX );
        }
    }

    @Override
    public void endPrefixMapping( String prefix )
        throws SAXException
    {
        //no op
    }

    @Override
    public void characters( final char[] ch, final int start, final int length )
        throws SAXException
    {
        try
        {
            if ( isCDATA_ )
            {
                String s = String.valueOf( ch, start, length );
                writer_.writeCData( s );
            }
            else
            {
                writer_.writeCharacters( ch, start, length );
            }
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.NOT_WELL_FORMED, e.getMessage(), e );
        }
    }

    @Override
    public void ignorableWhitespace( char[] ch, int start, int length )
        throws SAXException
    {
        //no op
    }

    @Override
    public void startCDATA()
        throws SAXException
    {
        isCDATA_ = true;
    }

    @Override
    public void endCDATA()
        throws SAXException
    {
        isCDATA_ = false;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement( final String uri, final String localName, final String qName )
        throws SAXException
    {
        try
        {
            if ( depth_ == 1 )
            {
                writer_.writeEndDocument();
                writer_.flush();
                writer_.close();
                endRootNode();
            }
            else if ( depth_ == STANZA_DEPTH )
            {
                writer_.writeEndElement();
                writer_.flush();
                Element stanza = (Element) streamStart_.getFirstChild();
                streamStart_.removeChild( stanza );
                nodeTriggered( stanza );
            }
            else
            {
                writer_.writeEndElement();
            }
            depth_--;
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.NOT_WELL_FORMED, e.getMessage(), e );
        }
    }

    private void resetXmlStreamWriter()
    {
        try
        {
            document_ = DOCUMENT_BUILDER.newDocument();
            writer_ = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( document_ ) );
            writer_.writeStartDocument();
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }

    private void verifyStreamOpening()
        throws SAXException
    {
        try
        {
            writer_.flush();
            depth_++;
            if ( depth_ == 1 )
            {
                writer_.flush();
                streamStart_ = document_.getDocumentElement();
                startRootNode( streamStart_ );
            }
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.NOT_WELL_FORMED, e.getMessage(), e );
        }
    }

    private void nodeTriggered( Element aElement )
    {
        streamListener_.handleIncomingStanza( aElement );
    }

    private void startRootNode( Element aElement )
    {
        Header header = StreamDeserializer.decodeHeader( aElement );
        streamListener_.handleStreamOpening( header );
    }

    private void endRootNode()
    {
        streamListener_.handleStreamClosing();
    }
}
