/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

public class StreamErrorConstants
{
    public static final String STREAM_ERROR_NAMESPACE_URI = "urn:ietf:params:xml:ns:xmpp-streams";

    public static final String ERROR = "error";

    public static final String TEXT = "text";

    public static enum Error
    {
        BAD_FORMAT
            {
                @Override
                public String toString()
                {
                    return "bad-format";
                }
            },
        BAD_NAMESPACE_PREFIX
            {
                @Override
                public String toString()
                {
                    return "bad-namespace-prefix";
                }
            },
        CONFLICT
            {
                @Override
                public String toString()
                {
                    return "conflict";
                }
            },
        CONNECTION_TIMEOUT
            {
                @Override
                public String toString()
                {
                    return "connection-timeout";
                }
            },
        HOST_GONE
            {
                @Override
                public String toString()
                {
                    return "host-gone";
                }
            },
        HOST_UNKNOWN
            {
                @Override
                public String toString()
                {
                    return "host-unknown";
                }
            },
        IMPROPER_ADDRESSING
            {
                @Override
                public String toString()
                {
                    return "improper-addressing";
                }
            },
        INTERNAL_SERVER_ERROR
            {
                @Override
                public String toString()
                {
                    return "internal-server-error";
                }
            },
        INVALID_FROM
            {
                @Override
                public String toString()
                {
                    return "invalid-from";
                }
            },
        INVALID_ID
            {
                @Override
                public String toString()
                {
                    return "invalid-id";
                }
            },
        INVALID_NAMESPACE
            {
                @Override
                public String toString()
                {
                    return "invalid-namespace";
                }
            },
        INVALID_XML
            {
                @Override
                public String toString()
                {
                    return "invalid-xml";
                }
            },
        NO_AUTHORIZED
            {
                @Override
                public String toString()
                {
                    return "not-authorized";
                }
            },


        NOT_WELL_FORMED
            {
                @Override
                public String toString()
                {
                    return "not-well-formed";
                }
            },
        POLICY_VIOLATION
            {
                @Override
                public String toString()
                {
                    return "policy-violation";
                }
            },
        REMOTE_CONNECTION_FAILED
            {
                @Override
                public String toString()
                {
                    return "remote-connection-failed";
                }
            },
        RESET
            {
                @Override
                public String toString()
                {
                    return "reset";
                }
            },
        RESOURCE_CONSTRAINT
            {
                @Override
                public String toString()
                {
                    return "resource-constraint";
                }
            },
        RESTRICTED_XML
            {
                @Override
                public String toString()
                {
                    return "restricted-xml";
                }
            },
        SEE_OTHER_HOST
            {
                @Override
                public String toString()
                {
                    return "see-other-host";
                }
            },
        SYSTEM_SHUTDOWN
            {
                @Override
                public String toString()
                {
                    return "system-shutdown";
                }
            },
        UNDEFINED_CONDITION
            {
                @Override
                public String toString()
                {
                    return "undefined-condition";
                }
            },
        UNSUPPORTED_ENCODING
            {
                @Override
                public String toString()
                {
                    return "unsupported-encoding";
                }
            },
        UNSUPPORTED_STANZA_TYPE
            {
                @Override
                public String toString()
                {
                    return "unsupported-stanza-type";
                }
            },
        UNSUPPORTED_VERSION
            {
                @Override
                public String toString()
                {
                    return "unsupported-version";
                }
            }
    }

}
