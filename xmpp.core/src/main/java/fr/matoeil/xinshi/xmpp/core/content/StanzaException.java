/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.content;


//TODO add missing properties
public class StanzaException
    extends RuntimeException
{
    private final StanzaErrorConstants.Condition condition_;

    public StanzaException( StanzaErrorConstants.Condition aCondition )
    {
        super();
        condition_ = aCondition;
    }

    public StanzaException( StanzaErrorConstants.Condition aCondition, String message )
    {
        super( message );
        condition_ = aCondition;
    }

    public StanzaException( StanzaErrorConstants.Condition aCondition, String message, Throwable cause )
    {
        super( message, cause );
        condition_ = aCondition;
    }

    public StanzaException( StanzaErrorConstants.Condition aCondition, Throwable cause )
    {
        super( cause );
        condition_ = aCondition;
    }

    public StanzaErrorConstants.Condition condition()
    {
        return condition_;
    }
}
