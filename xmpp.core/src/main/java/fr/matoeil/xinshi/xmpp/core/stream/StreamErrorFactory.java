/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import com.google.common.base.Throwables;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;

import static fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants.ERROR;
import static fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants.STREAM_ERROR_NAMESPACE_URI;

//TODO migrate to a builder
public class StreamErrorFactory
{
    private final static XMLOutputFactory XML_OUTPUT_FACTORY = XMLOutputFactory.newFactory();

    private final static DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();

    private static DocumentBuilder DOCUMENT_BUILDER;

    static
    {
        try
        {
            DOCUMENT_BUILDER = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
        }
        catch ( ParserConfigurationException e )
        {
            throw Throwables.propagate( e );
        }
    }

    //TODO add missing elements
    //TODO optional text
    public static Element newError( StreamErrorConstants.Error aError )
        throws XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "stream", ERROR, StreamConstants.STREAM_NAMESPACE_URI );
        streamWriter.writeStartElement( "", aError.toString(), STREAM_ERROR_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( STREAM_ERROR_NAMESPACE_URI );
        streamWriter.writeEndElement();
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
        return doc.getDocumentElement();
    }
}
