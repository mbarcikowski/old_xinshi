/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.internal.stream.NoOpStreamHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.StreamHandler;
import org.xml.sax.SAXException;
import uk.org.retep.niosax.NioSaxParser;
import uk.org.retep.niosax.NioSaxParserFactory;
import uk.org.retep.niosax.NioSaxSource;
import uk.org.retep.niosax.charset.Charset;
import uk.org.retep.niosax.charset.UTF_8;

import java.nio.ByteBuffer;

public class StreamReader
{
    private final static NioSaxParserFactory NIO_SAX_PARSER_FACTORY = NioSaxParserFactory.getInstance();

    private static final Charset UTF_8_CHARSET = new UTF_8();

    private NioSaxParser nioSaxParser_;

    private NioSaxSource nioSaxSource_;

    public StreamReader( final StreamListener aStreamListener )
    {
        nioSaxParser_ = NIO_SAX_PARSER_FACTORY.newInstance();
        nioSaxParser_.setHandler( new StreamHandler( aStreamListener ) );
        nioSaxSource_ = new NioSaxSource( UTF_8_CHARSET );
        try
        {
            nioSaxParser_.startDocument();
        }
        catch ( SAXException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }

    public void read( final ByteBuffer aByteBuffer )
    {
        try
        {
            nioSaxSource_.setByteBuffer( aByteBuffer );
            nioSaxParser_.parse( nioSaxSource_ );
            nioSaxSource_.compact();
        }
        catch ( SAXException e )
        {
            throw new StreamException( StreamErrorConstants.Error.NOT_WELL_FORMED, e.getMessage(), e );
        }
    }

    public void reset()
    {
        StreamHandler previousStreamHandler = (StreamHandler) nioSaxParser_.getHandler();
        nioSaxParser_ = NIO_SAX_PARSER_FACTORY.newInstance();
        previousStreamHandler.reset();
        nioSaxParser_.setHandler( previousStreamHandler );
        nioSaxSource_ = new NioSaxSource( UTF_8_CHARSET );
        try
        {
            nioSaxParser_.startDocument();
        }
        catch ( SAXException e )
        {
            throw new StreamException( StreamErrorConstants.Error.NOT_WELL_FORMED, e.getMessage(), e );
        }
    }

    public void close()
    {
        try
        {
            nioSaxParser_.endDocument();
            nioSaxParser_.setHandler( new NoOpStreamHandler() );
        }
        catch ( SAXException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }
}
