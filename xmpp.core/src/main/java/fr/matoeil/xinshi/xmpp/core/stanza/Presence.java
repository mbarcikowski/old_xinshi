/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.Nodes;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Priority;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Status;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

@Immutable
public class Presence
    extends Stanza<Presence.Type>
{

    private static final String LOCAL_NAME = "presence";

    private final Show show_;

    private final List<Status> statuses_;

    private final Priority priority_;

    Presence( final String aNamespaceURI, @Nullable final Address aFrom, @Nullable final Address aTo,
              @Nullable final String aId, @Nullable final Type aType, @Nullable final Show aShow,
              List<Status> aStatuses, @Nullable final Priority aPriority )
    {
        super( aNamespaceURI, LOCAL_NAME, aFrom, aTo, aId, aType );
        show_ = aShow;
        statuses_ = new ImmutableList.Builder<Status>().addAll( aStatuses ).build();
        priority_ = aPriority;
        setImmutableChildNodes( generateChildNodes() );
    }

    @Nullable
    public Show getShow()
    {
        return show_;
    }

    public List<Status> getStatuses()
    {
        return statuses_;
    }

    @Nullable
    public Priority getPriority()
    {
        return priority_;
    }

    @Override
    protected final ImmutableNodeList generateChildNodes()
    {
        int initialCapacity = ( show_ == null ? 0 : 1 ) + statuses_.size() + ( priority_ == null ? 0 : 1 );
        if ( initialCapacity == 0 )
        {
            return Nodes.EMPTY_CHILD_NODES;
        }
        else
        {
            List<ImmutableChildElement> childElements = new ArrayList<>( initialCapacity );
            if ( show_ != null )
            {
                childElements.add( show_ );
            }
            childElements.addAll( statuses_ );
            if ( priority_ != null )
            {
                childElements.add( priority_ );
            }
            return new ImmutableArrayNodeList( childElements.toArray( new ImmutableNode[initialCapacity] ) );
        }
    }

    public static enum Type
        implements fr.matoeil.xinshi.xmpp.core.internal.stanza.Type
    {
        ERROR
            {
                @Override
                public String asString()
                {
                    return "error";
                }
            },
        PROBE
            {
                @Override
                public String asString()
                {
                    return "probe";
                }
            },
        SUBSCRIBE
            {
                @Override
                public String asString()
                {
                    return "subscribe";
                }
            },
        SUBSCRIBED
            {
                @Override
                public String asString()
                {
                    return "subscribed";
                }
            },
        UNAVAILABLE
            {
                @Override
                public String asString()
                {
                    return "unavailable";
                }
            },
        UNSUBSCRIBE
            {
                @Override
                public String asString()
                {
                    return "unsubscribe";
                }
            },
        UNSUBSCRIBED
            {
                @Override
                public String asString()
                {
                    return "unsubscribed";
                }
            };

        @Override
        public String toString()
        {
            return asString();
        }
    }

}
