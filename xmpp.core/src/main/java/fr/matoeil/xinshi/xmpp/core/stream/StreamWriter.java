/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import com.ctc.wstx.stax.WstxOutputFactory;
import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants;
import fr.matoeil.xinshi.xmpp.core.internal.stream.StreamSerializer;
import fr.matoeil.xinshi.xmpp.core.version.Version;
import org.codehaus.stax2.XMLOutputFactory2;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.OutputStream;

public class StreamWriter
{
    private final static WstxOutputFactory XML_OUTPUT_FACTORY = new WstxOutputFactory();

    static
    {
        XML_OUTPUT_FACTORY.configureForSpeed();
        XML_OUTPUT_FACTORY.setProperty( XMLOutputFactory2.P_AUTOMATIC_EMPTY_ELEMENTS, true );
    }

    private XMLStreamWriter streamWriter_;

    public StreamWriter( final OutputStream aOutputStream )
    {
        resetXmlStreamWriter( aOutputStream );
    }


    public void writeStreamOpening( Header aHeader )
    {
        try
        {
            streamWriter_.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
            streamWriter_.setPrefix( StreamConstants.STREAM_PREFIX, StreamConstants.STREAM_NAMESPACE_URI );
            streamWriter_.setDefaultNamespace( ClientContentConstants.CLIENT_CONTENT_NAMESPACE_URI );
            streamWriter_.writeStartElement( StreamConstants.STREAM_PREFIX, StreamConstants.STREAM,
                                             StreamConstants.STREAM_NAMESPACE_URI );
            streamWriter_.writeDefaultNamespace( ClientContentConstants.CLIENT_CONTENT_NAMESPACE_URI );
            streamWriter_.writeNamespace( StreamConstants.STREAM_PREFIX, StreamConstants.STREAM_NAMESPACE_URI );
            Address from = aHeader.from();
            if ( from != null )
            {
                streamWriter_.writeAttribute( "from", from.toString() );
            }
            String id = aHeader.id();
            if ( id != null )
            {
                streamWriter_.writeAttribute( "id", id );
            }
            Address to = aHeader.to();
            if ( to != null )
            {
                streamWriter_.writeAttribute( "from", to.toString() );
            }
            Version version = aHeader.version();
            if ( version != null )
            {
                streamWriter_.writeAttribute( "version", version.toString() );
            }
            String xmlLang = aHeader.lang();
            if ( xmlLang != null )
            {
                streamWriter_.writeAttribute( XMLConstants.XML_NS_PREFIX, XMLConstants.XML_NS_URI,
                                              StreamConstants.Attributes.LANG, xmlLang );
            }
            streamWriter_.writeCharacters( "" );
            streamWriter_.flush();
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }

    public void writeStanza( Element aStanza )
    {
        try
        {
            StreamSerializer.encodeStanza( aStanza, streamWriter_ );
            streamWriter_.flush();
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }

    public void writeStreamClosing()
    {
        try
        {
            streamWriter_.writeCharacters( "" );
            streamWriter_.writeEndElement();
            streamWriter_.writeEndDocument();
            streamWriter_.flush();
            streamWriter_.close();
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }

    public void reset( OutputStream aOutputStream )
    {
        resetXmlStreamWriter( aOutputStream );
    }

    public void close()
    {
        try
        {
            streamWriter_.close();
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }

    private void resetXmlStreamWriter( OutputStream aOutputStream )
    {
        try
        {
            streamWriter_ = XML_OUTPUT_FACTORY.createXMLStreamWriter( aOutputStream, StreamConstants.XML_ENCODING );
        }
        catch ( XMLStreamException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
    }


}
