/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.Nodes;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public class Iq
    extends Stanza<Iq.Type>
{

    private static final String LOCAL_NAME = "iq";

    Iq( final String aNamespaceURI, @Nullable final Address aFrom, @Nullable final Address aTo,
        @Nullable final String aId, @Nullable final Type aType )
    {
        super( aNamespaceURI, LOCAL_NAME, aFrom, aTo, aId, aType );
        setImmutableChildNodes( generateChildNodes() );
    }

    @Override
    protected final ImmutableNodeList generateChildNodes()
    {
        return Nodes.EMPTY_CHILD_NODES;
    }

    public static enum Type
        implements fr.matoeil.xinshi.xmpp.core.internal.stanza.Type
    {
        ERROR
            {
                @Override
                public String asString()
                {
                    return "error";
                }
            },
        GET
            {
                @Override
                public String asString()
                {
                    return "get";
                }
            },
        RESULT
            {
                @Override
                public String asString()
                {
                    return "result";
                }
            },
        SET
            {
                @Override
                public String asString()
                {
                    return "set";
                }
            };

        @Override
        public String toString()
        {
            return asString();
        }
    }
}
