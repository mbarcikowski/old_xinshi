/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import com.google.common.base.Joiner;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class ImmutableAttribute
    extends ImmutableNode
    implements Attr
{
    private final ImmutableElement ownerElement_;

    private final String namespaceURI_;

    private final String prefix_;

    private final String localName_;

    private final String value_;

    private final ImmutableOneNodeList childNodes_;

    public ImmutableAttribute( ImmutableElement aOwnerElement, String aNamespaceURI, String aPrefix, String aLocalName,
                               String aValue )
    {
        ownerElement_ = aOwnerElement;
        namespaceURI_ = aNamespaceURI;
        prefix_ = aPrefix;
        localName_ = aLocalName;
        value_ = aValue;
        childNodes_ = generateChildNodes();
    }


    @Override
    public String getName()
    {
        return getQualifiedName();
    }

    @Override
    public boolean getSpecified()
    {
        return true;
    }

    @Override
    public String getValue()
    {
        return value_;
    }

    @Override
    public void setValue( String value )
    {
        throw new UnsupportedOperationException( "ImmutableAttribute#setValue not supported" );
    }

    @Override
    public Element getOwnerElement()
    {
        return ownerElement_;
    }

    @Override
    public TypeInfo getSchemaTypeInfo()
    {
        throw new UnsupportedOperationException( "ImmutableAttribute#getSchemaTypeInfo not supported" );
    }

    @Override
    public boolean isId()
    {
        //TODO ImmutableAttribute#isId not implemented
        throw new UnsupportedOperationException( "ImmutableAttribute#isId not implemented" );
    }

    @Override
    public String getNodeName()
    {
        return getQualifiedName();
    }

    @Override
    public String getNodeValue()
    {
        return value_;
    }

    @Override
    public void setNodeValue( String nodeValue )
    {
        throw new UnsupportedOperationException( "ImmutableAttribute#setNodeValue not supported" );
    }

    @Override
    public short getNodeType()
    {
        return Node.ATTRIBUTE_NODE;
    }

    @Override
    public Node getParentNode()
    {
        return null;
    }

    @Override
    public NodeList getChildNodes()
    {
        return childNodes_;
    }

    @Override
    public Node getFirstChild()
    {
        return childNodes_.item( 0 );
    }

    @Override
    public Node getLastChild()
    {
        return childNodes_.item( 0 );
    }

    @Override
    public Node getPreviousSibling()
    {
        return null;
    }

    @Override
    public Node getNextSibling()
    {
        return null;
    }

    @Override
    public NamedNodeMap getAttributes()
    {
        return null;
    }

    @Override
    public boolean hasChildNodes()
    {
        return true;
    }

    @Override
    public String getNamespaceURI()
    {
        return namespaceURI_;
    }

    @Override
    public String getPrefix()
    {
        return prefix_;
    }

    @Override
    public String getLocalName()
    {
        return localName_;
    }

    @Override
    public boolean hasAttributes()
    {
        return false;
    }

    @Override
    public short compareDocumentPosition( Node other )
    {
        //TODO ImmutableAttribute#compareDocumentPosition not implemented
        throw new UnsupportedOperationException( "ImmutableAttribute#compareDocumentPosition not implemented" );
    }

    @Override
    public String getTextContent()
    {
        return value_;
    }

    @Override
    public String lookupPrefix( String namespaceURI )
    {
        //TODO ImmutableAttribute#lookupPrefix not implemented
        throw new UnsupportedOperationException( "ImmutableAttribute#lookupPrefix not implemented" );
    }

    @Override
    public boolean isDefaultNamespace( String namespaceURI )
    {
        //TODO ImmutableAttribute#isDefaultNamespace not implemented
        throw new UnsupportedOperationException( "ImmutableAttribute#isDefaultNamespace not implemented" );
    }

    @Override
    public String lookupNamespaceURI( String prefix )
    {
        //TODO ImmutableAttribute#lookupNamespaceURI not implemented
        throw new UnsupportedOperationException( "ImmutableAttribute#lookupNamespaceURI not implemented" );
    }

    @Override
    public boolean isEqualNode( Node arg )
    {
        //TODO ImmutableAttribute#isEqualNode not implemented
        throw new UnsupportedOperationException( "ImmutableAttribute#isEqualNode not implemented" );
    }

    private ImmutableOneNodeList generateChildNodes()
    {
        ImmutableText text = new ImmutableText( this, value_ );
        return new ImmutableOneNodeList( text );
    }

    private String getQualifiedName()
    {
        return Joiner.on( ':' ).skipNulls().join( prefix_, localName_ );
    }
}
