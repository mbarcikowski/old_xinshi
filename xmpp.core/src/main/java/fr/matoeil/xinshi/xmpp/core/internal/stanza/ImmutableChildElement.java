/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Node;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public abstract class ImmutableChildElement
    extends ImmutableElement
{
    private final ImmutableNode parentNode_;

    private final int elementIndex_;

    public ImmutableChildElement( final ImmutableNode aParentNode, final int aElementIndex, final String aNamespaceURI,
                                  final String aLocalName )
    {
        super( aNamespaceURI, aLocalName );
        parentNode_ = aParentNode;
        elementIndex_ = aElementIndex;
    }


    @Override
    public final Node getParentNode()
    {
        return parentNode_;
    }

    @Override
    public final Node getPreviousSibling()
    {
        return parentNode_.getChildNodes().item( elementIndex_ - 1 );
    }

    @Override
    public final Node getNextSibling()
    {
        return parentNode_.getChildNodes().item( elementIndex_ + 1 );
    }

    public abstract static class Builder<T extends ImmutableChildElement>
    {

        private ImmutableNode parentNode_;

        private Integer elementIndex_;

        private String namespaceURI_;

        protected Builder<T> isChildOf( ImmutableNode aParentNode )
        {
            checkArgument( aParentNode != null, "must be not null" );
            parentNode_ = aParentNode;
            return this;
        }

        protected Builder<T> atIndex( int aElementIndex )
        {
            checkArgument( aElementIndex >= 0, "must be nonnegative: %s", aElementIndex );
            elementIndex_ = aElementIndex;
            return this;
        }

        protected Builder<T> inNamespace( String aNamespaceURI )
        {
            checkArgument( aNamespaceURI != null, "must be not null or empty" );
            namespaceURI_ = aNamespaceURI;
            return this;
        }

        public final T build()
        {
            checkState( parentNode_ != null, "must not be null" );
            checkState( elementIndex_ != null && elementIndex_ >= 0, "must be nonnegative: %s", elementIndex_ );
            checkState( namespaceURI_ != null, "must be not null or empty" );
            return newImmutableChildElement( parentNode_, elementIndex_, namespaceURI_ );
        }

        protected abstract T newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex,
                                                       String aNamespaceURI );
    }
}
