/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.Nodes;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Body;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Subject;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Thread;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

@Immutable
public class Message
    extends Stanza<Message.Type>
{

    private static final String LOCAL_NAME = "message";

    private final List<Body> bodies_;

    private final List<Subject> subjects_;

    private final fr.matoeil.xinshi.xmpp.core.stanza.message.Thread thread_;

    Message( final String aNamespaceURI, @Nullable final Address aFrom, @Nullable final Address aTo,
             @Nullable final String aId, @Nullable final Type aType, List<Body> aBodies, List<Subject> aSubjects,
             @Nullable final Thread aThread )
    {
        super( aNamespaceURI, LOCAL_NAME, aFrom, aTo, aId, aType );
        bodies_ = new ImmutableList.Builder<Body>().addAll( aBodies ).build();
        subjects_ = new ImmutableList.Builder<Subject>().addAll( aSubjects ).build();
        thread_ = aThread;
        setImmutableChildNodes( generateChildNodes() );
    }

    public List<Body> getBodies()
    {
        return bodies_;
    }

    public List<Subject> getSubjects()
    {
        return subjects_;
    }

    @Nullable
    public Thread getThread()
    {
        return thread_;
    }

    @Override
    protected final ImmutableNodeList generateChildNodes()
    {
        int initialCapacity = bodies_.size() + subjects_.size() + ( thread_ == null ? 0 : 1 );
        if ( initialCapacity == 0 )
        {
            return Nodes.EMPTY_CHILD_NODES;
        }
        else
        {
            List<ImmutableChildElement> childElements = new ArrayList<>( initialCapacity );
            childElements.addAll( bodies_ );
            childElements.addAll( subjects_ );
            if ( thread_ != null )
            {
                childElements.add( thread_ );
            }
            return new ImmutableArrayNodeList( childElements.toArray( new ImmutableNode[initialCapacity] ) );
        }
    }

    public static enum Type
        implements fr.matoeil.xinshi.xmpp.core.internal.stanza.Type
    {
        ERROR
            {
                @Override
                public String asString()
                {
                    return "error";
                }
            },
        CHAT
            {
                @Override
                public String asString()
                {
                    return "chat";
                }
            },
        GROUPCHAT
            {
                @Override
                public String asString()
                {
                    return "groupchat";
                }
            },
        HEADLINE
            {
                @Override
                public String asString()
                {
                    return "headline";
                }
            },
        NORMAL
            {
                @Override
                public String asString()
                {
                    return "normal";
                }
            };

        @Override
        public String toString()
        {
            return asString();
        }
    }

}
