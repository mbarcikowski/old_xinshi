/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.stanza.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNamedNodeMap;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableText;
import org.w3c.dom.Element;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public class Show
    extends ImmutableChildElement
    implements Element
{
    private static final String LOCAL_NAME = "show";

    private final State state_;

    Show( final ImmutableNode aParentNode, final int aElementIndex, final String aNamespaceURI, final State aState )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, LOCAL_NAME );
        state_ = aState;
        setImmutableAttributes( generateAttributes() );
        setImmutableChildNodes( generateChildNodes() );
    }

    public State getState()
    {
        return state_;
    }

    @Override
    public String getTextContent()
    {
        return state_.asString();
    }

    @Override
    protected final ImmutableNamedNodeMap generateAttributes()
    {
        return Elements.EMPTY_ATTRIBUTES;
    }

    @Override
    protected final ImmutableNodeList generateChildNodes()
    {
        ImmutableText text = new ImmutableText( this, state_.asString() );
        return new ImmutableOneNodeList( text );
    }

    public static enum State
    {
        AWAY
            {
                @Override
                public String asString()
                {
                    return "away";
                }
            },
        CHAT
            {
                @Override
                public String asString()
                {
                    return "chat";
                }
            },
        DND
            {
                @Override
                public String asString()
                {
                    return "dnd";
                }
            },
        XA
            {
                @Override
                public String asString()
                {
                    return "xa";
                }
            };

        public abstract String asString();

        @Override
        public String toString()
        {
            return asString();
        }
    }

    public static final class Builder
        extends ImmutableChildElement.Builder<Show>
    {


        private State state_;

        @Override
        public Builder isChildOf( ImmutableNode aParentNode )
        {
            return (Builder) super.isChildOf( aParentNode );
        }

        @Override
        public Builder atIndex( int aElementIndex )
        {
            return (Builder) super.atIndex( aElementIndex );
        }

        @Override
        public Builder inNamespace( String aNamespaceURI )
        {
            return (Builder) super.inNamespace( aNamespaceURI );
        }

        public Builder withState( State aState )
        {
            checkArgument( aState != null, "must not be null" );
            state_ = aState;
            return this;
        }

        @Override
        protected Show newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex,
                                                 String aNamespaceURI )
        {
            checkState( state_ != null, "must not be null" );
            return new Show( aParentNode, aElementIndex, aNamespaceURI, state_ );
        }


    }
}
