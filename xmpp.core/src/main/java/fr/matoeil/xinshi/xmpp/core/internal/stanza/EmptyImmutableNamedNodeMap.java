/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Attr;
import org.w3c.dom.Node;

/**
 * @author mathieu.barcikowski@gmail.com
 */
class EmptyImmutableNamedNodeMap
    implements ImmutableNamedNodeMap
{
    /**
     * Retrieves a node specified by name.
     *
     * @param name The <code>nodeName</code> of a node to retrieve.
     * @return A <code>Node</code> (of any type) with the specified
     *         <code>nodeName</code>, or <code>null</code> if it does not identify
     *         any node in this map.
     */
    @Override
    public final Attr getNamedItem( final String name )
    {
        return null;
    }

    /**
     * Immutable NamedNodeMap. Mutation isn't supported.
     *
     * @throws UnsupportedOperationException operation not supported.
     */
    @Override
    public final Attr setNamedItem( final Node arg )
    {
        throw new UnsupportedOperationException( "#setNamedItem not supported" );
    }

    /**
     * Immutable NamedNodeMap. Mutation isn't supported.
     *
     * @throws UnsupportedOperationException operation not supported.
     */
    @Override
    public final Attr removeNamedItem( final String name )
    {
        throw new UnsupportedOperationException( "#removeNamedItem not supported" );
    }

    /**
     * Returns the <code>index</code>th item in the map. If <code>index</code>
     * is greater than or equal to the number of nodes in this map, this
     * returns <code>null</code>.
     *
     * @param index Index into this map.
     * @return The node at the <code>index</code>th position in the map, or
     *         <code>null</code> if that is not a valid index.
     */
    @Override
    public final Attr item( final int index )
    {
        return null;
    }

    /**
     * The number of nodes in this map. The range of valid child node indices
     * is <code>0</code> to <code>length-1</code> inclusive.
     */
    @Override
    public final int getLength()
    {
        return 0;
    }

    /**
     * Retrieves a node specified by local name and namespace URI.
     * <br>Per [<a href='http://www.w3.org/TR/1999/REC-xml-names-19990114/'>XML Namespaces</a>]
     * , applications must use the value null as the namespaceURI parameter
     * for methods if they wish to have no namespace.
     *
     * @param namespaceURI The namespace URI of the node to retrieve.
     * @param localName    The local name of the node to retrieve.
     * @return A <code>Node</code> (of any type) with the specified local
     *         name and namespace URI, or <code>null</code> if they do not
     *         identify any node in this map.
     * @throws org.w3c.dom.DOMException NOT_SUPPORTED_ERR: May be raised if the implementation does not
     *                                  support the feature "XML" and the language exposed through the
     *                                  Document does not support XML Namespaces (such as [<a href='http://www.w3.org/TR/1999/REC-html401-19991224/'>HTML 4.01</a>]).
     * @since DOM Level 2
     */
    @Override
    public final Attr getNamedItemNS( final String namespaceURI, final String localName )
    {
        return null;
    }


    /**
     * Immutable NamedNodeMap. Mutation isn't supported.
     *
     * @throws UnsupportedOperationException operation not supported.
     */
    @Override
    public final Attr setNamedItemNS( final Node arg )
    {
        throw new UnsupportedOperationException( "#setNamedItemNS not supported" );
    }

    /**
     * Immutable NamedNodeMap. Mutation isn't supported.
     *
     * @throws UnsupportedOperationException operation not supported.
     */
    @Override
    public final Attr removeNamedItemNS( final String namespaceURI, final String localName )
    {
        throw new UnsupportedOperationException( "#removeNamedItemNS not supported" );
    }
}
