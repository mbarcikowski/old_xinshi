/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.version;

import fr.matoeil.xinshi.xmpp.core.internal.version.VersionFactory;

public class Version
{

    private static final int HASH_CODE_MULT = 31;

    private final int major_;

    private final int minor_;

    public Version( int aMajor, int aMinor )
    {
        major_ = aMajor;
        minor_ = aMinor;
    }

    public int major()
    {
        return major_;
    }

    public int minor()
    {
        return minor_;
    }

    public static Version fromString( String s )
    {
        return VersionFactory.fromString( s );
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null || getClass() != o.getClass() )
        {
            return false;
        }

        Version version = (Version) o;

        return major_ == version.major_ && minor_ == version.minor_;

    }

    @Override
    public int hashCode()
    {
        int result = major_;
        result = HASH_CODE_MULT * result + minor_;
        return result;
    }

    @Override
    public String toString()
    {
        return String.format( "%d.%d", major_, minor_ );
    }
}
