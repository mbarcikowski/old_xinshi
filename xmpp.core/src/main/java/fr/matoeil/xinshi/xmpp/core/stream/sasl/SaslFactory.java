/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.sasl;

import com.google.common.base.Throwables;
import fr.matoeil.xinshi.xmpp.core.stream.StreamConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;
import java.io.IOException;

//TODO migrate to a builder
public class SaslFactory
{

    private final static XMLOutputFactory XML_OUTPUT_FACTORY = XMLOutputFactory.newFactory();

    private final static DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();

    private static DocumentBuilder DOCUMENT_BUILDER;

    static
    {
        try
        {
            DOCUMENT_BUILDER = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
        }
        catch ( ParserConfigurationException e )
        {
            throw Throwables.propagate( e );
        }
    }

    public static Element feature( String... aMechanisms )
        throws IOException, XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "", SaslConstants.MECHANISMS, SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( SaslConstants.SASL_NAMESPACE_URI );
        for ( String mechanism : aMechanisms )
        {
            streamWriter.writeStartElement( "", SaslConstants.MECHANISM, SaslConstants.SASL_NAMESPACE_URI );
            streamWriter.writeCharacters( mechanism );
            streamWriter.writeEndElement();
        }
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
        return doc.getDocumentElement();
    }

    public static Element fromChallenge( String aChallenge )
        throws IOException, XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "", SaslConstants.CHALLENGE, SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeCharacters( aChallenge );
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
        return doc.getDocumentElement();
    }

    public static Element failure( SaslConstants.Failure aFailure )
        throws IOException, XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "", SaslConstants.FAILURE, SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeStartElement( "", aFailure.toString(), SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeEndElement();
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
        return doc.getDocumentElement();
    }

    public static Element success()
        throws IOException, XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "", SaslConstants.SUCCESS, SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( SaslConstants.SASL_NAMESPACE_URI );
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
        return doc.getDocumentElement();
    }
}
