/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.UserDataHandler;

import javax.annotation.concurrent.Immutable;

@Immutable
public abstract class ImmutableNode
    implements Node
{

    @Override
    public final Document getOwnerDocument()
    {
        return null;
    }


    @Override
    public final Node insertBefore( final Node newChild, final Node refChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#insertBefore not supported" );
    }

    @Override
    public final Node replaceChild( final Node newChild, final Node oldChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#replaceChild not supported" );
    }

    @Override
    public final Node removeChild( final Node oldChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#removeChild not supported" );
    }

    @Override
    public final Node appendChild( final Node newChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#appendChild not supported" );
    }


    /**
     * Not supported since Node is an immutable object, we don't need to clone id
     *
     * @param deep
     * @return nothing
     * @throws UnsupportedOperationException
     */
    @Override
    public final Node cloneNode( final boolean deep )
    {
        throw new UnsupportedOperationException( "ImmutableNode#cloneNode not supported" );
    }

    @Override
    public final void normalize()
    {
        throw new UnsupportedOperationException( "ImmutableNode#normalize not supported" );
    }

    @Override
    public final boolean isSupported( final String feature, final String version )
    {
        return false;
    }

    @Override
    public final void setPrefix( final String prefix )
    {
        throw new UnsupportedOperationException( "ImmutableNode#setPrefix not supported" );
    }


    @Override
    public final String getBaseURI()
    {
        return null;
    }

    @Override
    public final void setTextContent( final String textContent )
    {
        throw new UnsupportedOperationException( "ImmutableNode#setTextContent not supported" );
    }

    @Override
    public final boolean isSameNode( final Node other )
    {
        return this == other;
    }

    @Override
    public final Object getFeature( final String feature, final String version )
    {
        return null;
    }

    @Override
    public final Object setUserData( final String key, final Object data, final UserDataHandler handler )
    {
        throw new UnsupportedOperationException( "ImmutableNode#setUserData not supported" );
    }

    @Override
    public final Object getUserData( final String key )
    {
        return null;
    }
}
