/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.address;

public class Address
{
    public static final String DOMAIN_SEPARATOR = "@";

    public static final String RESOURCE_SEPARATOR = "/";

    private final String node_;

    private final String domain_;

    private final String resource_;

    private final String fullAddress_;

    private final String bareAddress_;

    public Address( String aNode, String aDomain, String aResource )
    {
        node_ = aNode;
        domain_ = aDomain;
        resource_ = aResource;

        StringBuilder sb = buildBareAddress();
        bareAddress_ = sb.toString();
        fullAddress_ = buildFullAddress( sb ).toString();
    }

    public String node()
    {
        return node_;
    }

    public String domain()
    {
        return domain_;
    }

    public String resource()
    {
        return resource_;
    }

    public String bareAddress()
    {
        return bareAddress_;
    }

    public String fullAddress()
    {
        return fullAddress_;
    }

    public boolean hasNode()
    {
        return !node_.isEmpty();
    }

    public boolean hasResource()
    {
        return !resource_.isEmpty();
    }

    @Override
    public String toString()
    {
        return fullAddress_;
    }

    private StringBuilder buildBareAddress()
    {

        // Cache the bare JID
        boolean hasNode = hasNode();
        int size = hasNode ? 0 : ( node_.length() + 1 );
        size += domain_.length();
        size += hasResource() ? 0 : ( resource_.length() + 1 );
        StringBuilder addressBuffer = new StringBuilder( size );
        if ( hasNode )
        {
            addressBuffer.append( node_ ).append( DOMAIN_SEPARATOR );
        }
        addressBuffer.append( domain_ );
        return addressBuffer;

    }

    private StringBuilder buildFullAddress( StringBuilder anAddressBuffer )
    {
        if ( hasResource() )
        {
            anAddressBuffer.append( RESOURCE_SEPARATOR ).append( resource_ );
        }
        return anAddressBuffer;
    }
}
