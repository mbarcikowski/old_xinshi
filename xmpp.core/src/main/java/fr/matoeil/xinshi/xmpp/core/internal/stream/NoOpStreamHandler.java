/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import uk.org.retep.niosax.NioSaxParserHandler;

public class NoOpStreamHandler
    implements ContentHandler, LexicalHandler, NioSaxParserHandler
{
    @Override
    public void setDocumentLocator( Locator locator )
    {
        //no op
    }

    @Override
    public void startDocument()
        throws SAXException
    {
        //no op
    }

    @Override
    public void endDocument()
        throws SAXException
    {
        //no op
    }

    @Override
    public void startPrefixMapping( String prefix, String uri )
        throws SAXException
    {
        //no op
    }

    @Override
    public void endPrefixMapping( String prefix )
        throws SAXException
    {
        //no op
    }

    @Override
    public void startElement( String uri, String localName, String qName, Attributes atts )
        throws SAXException
    {
        //no op
    }

    @Override
    public void endElement( String uri, String localName, String qName )
        throws SAXException
    {
        //no op
    }

    @Override
    public void characters( char[] ch, int start, int length )
        throws SAXException
    {
        //no op
    }

    @Override
    public void ignorableWhitespace( char[] ch, int start, int length )
        throws SAXException
    {
        //no op
    }

    @Override
    public void processingInstruction( String target, String data )
        throws SAXException
    {
        //no op
    }

    @Override
    public void skippedEntity( String name )
        throws SAXException
    {
        //no op
    }

    @Override
    public void startDTD( String name, String publicId, String systemId )
        throws SAXException
    {
        //no op
    }

    @Override
    public void endDTD()
        throws SAXException
    {
        //no op
    }

    @Override
    public void startEntity( String name )
        throws SAXException
    {
        //no op
    }

    @Override
    public void endEntity( String name )
        throws SAXException
    {
        //no op
    }

    @Override
    public void startCDATA()
        throws SAXException
    {
        //no op
    }

    @Override
    public void endCDATA()
        throws SAXException
    {
        //no op
    }

    @Override
    public void comment( char[] ch, int start, int length )
        throws SAXException
    {
        //no op
    }

    @Override
    public void xmlDeclaration( String versionInfo, String encoding, boolean standalone )
    {
        //no op
    }
}
