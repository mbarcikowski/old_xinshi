/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableArrayAttrMap;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableElement;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNamedNodeMap;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.Type;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

import static fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants.Attributes.*;

@Immutable
public abstract class Stanza<T extends Type>
    extends ImmutableElement
    implements Element
{

    private static final int COMMON_ATTRIBUTES_COUNT = 4;

    private final Address from_;

    private final Address to_;

    private final String id_;

    private final T type_;

    //TODO check Arguments... ?
    // namespaceURI = 'jabber:client' or 'jabber:server'
    //TODO xml:lang ?
    //TODO stanza error
    //TODO extended content

    Stanza( final String aNamespaceURI, final String aLocalName, @Nullable final Address aFrom,
            @Nullable final Address aTo, @Nullable final String aId, @Nullable final T aType )
    {
        super( aNamespaceURI, aLocalName );

        from_ = aFrom;
        to_ = aTo;
        id_ = aId;
        type_ = aType;
        setImmutableAttributes( generateAttributes() );
    }

    @Nullable
    public final Address getFrom()
    {
        return from_;
    }

    @Nullable
    public final Address getTo()
    {
        return to_;
    }

    @Nullable
    public final String getId()
    {
        return id_;
    }

    @Nullable
    public final T getType()
    {
        return type_;
    }

    @Override
    public final Node getParentNode()
    {
        return null;
    }

    @Override
    public final Node getPreviousSibling()
    {
        return null;
    }

    @Override
    public final Node getNextSibling()
    {
        return null;
    }

    @Override
    public final String getTextContent()
    {
        //TODO Stanza#getTextContent not implemented
        throw new UnsupportedOperationException( "Stanza#getTextContent not implemented" );
    }


    @Override
    protected final ImmutableNamedNodeMap generateAttributes()
    {
        List<ImmutableAttribute> attributes = new ArrayList<>( COMMON_ATTRIBUTES_COUNT );
        generateAttribute( attributes, FROM, from_ );
        generateAttribute( attributes, TO, to_ );
        generateAttribute( attributes, ID, id_ );
        generateAttribute( attributes, TYPE, type_ );
        return new ImmutableArrayAttrMap( attributes.toArray( new Attr[attributes.size()] ) );
    }

    private void generateAttribute( List<ImmutableAttribute> aAttributes, String aLocalName, Object aValue )
    {
        if ( aValue != null )
        {
            aAttributes.add( new ImmutableAttribute( this, null, null, aLocalName, aValue.toString() ) );
        }
    }
}
