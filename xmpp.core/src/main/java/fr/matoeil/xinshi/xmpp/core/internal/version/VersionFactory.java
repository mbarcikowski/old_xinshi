/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.version;

import fr.matoeil.xinshi.xmpp.core.version.Version;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class VersionFactory
{

    private static final int NB_VERSION_COMPONENTS = 2;

    public static Version fromString( String s )
    {
        checkNotNull( s, "version cannot be null" );
        String[] split = s.split( "[.]" );
        checkArgument( split.length == NB_VERSION_COMPONENTS, "version must be formatted as \"major.minor\"" );
        return new Version( Integer.parseInt( split[0] ), Integer.parseInt( split[1] ) );
    }
}
