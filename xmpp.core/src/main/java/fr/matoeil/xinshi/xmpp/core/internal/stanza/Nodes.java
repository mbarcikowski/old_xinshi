/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import com.google.common.base.Joiner;

import javax.annotation.Nullable;

public final class Nodes
{

    private Nodes()
    {

    }

    public static final ImmutableNodeList EMPTY_CHILD_NODES = new EmptyImmutableNodeList();

    public static final String getQualifiedName( @Nullable final String aPrefix, final String aLocalName )
    {
        return Joiner.on( ':' ).skipNulls().join( aPrefix, aLocalName );
    }
}
