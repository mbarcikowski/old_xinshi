/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream;

import com.ctc.wstx.stax.WstxInputFactory;
import fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants;
import org.w3c.dom.Element;
import org.xml.sax.helpers.NamespaceSupport;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMSource;

public class StreamSerializer
{

    private final static XMLInputFactory XML_INPUT_FACTORY = new WstxInputFactory();

    public static void encodeStanza( Element aStanza, XMLStreamWriter aStreamWriter )
        throws XMLStreamException
    {
        NamespaceSupport namespaceSupport = new NamespaceSupport();
        namespaceSupport.declarePrefix( "", ClientContentConstants.CLIENT_CONTENT_NAMESPACE_URI );
        XMLStreamReader streamReader = XML_INPUT_FACTORY.createXMLStreamReader( new DOMSource( aStanza ) );
        while ( streamReader.hasNext() )
        {
            int type = streamReader.next();
            switch ( type )
            {
                case XMLStreamReader.START_ELEMENT:
                {
                    writeStartElement( streamReader, aStreamWriter, namespaceSupport );
                    break;
                }
                case XMLStreamReader.CDATA:
                {
                    aStreamWriter.writeCData( streamReader.getText() );
                    break;
                }
                case XMLStreamReader.CHARACTERS:
                {
                    if ( !streamReader.isWhiteSpace() )
                    {
                        aStreamWriter.writeCharacters( streamReader.getText() );
                    }
                    break;
                }
                case XMLStreamReader.END_ELEMENT:
                {
                    writeEndElement( aStreamWriter, namespaceSupport );
                    break;
                }
                default:
                    //nothing to do
            }
        }
        streamReader.close();
    }

    private static void writeStartElement( XMLStreamReader aReader, XMLStreamWriter aWriter,
                                           NamespaceSupport aNamespaceSupport )
        throws XMLStreamException
    {
        aNamespaceSupport.pushContext();
        aWriter.writeStartElement( aReader.getPrefix(), aReader.getLocalName(), aReader.getNamespaceURI() );
        writeNamespaces( aReader, aWriter, aNamespaceSupport );
        writeAttributes( aReader, aWriter );
    }

    private static void writeNamespaces( XMLStreamReader aReader, XMLStreamWriter aWriter,
                                         NamespaceSupport aNamespaceSupport )
        throws XMLStreamException
    {
        for ( int index = 0, count = aReader.getNamespaceCount(); index < count; index++ )
        {
            String prefix = aReader.getNamespacePrefix( index );
            String uri = aReader.getNamespaceURI( index );
            String declaredURI = aNamespaceSupport.getURI( prefix );
            if ( !uri.equals( declaredURI ) )
            {
                aNamespaceSupport.declarePrefix( prefix, uri );
                aWriter.writeNamespace( prefix, uri );
            }
        }
    }

    private static void writeAttributes( XMLStreamReader aReader, XMLStreamWriter aWriter )
        throws XMLStreamException
    {
        for ( int index = 0, count = aReader.getAttributeCount(); index < count; index++ )
        {
            QName attribute = aReader.getAttributeName( index );
            String attributeValue = aReader.getAttributeValue( index );
            aWriter.writeAttribute( attribute.getPrefix(), attribute.getNamespaceURI(), attribute.getLocalPart(),
                                    attributeValue );
        }
    }

    private static void writeEndElement( XMLStreamWriter aStreamWriter, NamespaceSupport aNamespaceSupport )
        throws XMLStreamException
    {
        aStreamWriter.writeEndElement();
        aNamespaceSupport.popContext();
    }
}
