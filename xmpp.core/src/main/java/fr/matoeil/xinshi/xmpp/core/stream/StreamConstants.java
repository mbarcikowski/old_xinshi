/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

public final class StreamConstants
{
    public static final String XML_VERSION = "1.0";

    public static final String XML_ENCODING = "UTF-8";


    public static final String STREAM_NAMESPACE_URI = "http://etherx.jabber.org/streams";

    public static final String STREAM_PREFIX = "stream";

    public static final String STREAM = "stream";

    public static final String FEATURES = "features";

    public static class Attributes
    {
        public static final String FROM = "from";

        public static final String ID = "id";

        public static final String TO = "to";

        public static final String VERSION = "version";

        public static final String LANG = "lang";
    }

    private StreamConstants()
    {

    }
}
