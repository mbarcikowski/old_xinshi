/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.content;

public class StanzaErrorConstants
{
    public static final String STANZA_ERROR_NAMESPACE_URI = "urn:ietf:params:xml:ns:xmpp-stanzas";

    public static final String ERROR = "error";

    public static final String TEXT = "text";

    public static class Attributes
    {
        public static final String TYPE = "type";

        public static final String BY = "by";
    }

    public static enum Type
    {
        AUTH
            {
                @Override
                public String toString()
                {
                    return "auth";
                }
            },
        CANCEL
            {
                @Override
                public String toString()
                {
                    return "cancel";
                }
            },
        CONTINUE
            {
                @Override
                public String toString()
                {
                    return "continue";
                }
            },
        MODIFY
            {
                @Override
                public String toString()
                {
                    return "modify";
                }
            },
        WAIT
            {
                @Override
                public String toString()
                {
                    return "wait";
                }
            }
    }

    public static enum Condition
    {
        BAD_REQUEST
            {
                @Override
                public String toString()
                {
                    return "bad-request";
                }
            },
        CONFLICT
            {
                @Override
                public String toString()
                {
                    return "conflict";
                }
            },
        FEATURE_NOT_IMPLEMENTED
            {
                @Override
                public String toString()
                {
                    return "feature-not-implemented";
                }
            },
        FORBIDDEN
            {
                @Override
                public String toString()
                {
                    return "forbidden";
                }
            },
        GONE
            {
                @Override
                public String toString()
                {
                    return "gone";
                }
            },
        INTERNAL_SERVER_ERROR
            {
                @Override
                public String toString()
                {
                    return "internal-server-error";
                }
            },
        ITEM_NOT_FOUND
            {
                @Override
                public String toString()
                {
                    return "item-not-found";
                }
            },
        JID_MALFORMED
            {
                @Override
                public String toString()
                {
                    return "jid-malformed";
                }
            },
        NOT_ACCEPTABLE
            {
                @Override
                public String toString()
                {
                    return "not-acceptable";
                }
            },
        NOT_ALLOWED
            {
                @Override
                public String toString()
                {
                    return "not-allowed";
                }
            },
        NOT_AUTHORIZED
            {
                @Override
                public String toString()
                {
                    return "not-authorized";
                }
            },
        POLICY_VIOLATION
            {
                @Override
                public String toString()
                {
                    return "policy-violation";
                }
            },
        RECIPIENT_UNAVAILABLE
            {
                @Override
                public String toString()
                {
                    return "recipient-unavailable";
                }
            },
        REDIRECT
            {
                @Override
                public String toString()
                {
                    return "redirect";
                }
            },
        REGISTRATION_REQUIRED
            {
                @Override
                public String toString()
                {
                    return "registration-required";
                }
            },
        REMOTE_SERVER_NOT_FOUND
            {
                @Override
                public String toString()
                {
                    return "remote-server-not-found";
                }
            },
        REMOTE_SERVER_TIMEOUT
            {
                @Override
                public String toString()
                {
                    return "remote-server-timeout";
                }
            },
        RESOURCE_CONSTRAINT
            {
                @Override
                public String toString()
                {
                    return "resource-constraint";
                }
            },
        SERVICE_UNAVAILABLE
            {
                @Override
                public String toString()
                {
                    return "service-unavailable";
                }
            },
        SUBSCRIPTION_REQUIRED
            {
                @Override
                public String toString()
                {
                    return "subscription-required";
                }
            },
        UNDEFINED_CONDITION
            {
                @Override
                public String toString()
                {
                    return "undefined-condition";
                }
            },
        UNEXPECTED_REQUEST
            {
                @Override
                public String toString()
                {
                    return "unexpected-request";
                }
            }
    }
}





