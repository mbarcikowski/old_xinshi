/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

public class StreamException
    extends RuntimeException
{
    private final StreamErrorConstants.Error error_;

    public StreamException( StreamErrorConstants.Error aError )
    {
        super();
        error_ = aError;
    }

    public StreamException( StreamErrorConstants.Error aError, String message )
    {
        super( message );
        error_ = aError;
    }

    public StreamException( StreamErrorConstants.Error aError, String message, Throwable cause )
    {
        super( message, cause );
        error_ = aError;
    }

    public StreamException( StreamErrorConstants.Error aError, Throwable cause )
    {
        super( cause );
        error_ = aError;
    }

    public StreamErrorConstants.Error error()
    {
        return error_;
    }
}
