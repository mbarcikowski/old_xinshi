/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;

import javax.annotation.concurrent.Immutable;

@Immutable
public abstract class ImmutableElement
    extends ImmutableNode
    implements Element
{
    private final String localName_;

    private final String namespaceURI_;

    private ImmutableNamedNodeMap attributes_;

    private ImmutableNodeList childNodes_;

    //TODO missing prefix
    public ImmutableElement( final String aNamespaceURI, final String aLocalName )
    {
        namespaceURI_ = aNamespaceURI;
        localName_ = aLocalName;
    }

    protected final ImmutableNamedNodeMap getImmutableAttributes()
    {
        return attributes_;
    }

    protected final void setImmutableAttributes( ImmutableNamedNodeMap aImmutableAttributes )
    {
        attributes_ = aImmutableAttributes;
    }

    protected final ImmutableNodeList getImmutableChildNodes()
    {
        return childNodes_;
    }

    protected final void setImmutableChildNodes( ImmutableNodeList aImmutableChildNodes )
    {
        childNodes_ = aImmutableChildNodes;
    }

    @Override
    public final void setAttribute( final String name, final String value )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setAttribute not supported" );
    }

    @Override
    public final void removeAttribute( final String name )
    {
        throw new UnsupportedOperationException( "ImmutableElement#removeAttribute not supported" );
    }

    @Override
    public final Attr setAttributeNode( final Attr newAttr )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setAttributeNode not supported" );
    }

    @Override
    public final Attr removeAttributeNode( final Attr oldAttr )
    {
        throw new UnsupportedOperationException( "ImmutableElement#removeAttributeNode not supported" );
    }


    @Override
    public final void setAttributeNS( final String namespaceURI, final String qualifiedName, final String value )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setAttributeNS not implemented" );
    }

    @Override
    public final void removeAttributeNS( final String namespaceURI, final String localName )
    {
        throw new UnsupportedOperationException( "ImmutableElement#removeAttributeNS not supported" );
    }

    @Override
    public final Attr setAttributeNodeNS( final Attr newAttr )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setAttributeNodeNS not supported" );
    }

    @Override
    public final TypeInfo getSchemaTypeInfo()
    {
        throw new UnsupportedOperationException( "ImmutableElement#setIdAttribute not supported" );
    }

    @Override
    public final void setIdAttribute( final String name, final boolean isId )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setIdAttribute not supported" );
    }

    @Override
    public final void setIdAttributeNS( final String namespaceURI, final String localName, final boolean isId )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setIdAttributeNS not supported" );
    }

    @Override
    public final void setIdAttributeNode( final Attr idAttr, final boolean isId )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setIdAttributeNode not supported" );
    }

    @Override
    public final void setNodeValue( final String nodeValue )
    {
        throw new UnsupportedOperationException( "ImmutableElement#setNodeValue not supported" );
    }

    @Override
    public final String getNodeValue()
    {
        return null;
    }

    @Override
    public final short getNodeType()
    {
        return Node.ELEMENT_NODE;
    }

    //TODO full qualified name
    @Override
    public final String getTagName()
    {
        return localName_;
    }

    @Override
    public final String getAttribute( final String aName )
    {
        return Elements.getAttribute( attributes_, aName );
    }

    @Override
    public final Attr getAttributeNode( final String aName )
    {
        return attributes_.getNamedItem( aName );
    }

    @Override
    public final NodeList getElementsByTagName( final String name )
    {
        //TODO implements Body#getElementsByTagName
        throw new UnsupportedOperationException( "Body#getElementsByTagName not implemented" );
    }

    @Override
    public final String getAttributeNS( final String aNamespaceURI, final String aLocalName )
    {
        return Elements.getAttributeNS( attributes_, aNamespaceURI, aLocalName );
    }

    @Override
    public final Attr getAttributeNodeNS( final String aNamespaceURI, final String aLocalName )
    {
        return attributes_.getNamedItemNS( aNamespaceURI, aLocalName );
    }

    @Override
    public final NodeList getElementsByTagNameNS( final String namespaceURI, final String localName )
    {
        //TODO implements Body#getElementsByTagNameNS
        throw new UnsupportedOperationException( "Body#getElementsByTagNameNS not implemented" );
    }

    @Override
    public final boolean hasAttribute( final String aName )
    {
        return getAttributeNode( aName ) != null;
    }

    @Override
    public final boolean hasAttributeNS( final String aNamespaceURI, final String aLocalName )
    {
        return getAttributeNodeNS( aNamespaceURI, aLocalName ) != null;
    }

    //TODO full qualified name
    @Override
    public final String getNodeName()
    {
        return localName_;
    }

    @Override
    public final NodeList getChildNodes()
    {
        return childNodes_;
    }

    @Override
    public final Node getFirstChild()
    {
        return childNodes_.item( 0 );
    }

    @Override
    public final Node getLastChild()
    {
        return childNodes_.item( childNodes_.getLength() - 1 );
    }

    @Override
    public final NamedNodeMap getAttributes()
    {
        return attributes_;
    }

    @Override
    public final boolean hasChildNodes()
    {
        return childNodes_.getLength() > 0;
    }

    @Override
    public final String getNamespaceURI()
    {
        return namespaceURI_;
    }

    //TODO missing prefix
    @Override
    public final String getPrefix()
    {
        return null;
    }

    @Override
    public final String getLocalName()
    {
        return localName_;
    }

    @Override
    public final boolean hasAttributes()
    {
        return attributes_.getLength() > 0;
    }

    @Override
    public final short compareDocumentPosition( final Node other )
    {
        //TODO implements Body#compareDocumentPosition
        throw new UnsupportedOperationException( "Body#compareDocumentPosition not implemented" );
    }

    @Override
    public final String lookupPrefix( final String namespaceURI )
    {
        //TODO implements Body#lookupPrefix
        throw new UnsupportedOperationException( "Body#lookupPrefix not implemented" );
    }

    @Override
    public final boolean isDefaultNamespace( final String aNamespaceURI )
    {
        //TODO implements Body#isDefaultNamespace
        throw new UnsupportedOperationException( "Body#isDefaultNamespace not implemented" );
    }

    @Override
    public final String lookupNamespaceURI( final String aPrefix )
    {
        //TODO implements Body#lookupNamespaceURI
        throw new UnsupportedOperationException( "Body#lookupNamespaceURI not implemented" );
    }

    @Override
    public final boolean isEqualNode( final Node arg )
    {
        //TODO implements Body#isEqualNode
        throw new UnsupportedOperationException( "Body#isEqualNode not implemented" );
    }

    protected abstract ImmutableNamedNodeMap generateAttributes();

    protected abstract ImmutableNodeList generateChildNodes();
}
