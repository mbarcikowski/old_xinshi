/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.address;

import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stringprep.Stringprep;
import fr.matoeil.xinshi.xmpp.core.internal.stringprep.StringprepException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class AddressFactory
{
    private static final int NB_PORTION = 3;

    private static final String MISSING_DOMAIN_PART_ERROR = "missing domain part in xmpp address '%s'";

    private static final String DOMAIN_SEPARATOR = "@";

    private static final String RESOURCE_SEPARATOR = "/";

    public static Address fromString( String s )
    {
        checkNotNull( s, "address cannot be null" );
        try
        {
            String[] portions = split( s );
            return new Address( portions[0], portions[1], portions[2] );
        }
        catch ( StringprepException e )
        {
            throw new IllegalArgumentException( e.getMessage(), e );
        }
    }

    static String[] split( String s )
        throws StringprepException
    {
        String[] portions = new String[NB_PORTION];

        int atIndex = parseNode( s, portions );
        int slashIndex = parseDomain( s, atIndex, portions );
        parseResource( s, portions, slashIndex );

        return portions;
    }

    private static int parseNode( String s, String[] aPortions )
        throws StringprepException
    {
        String node;

        int atIndex = s.indexOf( DOMAIN_SEPARATOR );

        if ( atIndex <= 0 )
        {
            node = "";
        }
        else
        {
            node = Stringprep.nodeprep( s.substring( 0, atIndex ) );
        }

        aPortions[0] = node;
        return atIndex;
    }

    private static int parseDomain( String s, int aAtIndex, String[] aPortions )
        throws StringprepException
    {
        String domain;

        int slashIndex = s.indexOf( RESOURCE_SEPARATOR );

        checkArgument( aAtIndex < s.length(), MISSING_DOMAIN_PART_ERROR, s );
        if ( aAtIndex < 0 )
        {
            if ( slashIndex > 0 )
            {
                domain = s.substring( 0, slashIndex );
            }
            else
            {
                domain = s;
            }
        }
        else
        {
            if ( slashIndex > 0 )
            {
                domain = s.substring( aAtIndex + 1, slashIndex );
            }
            else
            {
                domain = s.substring( aAtIndex + 1 );
            }
        }

        aPortions[1] = Stringprep.nameprep( domain );
        return slashIndex;
    }

    private static void parseResource( String s, String[] aPortions, int aSlashIndex )
        throws StringprepException
    {
        String resource;
        if ( aSlashIndex + 1 > s.length() || aSlashIndex < 0 )
        {
            resource = "";
        }
        else
        {
            resource = Stringprep.resourceprep( s.substring( aSlashIndex + 1 ) );
        }
        aPortions[2] = resource;
    }
}
