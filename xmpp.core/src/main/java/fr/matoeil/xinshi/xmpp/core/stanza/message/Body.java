/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableLocalizedContentLeaf;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public class Body
    extends ImmutableLocalizedContentLeaf
    implements Element
{

    private static final String LOCAL_NAME = "body";


    Body( final ImmutableNode aParentNode, final int aElementIndex, final String aNamespaceURI,
          @Nullable final String aContent )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, LOCAL_NAME, aContent );
    }

    public static final class Builder
        extends ImmutableLocalizedContentLeaf.Builder<Body>
    {

        @Override
        protected Body newImmutableLocalizedContentLeaf( ImmutableNode aParentNode, Integer aElementIndex,
                                                         String aNamespaceURI, @Nullable String aContent )
        {
            return new Body( aParentNode, aElementIndex, aNamespaceURI, aContent );
        }
    }
}
