/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream;

import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.version.Version;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import static fr.matoeil.xinshi.xmpp.core.stream.StreamConstants.Attributes.*;
import static fr.matoeil.xinshi.xmpp.core.stream.StreamConstants.STREAM;
import static fr.matoeil.xinshi.xmpp.core.stream.StreamConstants.STREAM_NAMESPACE_URI;
import static javax.xml.XMLConstants.XML_NS_URI;

final class StreamDeserializer
{

    public static Header decodeHeader( Element aElement )
    {
        Address from = null;
        String id = null;
        Address to = null;
        Version version = null;
        String xmlLang = null;

        String name = aElement.getLocalName();
        String namespaceURI = aElement.getNamespaceURI();
        if ( !STREAM_NAMESPACE_URI.equals( namespaceURI ) )
        {
            throw new StreamException( StreamErrorConstants.Error.INVALID_NAMESPACE );
        }
        if ( !STREAM.equals( name ) )
        {
            throw new StreamException( StreamErrorConstants.Error.INVALID_XML );
        }
        Attr fromAttribute = aElement.getAttributeNode( FROM );
        if ( fromAttribute != null )
        {
            try
            {
                from = Addresses.fromString( fromAttribute.getValue() );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        Attr toAttribute = aElement.getAttributeNode( TO );
        if ( toAttribute != null )
        {
            try
            {
                to = Addresses.fromString( toAttribute.getValue() );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        Attr versionAttribute = aElement.getAttributeNode( VERSION );
        if ( versionAttribute != null )
        {
            try
            {
                version = Version.fromString( versionAttribute.getValue() );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        Attr idAttribute = aElement.getAttributeNode( ID );
        if ( idAttribute != null )
        {
            id = idAttribute.getValue();
        }
        Attr xmlLangAttribute = aElement.getAttributeNodeNS( XML_NS_URI, LANG );
        if ( xmlLangAttribute != null )
        {
            xmlLang = xmlLangAttribute.getValue();
        }
        if ( version == null )
        {
            throw new StreamException( StreamErrorConstants.Error.UNSUPPORTED_VERSION );
        }
        return new Header( version, from, to, id, xmlLang );
    }
}
