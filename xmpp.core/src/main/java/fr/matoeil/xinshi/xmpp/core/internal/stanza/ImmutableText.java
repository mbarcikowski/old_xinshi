/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.w3c.dom.Node;
import org.w3c.dom.Text;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class ImmutableText
    extends ImmutableCharacterData
    implements Text
{

    private static final String NODE_NAME = "#text";

    private final Node parent_;

    public ImmutableText( final ImmutableNode aParent, final String aData )
    {
        super( aData );
        parent_ = aParent;
    }

    @Override
    public Text splitText( final int offset )
    {
        throw new UnsupportedOperationException( "ImmutableText#splitText not supported" );
    }

    @Override
    public boolean isElementContentWhitespace()
    {
        //TODO ImmutableText#isElementContentWhitespace not implemented
        throw new UnsupportedOperationException( "ImmutableText#isElementContentWhitespace not implemented" );
    }

    @Override
    public String getWholeText()
    {
        //TODO ImmutableText#getWholeText not implemented
        throw new UnsupportedOperationException( "ImmutableText#getWholeText not implemented" );
    }

    @Override
    public Text replaceWholeText( final String content )
    {
        throw new UnsupportedOperationException( "ImmutableText#replaceWholeText not supported" );
    }

    @Override
    public String getNodeName()
    {
        return NODE_NAME;
    }

    @Override
    public short getNodeType()
    {
        return Node.TEXT_NODE;
    }

    @Override
    public Node getParentNode()
    {
        return parent_;
    }

    @Override
    public Node getPreviousSibling()
    {
        //TODO ImmutableText#getPreviousSibling not implemented
        throw new UnsupportedOperationException( "ImmutableText#getPreviousSibling not implemented" );
    }

    @Override
    public Node getNextSibling()
    {
        //TODO ImmutableText#getNextSibling not implemented
        throw new UnsupportedOperationException( "ImmutableText#getNextSibling not implemented" );
    }

    @Override
    public short compareDocumentPosition( final Node other )
    {
        //TODO ImmutableText#compareDocumentPosition not implemented
        throw new UnsupportedOperationException( "ImmutableText#compareDocumentPosition not implemented" );
    }

    @Override
    public String lookupPrefix( final String namespaceURI )
    {
        //TODO ImmutableText#lookupPrefix not implemented
        throw new UnsupportedOperationException( "ImmutableText#lookupPrefix not implemented" );
    }

    @Override
    public boolean isDefaultNamespace( final String namespaceURI )
    {
        //TODO ImmutableText#isDefaultNamespace not implemented
        throw new UnsupportedOperationException( "ImmutableText#isDefaultNamespace not implemented" );
    }

    @Override
    public String lookupNamespaceURI( final String prefix )
    {
        //TODO ImmutableText#lookupNamespaceURI not implemented
        throw new UnsupportedOperationException( "ImmutableText#lookupNamespaceURI not implemented" );
    }
}
