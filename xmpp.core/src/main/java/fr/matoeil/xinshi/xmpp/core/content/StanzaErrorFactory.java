/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.content;

import com.google.common.base.Throwables;
import fr.matoeil.xinshi.xmpp.core.stream.StreamConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;

import static fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants.CLIENT_CONTENT_NAMESPACE_URI;
import static fr.matoeil.xinshi.xmpp.core.content.StanzaErrorConstants.Attributes.TYPE;
import static fr.matoeil.xinshi.xmpp.core.content.StanzaErrorConstants.*;

//TODO migrate to a builder
public class StanzaErrorFactory
{
    private final static XMLOutputFactory XML_OUTPUT_FACTORY = XMLOutputFactory.newFactory();

    private final static DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();

    private static DocumentBuilder DOCUMENT_BUILDER;

    static
    {
        try
        {
            DOCUMENT_BUILDER = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
        }
        catch ( ParserConfigurationException e )
        {
            throw Throwables.propagate( e );
        }
    }

    //TODO optional by
    //TODO optional text
    //TODO optional inner text for gone & redirect
    //TODO OPTIONAL application-specific condition element
    public static Element newError( Type aType, Condition aCondition )
        throws XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "", ERROR, CLIENT_CONTENT_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( CLIENT_CONTENT_NAMESPACE_URI );
        streamWriter.writeAttribute( TYPE, aType.toString() );
        streamWriter.writeStartElement( "", aCondition.toString(), STANZA_ERROR_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( STANZA_ERROR_NAMESPACE_URI );
        streamWriter.writeEndElement();
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
        return doc.getDocumentElement();
    }
}
