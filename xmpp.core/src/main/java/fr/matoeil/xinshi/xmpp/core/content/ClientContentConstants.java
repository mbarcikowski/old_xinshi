/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.content;

import com.google.common.collect.Maps;

import javax.annotation.CheckForNull;
import java.util.Map;

public class ClientContentConstants
{
    public static final String CLIENT_CONTENT_NAMESPACE_URI = "jabber:client";

    public static final String IQ = "iq";

    public static final String PRESENCE = "presence";

    public static final String MESSAGE = "message";


    public static class Attributes
    {
        public static final String ID = "id";

        public static final String TYPE = "type";

        public static final String FROM = "from";

        public static final String TO = "to";
    }

    //TODO use type according stanza localName
    //TODO make type for iq, message & presence
    public static enum Type
    {
        RESULT
            {
                @Override
                public String toString()
                {
                    return "result";
                }
            },
        ERROR
            {
                @Override
                public String toString()
                {
                    return "error";
                }
            },
        SET
            {
                @Override
                public String toString()
                {
                    return "set";
                }
            },
        GET
            {
                @Override
                public String toString()
                {
                    return "get";
                }
            };

        @CheckForNull
        public static Type fromString( String s )
        {
            return toStringMap.get( s );
        }

        private final static Map<String, Type> toStringMap = Maps.newHashMap();

        static
        {
            for ( Type aType : Type.values() )
            {
                toStringMap.put( aType.toString(), aType );
            }
        }
    }
}
