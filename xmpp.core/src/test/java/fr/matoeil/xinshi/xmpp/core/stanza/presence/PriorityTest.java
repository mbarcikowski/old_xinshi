/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class PriorityTest
{

    private Priority target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final byte expectedValue = Byte.valueOf( "0" );

    private final String expectedLocalName = "priority";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Priority( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedValue );
    }

    @After
    public void tearDown()
        throws Exception
    {

    }

    @Test
    public void testGetValue()
        throws Exception
    {
        byte actualValue = target_.getValue();

        assertThat( actualValue, is( equalTo( expectedValue ) ) );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetAttribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNode()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNS()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNodeNS()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testHasAttribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }


    @Test
    public void testHasAttributeNS()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        Node actual = target_.getLastChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( Byte.toString( expectedValue ) ) ) );
    }

    //TODO BuilderTest
    public static class BuilderTest
    {
        @Ignore
        @Test
        public void testBuild()
        {

        }
    }

}
