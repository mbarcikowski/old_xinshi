/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants.Attributes.FROM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableLocalizedContentLeafTest
{

    private ImmutableLocalizedContentLeaf target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedLocalName = "expectedLocalName";

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedContent = "a content";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableLocalizedContentLeaf( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI,
                                                     expectedLocalName, expectedContent );
    }

    @After
    public void tearDown()
        throws Exception
    {

    }

    @Test
    public void testGetContent()
        throws Exception
    {
        String actualContent = target_.getContent();

        assertThat( actualContent, is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Ignore
    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        // TODO testGetAttribute_with_know_attribute not implemented
        /*String actual = target_.getAttribute(FROM);

        assertThat(actual, is(equalTo(expectedFrom.fullAddress())));*/
    }

    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Ignore
    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        // TODO testGetAttributeNode_with_know_attribute not implemented
        /*Attr actual = target_.getAttributeNode(FROM);

        assertThat(actual.getNodeName(), is(equalTo(FROM)));
        assertThat(actual.getNodeValue(), is(equalTo(expectedFrom.fullAddress())));*/
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Ignore
    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {
        // TODO testGetAttributeNS_with_know_attribute not implemented
        /*String actual = target_.getAttributeNS(null, FROM);

        assertThat(actual, is(equalTo(expectedFrom.fullAddress())));*/
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Ignore
    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        // TODO testGetAttributeNodeNS_with_know_attribute not implemented
       /* Attr actual = target_.getAttributeNodeNS(null, FROM);

        assertThat(actual.getNodeName(), is(equalTo(FROM)));
        assertThat(actual.getNodeValue(), is(equalTo(expectedFrom.fullAddress())));
        */
    }

    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Ignore
    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        //TODO testHasAttribute_with_know_attribute
        boolean actual = target_.hasAttribute( FROM );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Ignore
    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        //TODO testHasAttributeNS_with_know_attribute
        boolean actual = target_.hasAttributeNS( null, FROM );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        Node actual = target_.getLastChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Ignore
    @Test
    public void testGetAttributes()
        throws Exception
    {
        //TODO testGetAttributes
        NamedNodeMap actual = target_.getAttributes();
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Ignore
    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedContent ) ) );
    }

    //TODO BuilderTest
    public static class BuilderTest
    {
        @Ignore
        @Test
        public void testBuild()
        {

        }
    }
}
