/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Body;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Subject;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Thread;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static fr.matoeil.xinshi.xmpp.core.stanza.Message.Type;
import static fr.matoeil.xinshi.xmpp.core.stanza.Message.Type.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

public class MessageTest
{

    private Message target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedLocalName = "message";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final Type expectedType = Type.CHAT;

    private final int expectedBodyElementIndex = 0;

    private final String expectedBodyContent = "a content";

    private final Body expectedBody =
        new Body.Builder().isChildOf( mockedImmutableNode_ ).atIndex( expectedBodyElementIndex ).inNamespace(
            expectedNamespaceURI ).withContent( expectedBodyContent ).build();


    private final List<Body> expectedBodies = new ImmutableList.Builder<Body>().add( expectedBody ).build();

    private final int expectedSubjectElementIndex = 1;

    private final String expectedSubjectContent = "a content";

    private final Subject expectedSubject =
        new Subject.Builder().isChildOf( mockedImmutableNode_ ).atIndex( expectedSubjectElementIndex ).inNamespace(
            expectedNamespaceURI ).withContent( expectedSubjectContent ).build();

    private final List<Subject> expectedSubjects = new ImmutableList.Builder<Subject>().add( expectedSubject ).build();

    private final int expectedThreadElementIndex = 2;

    private final String expectedThreadParentId = "parent_id";

    private final String expectedThreadId = "thread_id";

    private final fr.matoeil.xinshi.xmpp.core.stanza.message.Thread expectedThread =
        new Thread.Builder().isChildOf( mockedImmutableNode_ ).atIndex( expectedThreadElementIndex ).inNamespace(
            expectedNamespaceURI ).hasParentThread( expectedThreadParentId ).identifiedBy( expectedThreadId ).build();

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Message( expectedNamespaceURI, expectedFrom, expectedTo, expectedId, expectedType, expectedBodies,
                               expectedSubjects, expectedThread );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetBodies()
        throws Exception
    {
        List<Body> actualBodies = target_.getBodies();

        assertThat( actualBodies, is( equalTo( expectedBodies ) ) );
    }

    @Test
    public void testGetSubjects()
        throws Exception
    {
        List<Subject> actualSubjects = target_.getSubjects();

        assertThat( actualSubjects, is( equalTo( expectedSubjects ) ) );
    }

    @Test
    public void testGetThread()
        throws Exception
    {
        Thread actualThread = target_.getThread();

        assertThat( actualThread, is( equalTo( expectedThread ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    public static class TypeTest
    {

        @Test
        public void testERRORAsString()
            throws Exception
        {
            String actual = ERROR.asString();

            assertThat( actual, is( equalTo( "error" ) ) );
        }

        @Test
        public void testCHATAsString()
            throws Exception
        {
            String actual = CHAT.asString();

            assertThat( actual, is( equalTo( "chat" ) ) );
        }

        @Test
        public void testGROUPCHATAsString()
            throws Exception
        {
            String actual = GROUPCHAT.asString();

            assertThat( actual, is( equalTo( "groupchat" ) ) );
        }

        @Test
        public void testHEADLINEAsString()
            throws Exception
        {
            String actual = HEADLINE.asString();

            assertThat( actual, is( equalTo( "headline" ) ) );
        }

        @Test
        public void testNORMALAsString()
            throws Exception
        {
            String actual = NORMAL.asString();

            assertThat( actual, is( equalTo( "normal" ) ) );
        }

        @Test
        public void testToString()
            throws Exception
        {
            for ( Type type : Type.values() )
            {
                String actual = type.toString();
                String expected = type.asString();
                assertThat( actual, is( equalTo( expected ) ) );
            }
        }
    }
}
