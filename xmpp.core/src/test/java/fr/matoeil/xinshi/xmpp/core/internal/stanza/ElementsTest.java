/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class ElementsTest
{

    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target_ = Elements.class.getDeclaredConstructor();

        assertThat( target_.isAccessible(), is( equalTo( false ) ) );

        target_.setAccessible( true );
        target_.newInstance();
    }

    @Test
    public void testGetAttribute_with_not_existing_attribute()
        throws Exception
    {
        String actual = Elements.getAttribute( Elements.EMPTY_ATTRIBUTES, "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_not_existing_attribute()
        throws Exception
    {
        String actual = Elements.getAttributeNS( Elements.EMPTY_ATTRIBUTES, "namespace", "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_existing_attribute()
        throws Exception
    {
        String expectedValue = "value";
        ImmutableOneAttrMap attrMap =
            new ImmutableOneAttrMap( new ImmutableAttribute( null, "namespace", null, "name", expectedValue ) );

        String actual = Elements.getAttribute( attrMap, "name" );

        assertThat( actual, is( equalTo( expectedValue ) ) );
    }

    @Test
    public void testGetAttributeNS_with_existing_attribute()
        throws Exception
    {
        String expectedValue = "value";
        ImmutableOneAttrMap attrMap =
            new ImmutableOneAttrMap( new ImmutableAttribute( null, "namespace", null, "name", expectedValue ) );

        String actual = Elements.getAttributeNS( attrMap, "namespace", "name" );

        assertThat( actual, is( equalTo( expectedValue ) ) );
    }

    @Test
    public void testEMPTY_ATTRIBUTES()
        throws Exception
    {
        ImmutableNamedNodeMap actual = Elements.EMPTY_ATTRIBUTES;

        assertThat( actual, is( instanceOf( EmptyImmutableNamedNodeMap.class ) ) );
    }
}
