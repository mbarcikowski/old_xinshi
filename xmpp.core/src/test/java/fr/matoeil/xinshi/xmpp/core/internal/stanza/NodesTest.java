/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class NodesTest
{

    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = Nodes.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testgetQualifiedName_with_prefix()
        throws Exception
    {
        String prefix = "prefix";
        String localName = "localName";
        String expectedQualifiedName = prefix + ":" + localName;

        String actual = Nodes.getQualifiedName( prefix, localName );

        assertThat( actual, is( equalTo( expectedQualifiedName ) ) );

    }

    @Test
    public void testgetQualifiedName_without_prefix()
        throws Exception
    {
        String prefix = null;
        String localName = "localName";
        String expectedQualifiedName = localName;

        String actual = Nodes.getQualifiedName( prefix, localName );

        assertThat( actual, is( equalTo( expectedQualifiedName ) ) );

    }

    @Test
    public void testEMPTY_CHILD_NODES()
        throws Exception
    {

        ImmutableNodeList actual = Nodes.EMPTY_CHILD_NODES;

        assertThat( actual, is( instanceOf( EmptyImmutableNodeList.class ) ) );
    }
}
