/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableArrayNodeListTest
{
    private ImmutableArrayNodeList target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final ImmutableNode[] expectedNodes = new ImmutableNode[]{ mockedImmutableNode };


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableArrayNodeList( expectedNodes );
    }

    @Test
    public void testItem_with_valid_index()
        throws Exception
    {
        Node actual = target_.item( expectedNodes.length - 1 );

        assertThat( actual, is( equalTo( (Node) mockedImmutableNode ) ) );
    }

    @Test
    public void testItem_with_invalid_index()
        throws Exception
    {
        Node actual = target_.item( expectedNodes.length );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetLength()
        throws Exception
    {
        int actual = target_.getLength();

        assertThat( actual, is( equalTo( expectedNodes.length ) ) );
    }
}
