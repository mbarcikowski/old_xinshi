/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ThreadTest
{
    private static final String PARENT = "parent";

    private fr.matoeil.xinshi.xmpp.core.stanza.message.Thread target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedParent = "parent";

    private final String expectedContent = "a content";

    private final String expectedLocalName = "thread";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedParent,
                              expectedContent );
    }

    @After
    public void tearDown()
        throws Exception
    {

    }

    @Test
    public void testGetParent()
        throws Exception
    {
        String actualParent = target_.getParentId();

        assertThat( actualParent, is( equalTo( expectedParent ) ) );
    }

    @Test
    public void testGetContent()
        throws Exception
    {
        String actualContent = target_.getId();

        assertThat( actualContent, is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetAttribute_with_parent_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( PARENT );

        assertThat( actual, is( equalTo( expectedParent ) ) );
    }

    @Test
    public void testGetAttribute_without_parent_attribute()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, null, expectedContent );

        String actual = target_.getAttribute( PARENT );

        assertThat( actual, isEmptyString() );
    }


    @Test
    public void testGetAttributeNode_with_parent_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( PARENT );

        assertThat( actual.getNodeName(), is( equalTo( PARENT ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedParent ) ) );
    }

    @Test
    public void testGetAttributeNode_without_parent_attribute()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, null, expectedContent );

        Attr actual = target_.getAttributeNode( PARENT );

        assertThat( actual, is( nullValue() ) );
    }


    @Test
    public void testGetAttributeNS_with_parent_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( null, PARENT );

        assertThat( actual, is( equalTo( expectedParent ) ) );
    }

    @Test
    public void testGetAttributeNS_without_parent_attribute()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, null, expectedContent );

        String actual = target_.getAttributeNS( null, PARENT );

        assertThat( actual, isEmptyString() );
    }


    @Test
    public void testGetAttributeNodeNS_with_parent_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( null, PARENT );

        assertThat( actual.getNodeName(), is( equalTo( PARENT ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedParent ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_without_parent_attribute()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, null, expectedContent );

        Attr actual = target_.getAttributeNodeNS( null, PARENT );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testHasAttribute_with_parent_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( PARENT );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttribute_without_parent_attribute()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, null, expectedContent );

        boolean actual = target_.hasAttribute( PARENT );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_parent_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( null, PARENT );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_without_parent_attribute()
        throws Exception
    {
        target_ = new Thread( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, null, expectedContent );

        boolean actual = target_.hasAttributeNS( null, PARENT );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        Node actual = target_.getLastChild();

        assertThat( actual, is( notNullValue() ) );
    }


    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedContent ) ) );
    }

    //TODO BuilderTest
    public static class BuilderTest
    {
        @Ignore
        @Test
        public void testBuild()
        {

        }
    }
}
