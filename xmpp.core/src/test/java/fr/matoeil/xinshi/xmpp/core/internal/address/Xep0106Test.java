package fr.matoeil.xinshi.xmpp.core.internal.address;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class Xep0106Test
{
    @Test
    public void testEscapeNodePart()
        throws Exception
    {
        assertThat( Xep0106.escapeNodePart( "" ), is( equalTo( "" ) ) );
        assertThat( Xep0106.escapeNodePart( "\\2plus\\2is\\4" ), is( equalTo( "\\2plus\\2is\\4" ) ) );
        assertThat( Xep0106.escapeNodePart( "foo\\bar" ), is( equalTo( "foo\\bar" ) ) );
        assertThat( Xep0106.escapeNodePart( "foob\\41r" ), is( equalTo( "foob\\41r" ) ) );

        assertThat( Xep0106.escapeNodePart( "space cadet" ), is( equalTo( "space\\20cadet" ) ) );
        assertThat( Xep0106.escapeNodePart( "call me \"ishmael\"" ), is( equalTo( "call\\20me\\20\\22ishmael\\22" ) ) );
        assertThat( Xep0106.escapeNodePart( "at&t guy" ), is( equalTo( "at\\26t\\20guy" ) ) );
        assertThat( Xep0106.escapeNodePart( "d'artagnan" ), is( equalTo( "d\\27artagnan" ) ) );
        assertThat( Xep0106.escapeNodePart( "/.fanboy" ), is( equalTo( "\\2f.fanboy" ) ) );
        assertThat( Xep0106.escapeNodePart( "::foo::" ), is( equalTo( "\\3a\\3afoo\\3a\\3a" ) ) );
        assertThat( Xep0106.escapeNodePart( "<foo>" ), is( equalTo( "\\3cfoo\\3e" ) ) );
        assertThat( Xep0106.escapeNodePart( "user@host" ), is( equalTo( "user\\40host" ) ) );
        assertThat( Xep0106.escapeNodePart( "c:\\net" ), is( equalTo( "c\\3a\\net" ) ) );
        assertThat( Xep0106.escapeNodePart( "c:\\\\net" ), is( equalTo( "c\\3a\\\\net" ) ) );
        assertThat( Xep0106.escapeNodePart( "c:\\cool stuff" ), is( equalTo( "c\\3a\\cool\\20stuff" ) ) );
        assertThat( Xep0106.escapeNodePart( "c:\\5commas" ), is( equalTo( "c\\3a\\5c5commas" ) ) );

        assertThat( Xep0106.escapeNodePart( "\" &'/:<>@\\" ),
                    is( equalTo( "\\22\\20\\26\\27\\2f\\3a\\3c\\3e\\40\\" ) ) );
        assertThat( Xep0106.escapeNodePart( "\\22\\20\\26\\27\\2f\\3a\\3c\\3e\\40\\5c" ),
                    is( equalTo( "\\5c22\\5c20\\5c26\\5c27\\5c2f\\5c3a\\5c3c\\5c3e\\5c40\\5c5c" ) ) );
        assertThat( Xep0106.escapeNodePart( "\\2\\3\\4\\5\\5" ), is( equalTo( "\\2\\3\\4\\5\\5" ) ) );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEscapeNodeStartWithSpace()
        throws Exception
    {
        Xep0106.escapeNodePart( " " );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEscapeNodeEndWithSpace()
        throws Exception
    {
        Xep0106.escapeNodePart( "a " );
    }

    @Test
    public void testUnescapeNodePart()
        throws Exception
    {
        assertThat( Xep0106.unescapeNodePart( "" ), is( equalTo( "" ) ) );
        assertThat( Xep0106.unescapeNodePart( "\\2plus\\2is\\4" ), is( equalTo( "\\2plus\\2is\\4" ) ) );
        assertThat( Xep0106.unescapeNodePart( "foo\\bar" ), is( equalTo( "foo\\bar" ) ) );
        assertThat( Xep0106.unescapeNodePart( "foob\\41r" ), is( equalTo( "foob\\41r" ) ) );

        assertThat( Xep0106.unescapeNodePart( "space\\20cadet" ), is( equalTo( "space cadet" ) ) );
        assertThat( Xep0106.unescapeNodePart( "call\\20me\\20\\22ishmael\\22" ),
                    is( equalTo( "call me \"ishmael\"" ) ) );
        assertThat( Xep0106.unescapeNodePart( "at\\26t\\20guy" ), is( equalTo( "at&t guy" ) ) );
        assertThat( Xep0106.unescapeNodePart( "d\\27artagnan" ), is( equalTo( "d'artagnan" ) ) );
        assertThat( Xep0106.unescapeNodePart( "\\2f.fanboy" ), is( equalTo( "/.fanboy" ) ) );
        assertThat( Xep0106.unescapeNodePart( "\\3a\\3afoo\\3a\\3a" ), is( equalTo( "::foo::" ) ) );
        assertThat( Xep0106.unescapeNodePart( "\\3cfoo\\3e" ), is( equalTo( "<foo>" ) ) );
        assertThat( Xep0106.unescapeNodePart( "user\\40host" ), is( equalTo( "user@host" ) ) );
        assertThat( Xep0106.unescapeNodePart( "c\\3a\\net" ), is( equalTo( "c:\\net" ) ) );
        assertThat( Xep0106.unescapeNodePart( "c\\3a\\\\net" ), is( equalTo( "c:\\\\net" ) ) );
        assertThat( Xep0106.unescapeNodePart( "c\\3a\\cool\\20stuff" ), is( equalTo( "c:\\cool stuff" ) ) );
        assertThat( Xep0106.unescapeNodePart( "c\\3a\\5c5commas" ), is( equalTo( "c:\\5commas" ) ) );

        assertThat( Xep0106.unescapeNodePart( "\\22\\20\\26\\27\\2f\\3a\\3c\\3e\\40\\" ),
                    is( equalTo( "\" &'/:<>@\\" ) ) );
        assertThat( Xep0106.unescapeNodePart( "\\5c22\\5c20\\5c26\\5c27\\5c2f\\5c3a\\5c3c\\5c3e\\5c40\\5c5c" ),
                    is( equalTo( "\\22\\20\\26\\27\\2f\\3a\\3c\\3e\\40\\5c" ) ) );
        assertThat( Xep0106.unescapeNodePart( "\\2\\3\\4\\5\\5" ), is( equalTo( "\\2\\3\\4\\5\\5" ) ) );
    }
}
