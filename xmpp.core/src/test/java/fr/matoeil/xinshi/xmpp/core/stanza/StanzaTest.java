/*
 * Copyright 2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNodeList;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import static fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants.Attributes.FROM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class StanzaTest
{

    private Stanza<Message.Type> target_;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedLocalName = "expectedLocalName";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final Message.Type expectedType = Message.Type.CHAT;

    private final ImmutableNodeList mockedChildNodes = mock( ImmutableNodeList.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new Stanza<Message.Type>( expectedNamespaceURI, expectedLocalName, expectedFrom, expectedTo, expectedId,
                                      expectedType )
            {
                @Override
                protected ImmutableNodeList generateChildNodes()
                {
                    setImmutableChildNodes( mockedChildNodes );
                    return mockedChildNodes;
                }
            };
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetFrom()
        throws Exception
    {
        Address actual = target_.getFrom();

        assertThat( actual, is( equalTo( expectedFrom ) ) );

    }

    @Test
    public void testGetTo()
        throws Exception
    {
        Address actual = target_.getTo();

        assertThat( actual, is( equalTo( expectedTo ) ) );
    }

    @Test
    public void testGetId()
        throws Exception
    {
        String actual = target_.getId();

        assertThat( actual, is( equalTo( expectedId ) ) );

    }

    @Test
    public void testGetType()
        throws Exception
    {
        fr.matoeil.xinshi.xmpp.core.internal.stanza.Type actual = target_.getType();

        assertThat( actual, is( equalTo( (fr.matoeil.xinshi.xmpp.core.internal.stanza.Type) expectedType ) ) );
    }


    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( FROM );

        assertThat( actual, is( equalTo( expectedFrom.fullAddress() ) ) );
    }


    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( FROM );

        assertThat( actual.getNodeName(), is( equalTo( FROM ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedFrom.fullAddress() ) ) );
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( null, FROM );

        assertThat( actual, is( equalTo( expectedFrom.fullAddress() ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( null, FROM );

        assertThat( actual.getNodeName(), is( equalTo( FROM ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedFrom.fullAddress() ) ) );
    }


    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( FROM );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( null, FROM );

        assertThat( actual, is( equalTo( true ) ) );
    }


    @Test
    public void testGetParentNode()
        throws Exception
    {
        Node actual = target_.getParentNode();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetPreviousSibling()
        throws Exception
    {
        Node actual = target_.getPreviousSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNextSibling()
        throws Exception
    {
        Node actual = target_.getNextSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Ignore
    @Test
    public void testGetAttributes()
        throws Exception
    {
        //TODO testGetAttributes
        NamedNodeMap actual = target_.getAttributes();
    }


    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributes_with_stanza_without_any_attributes()
        throws Exception
    {
        Stanza target = new Stanza( expectedNamespaceURI, expectedLocalName, null, null, null, null )
        {
            @Override
            protected ImmutableNodeList generateChildNodes()
            {
                setImmutableChildNodes( mockedChildNodes );
                return mockedChildNodes;
            }
        };
        boolean actual = target.hasAttributes();

        assertThat( actual, is( equalTo( false ) ) );
    }


    @Test(expected = UnsupportedOperationException.class)
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();
    }
}
