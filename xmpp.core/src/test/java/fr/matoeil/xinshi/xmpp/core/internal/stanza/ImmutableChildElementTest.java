/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableChildElementTest
{

    private ImmutableChildElement target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "namespace";

    private final String expectedLocalName = "localName";

    private final String expectedTextContent = "a content";

    private final ImmutableNamedNodeMap mockedImmutableArrayAttrMap_ = mock( ImmutableNamedNodeMap.class );

    private final ImmutableNodeList mockedChildNodes = mock( ImmutableNodeList.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableChildElement( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI,
                                             expectedLocalName )
        {
            @Override
            protected ImmutableNamedNodeMap generateAttributes()
            {

                return mockedImmutableArrayAttrMap_;
            }

            @Override
            protected ImmutableNodeList generateChildNodes()
            {
                return mockedChildNodes;
            }

            @Override
            public String getTextContent()
                throws DOMException
            {
                return expectedTextContent;
            }
        };

        target_.setImmutableAttributes( target_.generateAttributes() );
        target_.setImmutableChildNodes( target_.generateChildNodes() );

        final NodeList mockedNodeList = mock( NodeList.class );
        when( mockedImmutableNode_.getChildNodes() ).thenReturn( mockedNodeList );
    }


    @Test
    public void testGetParentNode()
        throws Exception
    {
        Node actual = target_.getParentNode();

        assertThat( actual, is( equalTo( (Node) mockedImmutableNode_ ) ) );
    }

    @Test
    public void testGetPreviousSibling()
        throws Exception
    {
        Node actual = target_.getPreviousSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNextSibling()
        throws Exception
    {
        Node actual = target_.getNextSibling();
    }

    //TODO BuilderTest
    public static class BuilderTest
    {
        @Ignore
        @Test
        public void testBuild()
        {

        }
    }

}
