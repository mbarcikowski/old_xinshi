/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class EmptyImmutableNamedNodeMapTest
{
    private ImmutableNamedNodeMap target_ = Elements.EMPTY_ATTRIBUTES;

    @Test
    public void testGetNamedItem()
        throws Exception
    {
        Attr actual = target_.getNamedItem( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testSetNamedItem()
        throws Exception
    {
        final Node node = mock( Node.class );
        target_.setNamedItem( node );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testRemoveNamedItem()
        throws Exception
    {
        target_.removeNamedItem( "name" );
    }

    @Test
    public void testItem()
        throws Exception
    {
        Attr actual = target_.item( 0 );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetLength()
        throws Exception
    {
        int actual = target_.getLength();

        assertThat( actual, is( equalTo( 0 ) ) );
    }

    @Test
    public void testGetNamedItemNS()
        throws Exception
    {
        Attr actual = target_.getNamedItemNS( "namespace", "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testSetNamedItemNS()
        throws Exception
    {
        final Node node = mock( Node.class );
        target_.setNamedItemNS( node );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testRemoveNamedItemNS()
        throws Exception
    {
        target_.removeNamedItemNS( "namespace", "name" );

    }
}
