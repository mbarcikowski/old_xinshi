/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableAttributeTest
{

    private ImmutableAttribute target_;

    private final ImmutableElement mockedImmutableElement = mock( ImmutableElement.class );

    private final String expectedNamespaceURI = "namespaceURI";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "localName";

    private final String expectedValue = "value";

    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new ImmutableAttribute( mockedImmutableElement, expectedNamespaceURI, expectedPrefix, expectedLocalName,
                                    expectedValue );
    }

    @Test
    public void testGetName()
        throws Exception
    {
        String actual = target_.getName();

        assertThat( actual, is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }

    @Test
    public void testGetSpecified()
        throws Exception
    {
        boolean actual = target_.getSpecified();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetValue()
        throws Exception
    {
        String actual = target_.getValue();

        assertThat( actual, is( equalTo( expectedValue ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testSetValue()
        throws Exception
    {
        target_.setValue( "value" );
    }

    @Test
    public void testGetOwnerElement()
        throws Exception
    {
        Element actual = target_.getOwnerElement();

        assertThat( actual, is( equalTo( (Element) mockedImmutableElement ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testGetSchemaTypeInfo()
        throws Exception
    {
        target_.getSchemaTypeInfo();
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testIsId()
        throws Exception
    {
        target_.isId();
    }

    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }

    @Test
    public void testGetNodeValue()
        throws Exception
    {
        String actual = target_.getNodeValue();

        assertThat( actual, is( equalTo( expectedValue ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testSetNodeValue()
        throws Exception
    {
        target_.setNodeValue( "value" );
    }

    @Test
    public void testGetNodeType()
        throws Exception
    {
        short actual = target_.getNodeType();

        assertThat( actual, is( Node.ATTRIBUTE_NODE ) );
    }

    @Test
    public void testGetParentNode()
        throws Exception
    {
        Node actual = target_.getParentNode();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual.getLength(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedValue ) ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        Node actual = target_.getLastChild();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedValue ) ) );
    }

    @Test
    public void testGetPreviousSibling()
        throws Exception
    {
        Node actual = target_.getPreviousSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNextSibling()
        throws Exception
    {
        Node actual = target_.getNextSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetPrefix()
        throws Exception
    {
        String actual = target_.getPrefix();

        assertThat( actual, is( equalTo( expectedPrefix ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testCompareDocumentPosition()
        throws Exception
    {
        Node mockedNode = mock( Node.class );
        target_.compareDocumentPosition( mockedNode );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedValue ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testLookupPrefix()
        throws Exception
    {
        target_.lookupPrefix( "namespace" );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testIsDefaultNamespace()
        throws Exception
    {
        target_.isDefaultNamespace( "namespace" );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testLookupNamespaceURI()
        throws Exception
    {
        target_.lookupNamespaceURI( "prefix" );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testIsEqualNode()
        throws Exception
    {
        Node mockedNode = mock( Node.class );
        target_.isEqualNode( mockedNode );
    }
}
