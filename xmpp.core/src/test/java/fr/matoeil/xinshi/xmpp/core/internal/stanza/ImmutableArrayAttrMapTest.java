/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableArrayAttrMapTest
{

    private ImmutableArrayAttrMap target_;

    private ImmutableElement mockedImmutableElement = mock( ImmutableElement.class );

    private String expectedNamespaceURI = "namespace";

    private String expectedPrefix = "prefix";

    private String expectedLocalName = "localName";

    private String expectedValue = "value";

    private ImmutableAttribute expectedImmutableAttribute =
        new ImmutableAttribute( mockedImmutableElement, expectedNamespaceURI, expectedPrefix, expectedLocalName,
                                expectedValue );

    private Attr[] expectedAttributes = new Attr[]{ expectedImmutableAttribute };

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableArrayAttrMap( expectedAttributes );

    }

    @Test
    public void testGetNamedItem_with_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItem( expectedPrefix + ":" + expectedLocalName );

        assertThat( actual, is( equalTo( (Attr) expectedImmutableAttribute ) ) );
    }

    @Test
    public void testGetNamedItem_with_non_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItem( "other" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testSetNamedItem()
        throws Exception
    {
        Node mockedNode = mock( Node.class );
        target_.setNamedItem( mockedNode );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testRemoveNamedItem()
        throws Exception
    {
        target_.removeNamedItem( expectedLocalName );
    }

    @Test
    public void testItem_with_existing_attribute()
        throws Exception
    {
        Attr actual = target_.item( expectedAttributes.length - 1 );

        assertThat( actual, is( equalTo( (Attr) expectedImmutableAttribute ) ) );
    }

    @Test
    public void testItem_with_non_existing_attribute()
        throws Exception
    {
        Attr actual = target_.item( expectedAttributes.length );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetLength()
        throws Exception
    {
        int actual = target_.getLength();

        assertThat( actual, is( equalTo( expectedAttributes.length ) ) );
    }


    @Test
    public void testGetNamedItemNS_with_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItemNS( expectedNamespaceURI, expectedLocalName );

        assertThat( actual, is( equalTo( (Attr) expectedImmutableAttribute ) ) );
    }

    @Test
    public void testGetNamedItemNS_with_non_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItemNS( "other", "other" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testSetNamedItemNS()
        throws Exception
    {
        Node mockedNode = mock( Node.class );
        target_.setNamedItemNS( mockedNode );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testRemoveNamedItemNS()
        throws Exception
    {
        target_.removeNamedItemNS( expectedNamespaceURI, expectedLocalName );
    }
}
