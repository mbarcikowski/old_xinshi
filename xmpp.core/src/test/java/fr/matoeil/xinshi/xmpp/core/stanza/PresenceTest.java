/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Priority;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static fr.matoeil.xinshi.xmpp.core.stanza.Presence.Type;
import static fr.matoeil.xinshi.xmpp.core.stanza.Presence.Type.*;
import static fr.matoeil.xinshi.xmpp.core.stanza.presence.Show.State.AWAY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

public class PresenceTest
{

    private Presence target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedLocalName = "presence";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final int expectedShowElementIndex = 0;

    private final Type expectedType = Type.UNAVAILABLE;

    private final Show expectedShow =
        new Show.Builder().isChildOf( mockedImmutableNode_ ).atIndex( expectedShowElementIndex ).inNamespace(
            expectedNamespaceURI ).withState( AWAY ).build();

    private final int expectedStatusElementIndex = 1;

    private final String expectedStatusContent = "a content";

    private final Status expectedStatus =
        new Status.Builder().isChildOf( mockedImmutableNode_ ).atIndex( expectedStatusElementIndex ).inNamespace(
            expectedNamespaceURI ).withContent( expectedStatusContent ).build();

    private final List<Status> expectedStatuses = new ImmutableList.Builder<Status>().add( expectedStatus ).build();

    private final int expectedPriorityElementIndex = 2;

    private final Priority expectedPriority =
        new Priority.Builder().isChildOf( mockedImmutableNode_ ).atIndex( expectedPriorityElementIndex ).inNamespace(
            expectedNamespaceURI ).withValue( Byte.valueOf( "0" ) ).build();

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Presence( expectedNamespaceURI, expectedFrom, expectedTo, expectedId, expectedType, expectedShow,
                                expectedStatuses, expectedPriority );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetShow()
        throws Exception
    {
        Show actualShow = target_.getShow();

        assertThat( actualShow, is( equalTo( expectedShow ) ) );
    }

    @Test
    public void testGetStatuses()
        throws Exception
    {
        List<Status> actualStatuses = target_.getStatuses();

        assertThat( actualStatuses, is( equalTo( expectedStatuses ) ) );
    }

    @Test
    public void testGetPriority()
        throws Exception
    {
        Priority actualPriority = target_.getPriority();

        assertThat( actualPriority, is( equalTo( expectedPriority ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    public static class TypeTest
    {

        @Test
        public void testERRORAsString()
            throws Exception
        {
            String actual = ERROR.asString();

            assertThat( actual, is( equalTo( "error" ) ) );
        }

        @Test
        public void testPROBEAsString()
            throws Exception
        {
            String actual = PROBE.asString();

            assertThat( actual, is( equalTo( "probe" ) ) );
        }

        @Test
        public void testSUBSCRIBEAsString()
            throws Exception
        {
            String actual = SUBSCRIBE.asString();

            assertThat( actual, is( equalTo( "subscribe" ) ) );
        }

        @Test
        public void testSUBSCRIBEDAsString()
            throws Exception
        {
            String actual = SUBSCRIBED.asString();

            assertThat( actual, is( equalTo( "subscribed" ) ) );
        }

        @Test
        public void testUNAVAILABLEAsString()
            throws Exception
        {
            String actual = UNAVAILABLE.asString();

            assertThat( actual, is( equalTo( "unavailable" ) ) );
        }

        @Test
        public void testUNSUBSCRIBEAsString()
            throws Exception
        {
            String actual = UNSUBSCRIBE.asString();

            assertThat( actual, is( equalTo( "unsubscribe" ) ) );
        }

        @Test
        public void testUNSUBSCRIBEDAsString()
            throws Exception
        {
            String actual = UNSUBSCRIBED.asString();

            assertThat( actual, is( equalTo( "unsubscribed" ) ) );
        }

        @Test
        public void testToString()
            throws Exception
        {
            for ( Type type : Type.values() )
            {
                String actual = type.toString();
                String expected = type.asString();
                assertThat( actual, is( equalTo( expected ) ) );
            }
        }
    }
}
