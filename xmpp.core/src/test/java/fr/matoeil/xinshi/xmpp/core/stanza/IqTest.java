/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.core.address.Address;
import fr.matoeil.xinshi.xmpp.core.address.Addresses;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static fr.matoeil.xinshi.xmpp.core.stanza.Iq.Type;
import static fr.matoeil.xinshi.xmpp.core.stanza.Iq.Type.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class IqTest
{

    private Iq target_;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedLocalName = "iq";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final Type expectedType = Type.GET;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Iq( expectedNamespaceURI, expectedFrom, expectedTo, expectedId, expectedType );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    public static class TypeTest
    {

        @Test
        public void testERRORAsString()
            throws Exception
        {
            String actual = Type.ERROR.asString();

            assertThat( actual, is( equalTo( "error" ) ) );
        }

        @Test
        public void testGETAsString()
            throws Exception
        {
            String actual = GET.asString();

            assertThat( actual, is( equalTo( "get" ) ) );
        }

        @Test
        public void testSETAsString()
            throws Exception
        {
            String actual = SET.asString();

            assertThat( actual, is( equalTo( "set" ) ) );
        }

        @Test
        public void testRESULTAsString()
            throws Exception
        {
            String actual = RESULT.asString();

            assertThat( actual, is( equalTo( "result" ) ) );
        }

        @Test
        public void testToString()
            throws Exception
        {
            for ( Type type : Type.values() )
            {
                String actual = type.toString();
                String expected = type.asString();
                assertThat( actual, is( equalTo( expected ) ) );
            }
        }
    }
}
