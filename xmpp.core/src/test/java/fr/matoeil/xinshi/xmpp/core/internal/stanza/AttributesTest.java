/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class AttributesTest
{

    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = Attributes.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testATTRIBUTE_NOT_DEFINED_VALUE()
        throws Exception
    {
        assertThat( Attributes.ATTRIBUTE_NOT_DEFINED_VALUE, isEmptyString() );
    }
}
