/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ImmutableOneAttrMapTest
{

    private ImmutableOneAttrMap target_;

    private final ImmutableAttribute expectedImmutableAttribute =
        new ImmutableAttribute( null, "namespace", null, "name", "value" );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableOneAttrMap( expectedImmutableAttribute );
    }

    @Test
    public void testGetNamedItem_with_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItem( "name" );

        assertThat( actual, is( equalTo( (Attr) expectedImmutableAttribute ) ) );
    }

    @Test
    public void testGetNamedItem_with_not_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItem( "not" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetNamedItem()
        throws Exception
    {
        final Node node = mock( Node.class );

        target_.setNamedItem( node );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveNamedItem()
        throws Exception
    {
        target_.removeNamedItem( "name" );
    }

    @Test
    public void testItem_with_valid_index()
        throws Exception
    {
        Attr actual = target_.item( 0 );

        assertThat( actual, is( equalTo( (Attr) expectedImmutableAttribute ) ) );
    }

    @Test
    public void testItem_with_invalid_index()
        throws Exception
    {
        Attr actual = target_.item( 1 );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetLength()
        throws Exception
    {
        int actual = target_.getLength();

        assertThat( actual, is( equalTo( 1 ) ) );
    }

    @Test
    public void testGetNamedItemNS_with_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItemNS( "namespace", "name" );

        assertThat( actual, is( equalTo( (Attr) expectedImmutableAttribute ) ) );
    }

    @Test
    public void testGetNamedItemNS_with_not_existing_attribute()
        throws Exception
    {
        Attr actual = target_.getNamedItemNS( "namespace", "not" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetNamedItemNS()
        throws Exception
    {
        final Node node = mock( Node.class );

        target_.setNamedItemNS( node );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveNamedItemNS()
        throws Exception
    {
        target_.removeNamedItemNS( "namespace", "name" );
    }
}
