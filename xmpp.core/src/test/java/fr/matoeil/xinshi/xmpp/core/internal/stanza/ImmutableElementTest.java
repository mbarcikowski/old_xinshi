/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ImmutableElementTest
{

    private ImmutableElement target_;

    private final String expectedNamespaceURI = "namespace";

    private final String expectedLocalName = "localName";

    private final ImmutableNamedNodeMap mockedImmutableArrayAttrMap_ = mock( ImmutableNamedNodeMap.class );

    private final ImmutableNodeList mockedChildNodes = mock( ImmutableNodeList.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableElement( expectedNamespaceURI, expectedLocalName )
        {


            @Override
            public Node getParentNode()
            {
                throw new UnsupportedOperationException( "#getParentNode not implemented" );
            }

            @Override
            public Node getPreviousSibling()
            {
                throw new UnsupportedOperationException( "#getPreviousSibling not implemented" );
            }

            @Override
            public Node getNextSibling()
            {
                throw new UnsupportedOperationException( "#getNextSibling not implemented" );
            }

            @Override
            public String getTextContent()
                throws DOMException
            {
                throw new UnsupportedOperationException( "#getTextContent not implemented" );
            }

            @Override
            protected ImmutableNamedNodeMap generateAttributes()
            {
                return mockedImmutableArrayAttrMap_;
            }

            @Override
            protected ImmutableNodeList generateChildNodes()
            {
                return mockedChildNodes;
            }
        };

        target_.setImmutableAttributes( target_.generateAttributes() );
        target_.setImmutableChildNodes( target_.generateChildNodes() );
    }

    @Test
    public void testGetImmutableAttributes()
        throws Exception
    {
        ImmutableNamedNodeMap actual = target_.getImmutableAttributes();

        assertThat( actual, is( equalTo( mockedImmutableArrayAttrMap_ ) ) );
    }

    @Test
    public void testGetImmutableChildNodes()
        throws Exception
    {
        ImmutableNodeList actual = target_.getImmutableChildNodes();

        assertThat( actual, is( equalTo( mockedChildNodes ) ) );

    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetAttribute()
        throws Exception
    {
        target_.setAttribute( "name", "value" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveAttribute()
        throws Exception
    {
        target_.removeAttribute( "name" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetAttributeNode()
        throws Exception
    {
        Attr newAttr = mock( Attr.class );

        target_.setAttributeNode( newAttr );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveAttributeNode()
        throws Exception
    {
        Attr oldAttr = mock( Attr.class );

        target_.removeAttributeNode( oldAttr );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetAttributeNS()
        throws Exception
    {
        target_.setAttributeNS( "namespace", "qualifiedName", "value" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveAttributeNS()
        throws Exception
    {
        target_.removeAttributeNS( "namespace", "localName" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetAttributeNodeNS()
        throws Exception
    {
        Attr newAttr = mock( Attr.class );

        target_.setAttributeNodeNS( newAttr );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetSchemaTypeInfo()
        throws Exception
    {
        TypeInfo actual = target_.getSchemaTypeInfo();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetIdAttribute()
        throws Exception
    {
        target_.setIdAttribute( "name", false );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetIdAttributeNS()
        throws Exception
    {
        target_.setIdAttributeNS( "namespace", "localName", false );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetIdAttributeNode()
        throws Exception
    {
        Attr idAttr = mock( Attr.class );

        target_.setIdAttributeNode( idAttr, false );
    }

    @Test
    public void testGetNodeValue()
        throws Exception
    {
        String actual = target_.getNodeValue();

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetNodeValue()
        throws Exception
    {
        target_.setNodeValue( "value" );
    }

    @Test
    public void testGetNodeType()
        throws Exception
    {
        short actual = target_.getNodeType();

        assertThat( actual, is( equalTo( Node.ELEMENT_NODE ) ) );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetAttribute()
        throws Exception
    {
        String name = "name";
        target_.getAttribute( name );

        verify( mockedImmutableArrayAttrMap_ ).getNamedItem( eq( name ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetElementsByTagName()
        throws Exception
    {
        NodeList actual = target_.getElementsByTagName( "name" );
    }

    @Test
    public void testGetAttributeNS()
        throws Exception
    {
        String localName = "localName";
        String namespaceURI = "namespaceURI";
        target_.getAttributeNS( namespaceURI, localName );

        verify( mockedImmutableArrayAttrMap_ ).getNamedItemNS( eq( namespaceURI ), eq( localName ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetElementsByTagNameNS()
        throws Exception
    {
        NodeList actual = target_.getElementsByTagNameNS( "namespace", "localName" );
    }

    @Test
    public void testHasAttribute()
        throws Exception
    {
        String name = "name";
        target_.hasAttribute( name );

        verify( mockedImmutableArrayAttrMap_ ).getNamedItem( eq( name ) );
    }

    @Test
    public void testHasAttributeNS()
        throws Exception
    {
        String localName = "localName";
        String namespaceURI = "namespaceURI";
        target_.hasAttributeNS( namespaceURI, localName );

        verify( mockedImmutableArrayAttrMap_ ).getNamedItemNS( eq( namespaceURI ), eq( localName ) );
    }

    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }


    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( equalTo( (NodeList) mockedChildNodes ) ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        verify( mockedChildNodes ).item( eq( 0 ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        final Integer length = 1;
        when( mockedChildNodes.getLength() ).thenReturn( length );
        Node actual = target_.getLastChild();

        verify( mockedChildNodes ).getLength();
        verify( mockedChildNodes ).item( eq( length - 1 ) );
    }


    @Test
    public void testGetAttributes()
        throws Exception
    {
        //TODO testGetAttributes
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( equalTo( (NamedNodeMap) mockedImmutableArrayAttrMap_ ) ) );
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        verify( mockedChildNodes ).getLength();
    }


    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetPrefix()
        throws Exception
    {
        String actual = target_.getPrefix();

        assertThat( actual, is( nullValue() ) );
    }


    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testHasAttributes()
        throws Exception
    {
        target_.hasAttributes();

        verify( mockedImmutableArrayAttrMap_ ).getLength();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCompareDocumentPosition()
        throws Exception
    {
        Node node = mock( Node.class );

        short actual = target_.compareDocumentPosition( node );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testLookupPrefix()
        throws Exception
    {
        String actual = target_.lookupPrefix( "namespace" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsDefaultNamespace()
        throws Exception
    {
        boolean actual = target_.isDefaultNamespace( "namespace" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testLookupNamespaceURI()
        throws Exception
    {
        String actual = target_.lookupNamespaceURI( null );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsEqualNode()
        throws Exception
    {
        Node node = mock( Node.class );

        boolean actual = target_.isEqualNode( node );
    }

}
