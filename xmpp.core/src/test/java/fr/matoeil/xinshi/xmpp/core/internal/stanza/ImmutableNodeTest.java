/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.concurrent.Immutable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

@Immutable
public class ImmutableNodeTest
{

    private ImmutableNode target_ = new ImmutableNode()
    {
        @Override
        public String getNodeName()
        {
            throw new UnsupportedOperationException( "#getNodeName not implemented" );
        }

        @Override
        public String getNodeValue()
            throws DOMException
        {
            throw new UnsupportedOperationException( "#getNodeValue not implemented" );
        }

        @Override
        public void setNodeValue( final String nodeValue )
            throws DOMException
        {
            throw new UnsupportedOperationException( "#setNodeValue not implemented" );
        }

        @Override
        public short getNodeType()
        {
            throw new UnsupportedOperationException( "#getNodeType not implemented" );
        }

        @Override
        public Node getParentNode()
        {
            throw new UnsupportedOperationException( "#getParentNode not implemented" );
        }

        @Override
        public NodeList getChildNodes()
        {
            throw new UnsupportedOperationException( "#getChildNodes not implemented" );
        }

        @Override
        public Node getFirstChild()
        {
            throw new UnsupportedOperationException( "#getFirstChild not implemented" );
        }

        @Override
        public Node getLastChild()
        {
            throw new UnsupportedOperationException( "#getLastChild not implemented" );
        }

        @Override
        public Node getPreviousSibling()
        {
            throw new UnsupportedOperationException( "#getPreviousSibling not implemented" );
        }

        @Override
        public Node getNextSibling()
        {
            throw new UnsupportedOperationException( "#getNextSibling not implemented" );
        }

        @Override
        public NamedNodeMap getAttributes()
        {
            throw new UnsupportedOperationException( "#getAttributes not implemented" );
        }

        @Override
        public boolean hasChildNodes()
        {
            throw new UnsupportedOperationException( "#hasChildNodes not implemented" );
        }

        @Override
        public String getNamespaceURI()
        {
            throw new UnsupportedOperationException( "#getNamespaceURI not implemented" );
        }

        @Override
        public String getPrefix()
        {
            throw new UnsupportedOperationException( "#getPrefix not implemented" );
        }

        @Override
        public String getLocalName()
        {
            throw new UnsupportedOperationException( "#getLocalName not implemented" );
        }

        @Override
        public boolean hasAttributes()
        {
            throw new UnsupportedOperationException( "#hasAttributes not implemented" );
        }

        @Override
        public short compareDocumentPosition( final Node other )
            throws DOMException
        {
            throw new UnsupportedOperationException( "#compareDocumentPosition not implemented" );
        }

        @Override
        public String getTextContent()
            throws DOMException
        {
            throw new UnsupportedOperationException( "#getTextContent not implemented" );
        }

        @Override
        public String lookupPrefix( final String namespaceURI )
        {
            throw new UnsupportedOperationException( "#lookupPrefix not implemented" );
        }

        @Override
        public boolean isDefaultNamespace( final String namespaceURI )
        {
            throw new UnsupportedOperationException( "#isDefaultNamespace not implemented" );
        }

        @Override
        public String lookupNamespaceURI( final String prefix )
        {
            throw new UnsupportedOperationException( "#lookupNamespaceURI not implemented" );
        }

        @Override
        public boolean isEqualNode( final Node arg )
        {
            throw new UnsupportedOperationException( "#isEqualNode not implemented" );
        }
    };

    @Before
    public void setUp()
        throws Exception
    {

    }

    @Test
    public void testGetOwnerDocument()
        throws Exception
    {
        Document actual = target_.getOwnerDocument();

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testInsertBefore()
        throws Exception
    {
        Node newChild = mock( Node.class );
        Node refChild = mock( Node.class );

        target_.insertBefore( newChild, refChild );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testReplaceChild()
        throws Exception
    {
        Node newChild = mock( Node.class );
        Node oldChild = mock( Node.class );

        target_.replaceChild( newChild, oldChild );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveChild()
        throws Exception
    {
        Node node = mock( Node.class );

        target_.removeChild( node );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAppendChild()
        throws Exception
    {
        Node node = mock( Node.class );

        target_.appendChild( node );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCloneNode_deep()
        throws Exception
    {
        Node actual = target_.cloneNode( true );
    }


    @Test(expected = UnsupportedOperationException.class)
    public void testCloneNode_not_deep()
        throws Exception
    {
        Node actual = target_.cloneNode( false );
    }


    @Test(expected = UnsupportedOperationException.class)
    public void testNormalize()
        throws Exception
    {
        target_.normalize();
    }

    @Test
    public void testIsSupported()
        throws Exception
    {
        boolean actual = target_.isSupported( "feature", "version" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetPrefix()
        throws Exception
    {
        target_.setPrefix( "prefix" );
    }

    @Test
    public void testGetBaseURI()
        throws Exception
    {
        String actual = target_.getBaseURI();

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetTextContent()
        throws Exception
    {
        target_.setTextContent( "content" );
    }

    @Test
    public void testIsSameNode_with_different_node()
        throws Exception
    {
        Node node = mock( Node.class );

        boolean actual = target_.isSameNode( node );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testIsSameNode_with_same_node()
        throws Exception
    {
        Node node = mock( Node.class );

        boolean actual = target_.isSameNode( target_ );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetFeature()
        throws Exception
    {
        Object actual = target_.getFeature( "feature", "version" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetUserData()
        throws Exception
    {
        target_.setUserData( "any", null, null );
    }

    @Test
    public void testGetUserData()
        throws Exception
    {
        Object actual = target_.getUserData( "any" );

        assertThat( actual, is( nullValue() ) );
    }
}
