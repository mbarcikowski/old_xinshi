/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.websocket.jetty.internal.activation;

import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import fr.matoeil.xinshi.api.ConnectionManager;
import fr.matoeil.xinshi.transport.websocket.jetty.internal.XmppServlet;
import fr.matoeil.xinshi.transport.websocket.jetty.internal.XmppWebSocket;
import org.eclipse.jetty.websocket.WebSocket;

import javax.servlet.Servlet;
import java.util.Map;

import static org.ops4j.peaberry.Peaberry.service;
import static org.ops4j.peaberry.util.TypeLiterals.export;

/**
 * websocket servlet bundle configuration module.
 */
public class BundleModule
    extends AbstractModule
{
    @Override
    protected void configure()
    {
        configureImportedServices();

        bind( WebSocket.class ).to( XmppWebSocket.class );
        bind( XmppServlet.class ).asEagerSingleton();
        configureExportedServices();

    }

    private void configureImportedServices()
    {
        //import
        bind( ConnectionManager.class ).toProvider( service( ConnectionManager.class ).single() ).asEagerSingleton();
    }

    private void configureExportedServices()
    {
        Map<String, ?> attributes =
            ImmutableMap.of( "alias", "/ws", "servlet-name", "fr.matoeil.xinshi.transport.websocket.jetty"
                /*,"init.bufferSize", 8192,
                "init.maxIdleTime", 300000,
                "init.maxTextMessageSize", "16 * 1024",
                "init.maxBinaryMessageSize", -1*/

            );
        bind( export( Servlet.class ) ).toProvider(
            service( XmppServlet.class ).attributes( attributes ).export() ).asEagerSingleton();
    }

}
