/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.websocket.jetty.internal;

import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.stream.StreamListener;
import fr.matoeil.xinshi.xmpp.core.stream.StreamReader;
import fr.matoeil.xinshi.xmpp.core.stream.StreamWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class StreamCodec
{
    private final static Logger LOGGER = LoggerFactory.getLogger( StreamCodec.class );

    private static final byte WHITESPACE_KEEPALIVE_CHAR = '\u0020';

    public static final String UTF_8 = "UTF-8";

    public static final Charset UTF_8_CHARSET = Charset.forName( UTF_8 );

    private final ExtendedByteArrayOutputStream outputStream_;

    private final StreamReader streamReader_;

    private final StreamWriter streamWriter_;


    public StreamCodec( StreamListener aStreamListener )
    {
        outputStream_ = new ExtendedByteArrayOutputStream();
        streamReader_ = new StreamReader( aStreamListener );
        streamWriter_ = new StreamWriter( outputStream_ );
    }


    protected boolean decode( String aData )
        throws Exception
    {
        LOGGER.debug( "decode '{}'", aData );
        ByteBuffer byteBuffer = ByteBuffer.wrap( aData.getBytes( UTF_8_CHARSET ) );
        byte[] byteArray = byteBuffer.array();
        if ( byteArray.length == 1 && WHITESPACE_KEEPALIVE_CHAR == byteArray[0] )
        {
            return true;
        }
        else
        {
            streamReader_.read( byteBuffer );
        }
        return false;
    }

    public String encodeHeader( Header aHeader )
    {
        outputStream_.reset();
        streamWriter_.writeStreamOpening( aHeader );
        String copy = outputStream_.toString( UTF_8_CHARSET );
        LOGGER.debug( "encode '{}'", copy );
        return copy;
    }

    public String encodeStanza( Element aStanza )
    {
        outputStream_.reset();
        streamWriter_.writeStanza( aStanza );
        String copy = outputStream_.toString( UTF_8_CHARSET );
        LOGGER.debug( "encode '{}'", copy );
        return copy;
    }

    public String encodeFooter()
    {
        outputStream_.reset();
        streamWriter_.writeStreamClosing();
        String copy = outputStream_.toString( UTF_8_CHARSET );
        LOGGER.debug( "encode '{}'", copy );
        return copy;
    }

    public void reset()
    {
        streamReader_.reset();
        streamWriter_.reset( outputStream_ );
    }

    public void close()
    {
        boolean failed = false;
        try
        {
            streamReader_.close();
        }
        catch ( Exception e )
        {
            failed = true;
            LOGGER.error( "error while closing streamReader", e );
        }
        try
        {
            streamWriter_.close();
        }
        catch ( Exception e )
        {
            failed = true;
            LOGGER.error( "error while closing streamWriter", e );
        }
        if ( failed )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR );
        }
    }

}
