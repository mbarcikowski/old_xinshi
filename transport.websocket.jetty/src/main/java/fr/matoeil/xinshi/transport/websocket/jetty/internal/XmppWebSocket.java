/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.websocket.jetty.internal;

import fr.matoeil.xinshi.api.ConnectionManager;
import org.eclipse.jetty.websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class XmppWebSocket
    implements WebSocket, WebSocket.OnTextMessage
{
    private final static Logger LOGGER = LoggerFactory.getLogger( XmppWebSocket.class );

    private final ConnectionManager connectionManager_;

    private fr.matoeil.xinshi.api.Connection connection_;

    private StreamCodec streamCodec_;


    @Inject
    public XmppWebSocket( ConnectionManager aConnectionManager )
    {
        connectionManager_ = aConnectionManager;
    }


    @Override
    public void onOpen( Connection aConnection )
    {
        ConnectionAdapter connectionAdapter = new ConnectionAdapter( aConnection );
        connection_ = connectionManager_.provideConnection( connectionAdapter );
        streamCodec_ = new StreamCodec( connection_ );
        connectionAdapter.setStreamCodec( streamCodec_ );
    }

    @Override
    public void onClose( int aCloseCode, String aMessage )
    {
        connection_.handleStreamDisconnecting();
    }

    @Override
    public void onMessage( String aData )
    {
        try
        {
            if ( streamCodec_.decode( aData ) )
            {
                connection_.handleKeepAlive();
            }
        }
        catch ( Exception e )
        {
            LOGGER.debug( "Cannot decode message", e );
            connection_.handleException( e );
        }
    }
}
