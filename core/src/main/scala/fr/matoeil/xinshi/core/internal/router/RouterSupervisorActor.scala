/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.router

import akka.actor.SupervisorStrategy.Resume
import akka.actor._
import akka.event.Logging
import fr.matoeil.xinshi.api.Router
import fr.matoeil.xinshi.core.internal.activation.router.RouterServiceLifeCycleListener
import javax.inject.{Provider, Inject}

class RouterSupervisorActor @Inject()(private val routerLifeCycleListener_ : RouterServiceLifeCycleListener,
                                      private val routerActorProvider_ : Provider[RouterActor])
  extends Actor
{
  private val logger_ = Logging.getLogger(context.system, this)

  private var routerRef_ : ActorRef = null

  override val supervisorStrategy = OneForOneStrategy()
  {
    case e ⇒
    {
      logger_.error(e, "Throwable detected \n" + "\t by:{}", context.self)
      Resume
    }
  }

  override def preStart()
  {
    super.preStart()
    logger_.debug("preStart\n" + "\t for:{}\n", context.self)
    initRouter()
  }

  override def postRestart(reason: Throwable)
  {
    super.postRestart(reason)
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context.self)
  }

  override def postStop()
  {
    super.postStop()
    logger_.debug("postStop\n" + "\t for:{}\n", context.self)
  }

  override def preRestart(reason: Throwable, message: Option[Any])
  {
    super.preRestart(reason, message)
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{}\n" + "\t for:{}\n", reason, message,
                  context.self)
  }

  def receive =
  {
    case terminated: Terminated =>
    {
      logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", terminated, sender,
                    context.self)
      if ( terminated.actor eq routerRef_ )
      {
        routerLifeCycleListener_.handleStop()
      }
    }
    case message =>
    {
      logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", message, sender,
                    context.self)
    }
  }


  private def initRouter()
  {
    val router: Router = TypedActor(context).typedActorOf(TypedProps(classOf[Router], routerActorProvider_.get()),
                                                          "router")
    routerRef_ = TypedActor(context).getActorRefFor(router)
    context.watch(routerRef_)
    routerLifeCycleListener_.handleStart(router)
  }

}
