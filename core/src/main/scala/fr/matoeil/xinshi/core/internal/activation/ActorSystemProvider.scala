/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.activation

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.{ConfigFactory, Config}
import fr.matoeil.xinshi.core.internal.activation.connectionmanager.ConnectionManagerModule
import fr.matoeil.xinshi.core.internal.connectionmanager.ConnectionManagerSupervisorActor
import fr.matoeil.xinshi.core.internal.router.RouterSupervisorActor
import fr.matoeil.xinshi.core.internal.server.actor.ServerSupervisorActor
import fr.matoeil.xinshi.core.internal.sessionmanager.actor.SessionManagerSupervisorActor
import javax.inject.{Provider, Inject}

class ActorSystemProvider @Inject()(private val connectionManagerSupervisorActorProvider_ : Provider[ConnectionManagerSupervisorActor],
                                    private val routerSupervisorActorProvider_ : Provider[RouterSupervisorActor],
                                    private val serverSupervisorActorProvider_ : Provider[ServerSupervisorActor],
                                    private val sessionManagerSupervisorActorProvider_ : Provider[SessionManagerSupervisorActor])
  extends Provider[ActorSystem]
{
  def get =
  {
    val referenceConfig: Config = ConfigFactory.load(classOf[ActorSystem].getClassLoader)
    val applicationConfig: Config = ConfigFactory.load(classOf[ConnectionManagerModule].getClassLoader)
    val config: Config = applicationConfig.getConfig("xinshi-system").withFallback(referenceConfig)
    val actorSystem = ActorSystem.create("XinshiSystem", config, classOf[ConnectionManagerModule].getClassLoader)

    actorSystem.actorOf(Props(connectionManagerSupervisorActorProvider_.get()), "connection-manager-supervisor")

    actorSystem.actorOf(Props(routerSupervisorActorProvider_.get()), "router-supervisor")

    actorSystem.actorOf(Props(serverSupervisorActorProvider_.get()), "server-supervisor")

    actorSystem.actorOf(Props(sessionManagerSupervisorActorProvider_.get()), "session-manager-supervisor")


    actorSystem
  }
}


