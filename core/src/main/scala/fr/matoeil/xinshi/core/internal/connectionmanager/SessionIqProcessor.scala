/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import akka.actor.ActorContext
import akka.event.Logging
import fr.matoeil.xinshi.core.internal.connectionmanager.IqProcessor.StanzaType
import fr.matoeil.xinshi.xmpp.core.address.Address
import fr.matoeil.xinshi.xmpp.core.content.{StanzaErrorConstants, StanzaErrorFactory}
import fr.matoeil.xinshi.xmpp.core.stream.session.{SessionFactory, SessionConstants}
import org.w3c.dom.Element

class SessionIqProcessor(context_ : ActorContext)
{
  private val logger_ = Logging.getLogger(context_.system, this)

  def feature: Element =
  {
    SessionFactory.feature()
  }

  def processStanza(aFrom: Address, aElement: Element): StanzaType =
  {
    aElement.getLocalName match
    {
      case SessionConstants.SESSION =>
      {
        new
            IqProcessor.Result(null)
      }
      case _ =>
      {
        //TODO error
        logger_.error("error \n" + "\t for:{}", context_.self)
        new
            IqProcessor.Error(StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                                          StanzaErrorConstants.Condition.FEATURE_NOT_IMPLEMENTED))
      }
    }
  }
}
