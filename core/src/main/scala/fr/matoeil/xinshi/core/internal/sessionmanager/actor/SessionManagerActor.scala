/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.sessionmanager.actor

import akka.actor.SupervisorStrategy.Resume
import akka.actor.TypedActor._
import akka.actor._
import akka.event.Logging
import com.google.common.collect.Maps
import fr.matoeil.xinshi.api.{Router, Connection}
import fr.matoeil.xinshi.core.internal.sessionmanager.SessionManager
import fr.matoeil.xinshi.core.internal.sessionmanager.actor.SessionRouteeActor.Route
import fr.matoeil.xinshi.xmpp.core.address.{Addresses, Address}
import fr.matoeil.xinshi.xmpp.core.content.{StanzaErrorConstants, ClientContentFactory, StanzaErrorFactory, ClientContentConstants}
import javax.inject.{Provider, Inject}
import org.w3c.dom.{Attr, Element}

class SessionManagerActor @Inject()(private val sessionRouteeActorProvider_ : Provider[SessionRouteeActor],
                                    private val router_ : Router)
  extends SessionManager with Supervisor with PreStart with PostStop with PreRestart with PostRestart with Receiver
{

  private val context_ = TypedActor.context

  private val logger_ = Logging.getLogger(context_.system, this)

  private val bareAddressToActorRef_ = Maps.newTreeMap[String, Registration]()


  override val supervisorStrategy = OneForOneStrategy()
  {
    case e ⇒
    {
      logger_.error(e, "Throwable detected \n" + "\t by:{}", context_.self)
      Resume
    }
  }


  override def registerSession(anAddress: Address, aConnection: Connection)
  {
    val bareAddress = anAddress.bareAddress()
    val actorRef = bareAddressToActorRef_.get(bareAddress) match
    {
      case registration: Registration =>
      {
        registration.increment()
        registration.sessionRoutee
      }
      case null =>
      {
        newSessionRouteeActor(bareAddress)
      }
    }
    logger_.debug("registerSession\n" + "\t address:{}\n" + "\t connection:{}\n" + "\t in:{}\n" + "\t for:{}\n",
                  anAddress.bareAddress(), aConnection, actorRef, context_.self)
    actorRef ! SessionRouteeActor.Register(anAddress, aConnection)
  }

  override def unregisterSession(anAddress: Address)
  {
    val bareAddress = anAddress.bareAddress()
    val registration = bareAddressToActorRef_.get(bareAddress)
    if ( registration != null )
    {
      logger_.debug("unregisterSession\n" + "\t address:{}\n" + "\t for:{}\n", anAddress, context_.self)
      registration.decrement()
      registration.sessionRoutee ! SessionRouteeActor.Unregister(anAddress)
      if ( registration.count == 0 )
      {
        logger_.debug("remove\n" + "\t sessionRoutee:{}\n" + "\t for:{}\n", registration.sessionRoutee, context_.self)
        context_.stop(registration.sessionRoutee)
        bareAddressToActorRef_.remove(bareAddress)
      }
    }
  }

  //TODO verify routing (stanza kind first)
  override def route(aStanza: Element)
  {
    logger_.debug("route \n" + "\t stanza:{} \n" + "\t for:{}", aStanza, context_.self)
    val to = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.TO))
    bareAddressToActorRef_.get(to.bareAddress()) match
    {
      case registration: Registration =>
      {
        registration.sessionRoutee ! Route(to, aStanza)
      }
      case _ =>
      {
        handleNoRoute(to, aStanza)
      }
    }
  }

  override def preStart()
  {
    logger_.debug("preStart \n" + "\t for:{}\n", context_.self)
  }

  override def postRestart(reason: Throwable)
  {
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context_.self)
  }

  override def postStop()
  {
    logger_.debug("postStop\n" + "\t for:{}\n", context_.self)
  }


  def preRestart(reason: Throwable, message: Option[Any])
  {
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{}\n" + "\t for:{}\n", reason, message,
                  context_.self)
  }

  def onReceive(message: Any, sender: ActorRef)
  {
    logger_.debug("onReceive {}:{}\n" + "\t for:{}\n", message, sender, context_.self)
  }


  private def newSessionRouteeActor(aBareAddress: String): ActorRef =
  {
    val actorRef = context_.actorOf(Props(sessionRouteeActorProvider_.get()), aBareAddress)
    bareAddressToActorRef_.put(aBareAddress, new
        Registration(actorRef))
    actorRef
  }

  //TODO see xmpp-im => 8.5.1., 8.5.2.2.
  private def handleNoRoute(aTo: Address, aStanza: Element)
  {
    //TODO use type according stanza localName
    ClientContentConstants.Type.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.TYPE)) match
    {
      case ClientContentConstants.Type.ERROR =>
      {
        //no op
      }
      case _ =>
      {
        deliversError(aTo, aStanza)
      }
    }

  }

  def deliversError(aTo: Address, aStanza: Element)
  {
    aStanza.getLocalName match
    {
      case ClientContentConstants.IQ =>
      {
        deliversIqError(aStanza, aTo)
      }
      case ClientContentConstants.MESSAGE =>
      {
        deliversMessageError(aStanza, aTo)
      }
      case _ =>
      {
        //no op
      }
    }
  }

  def deliversMessageError(aStanza: Element, aTo: Address)
  {
    val error = StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                            StanzaErrorConstants.Condition.SERVICE_UNAVAILABLE)
    val id = getId(aStanza)
    val from = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.FROM))
    val message = ClientContentFactory.newMessage(ClientContentConstants.Type.ERROR, id, aTo, from)
    message.appendChild(message.getOwnerDocument.adoptNode(error))
    try
    {
      router_.route(message)
    }
    catch
      {
        case e: Throwable =>
        {
          logger_.error(e, "failed to deliver message {}", message)
        }
      }
  }

  def deliversIqError(aStanza: Element, aTo: Address)
  {
    val error = StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                            StanzaErrorConstants.Condition.SERVICE_UNAVAILABLE)
    val id = getId(aStanza)
    val from = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.FROM))
    val iq = ClientContentFactory.newIq(ClientContentConstants.Type.ERROR, id, aTo, from)
    iq.appendChild(iq.getOwnerDocument.adoptNode(aStanza.getFirstChild))
    iq.appendChild(iq.getOwnerDocument.adoptNode(error))
    try
    {
      router_.route(iq)
    }
    catch
      {
        case e: Throwable =>
        {
          logger_.error(e, "failed to deliver iq {}", iq)
        }
      }
  }

  //TODO factorization
  def getId(aStanza: Element): String =
  {
    aStanza.getAttributeNode(ClientContentConstants.Attributes.ID) match
    {
      case attr: Attr =>
      {
        attr.getValue
      }
      case _ =>
      {
        null
      }
    }
  }
}
