/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import fr.matoeil.xinshi.core.internal.connectionmanager.IqProcessor.StanzaType
import fr.matoeil.xinshi.xmpp.core.address.Address
import fr.matoeil.xinshi.xmpp.core.content.{StanzaErrorConstants, StanzaErrorFactory}
import org.w3c.dom.Element

object FallbackProcessor
{

  def processStanza(aFrom: Address, aStanza: Element): StanzaType =
  {
    new
        IqProcessor.Error(StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                                      StanzaErrorConstants.Condition.FEATURE_NOT_IMPLEMENTED))
  }

}
