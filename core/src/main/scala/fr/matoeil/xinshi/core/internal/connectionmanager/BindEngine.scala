/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import fr.matoeil.xinshi.xmpp.core.address.{Addresses, Address}
import fr.matoeil.xinshi.xmpp.core.content.StanzaErrorConstants.{Condition, Type}
import fr.matoeil.xinshi.xmpp.core.content.{StanzaErrorFactory, ClientContentConstants}
import fr.matoeil.xinshi.xmpp.core.stream.bind.{BindConstants, BindFactory}
import fr.matoeil.xinshi.xmpp.core.stream.{StreamErrorConstants, StreamException}
import org.apache.shiro.crypto.SecureRandomNumberGenerator
import org.apache.shiro.crypto.hash.Sha1Hash
import org.apache.shiro.util.ByteSource
import org.w3c.dom.{Attr, Element}

object BindEngine
{

  sealed trait Result

  case class Exception(exception: StreamException)
    extends Result

  case class Failure(stanza: Element)
    extends Result

  case class Continue(stanza: Element)
    extends Result

  case class Success(address: Address, stanza: Element)
    extends Result

}

class BindEngine(private val username_ : String, private val domain_ : String, private val connectionId_ : String)
{

  private val secureRandomNumberGenerator_ = new
      SecureRandomNumberGenerator

  def feature: Element =
  {
    BindFactory.feature()
  }

  def handleStanza(aStanza: Element): BindEngine.Result =
  {
    aStanza.getNamespaceURI match
    {
      case ClientContentConstants.CLIENT_CONTENT_NAMESPACE_URI =>
      {
        aStanza.getLocalName match
        {
          case ClientContentConstants.IQ =>
          {
            ClientContentConstants.Type.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.TYPE)) match
            {
              case ClientContentConstants.Type.SET =>
              {
                aStanza.getFirstChild match
                {
                  case element: Element =>
                  {
                    //TODO factorization
                    handleBind(aStanza.getAttributeNode(ClientContentConstants.Attributes.ID), element)
                  }
                  case _ =>
                  {
                    BindEngine.Exception(new
                                             StreamException(StreamErrorConstants.Error.NO_AUTHORIZED))
                  }
                }
              }
              case _ =>
              {
                BindEngine.Failure(StanzaErrorFactory.newError(Type.MODIFY, Condition.BAD_REQUEST))
              }
            }
          }
          case _ =>
          {
            BindEngine.Exception(new
                                     StreamException(StreamErrorConstants.Error.NO_AUTHORIZED))
          }
        }
      }
      case _ =>
      {
        BindEngine.Exception(new
                                 StreamException(StreamErrorConstants.Error.UNSUPPORTED_STANZA_TYPE))
      }
    }

  }

  private def handleBind(aIdAttr: Attr, aElement: Element): BindEngine.Result =
  {
    aElement.getNamespaceURI match
    {
      case BindConstants.BIND_NAMESPACE_URI =>
      {
        aElement.getLocalName match
        {
          case BindConstants.BIND =>
          {
            processBind(aIdAttr, aElement)
          }
          case _ =>
          {
            BindEngine.Exception(new
                                     StreamException(StreamErrorConstants.Error.NO_AUTHORIZED))
          }
        }
      }
      case _ =>
      {
        BindEngine.Exception(new
                                 StreamException(StreamErrorConstants.Error.NO_AUTHORIZED))
      }
    }
  }


  private def processBind(aIdAttr: Attr, aBind: Element): BindEngine.Result =
  {

    val resource = aBind.getFirstChild match
    {
      case element: Element =>
      {
        element.getTextContent + "-"
      }
      case _ =>
      {
        ""
      }
    }


    val nodePart = Addresses.escapeNodePart(username_)
    val address = new
        Address(nodePart, domain_, resource + createNewResource())

    val id = aIdAttr match
    {
      case attr: Attr =>
      {
        attr.getValue
      }
      case _ =>
      {
        null
      }
    }

    BindEngine.Success(address, BindFactory.result(id, address))
  }


  private def createNewResource(): String =
  {
    val randomBytes: ByteSource = secureRandomNumberGenerator_.nextBytes
    new
        Sha1Hash(randomBytes, connectionId_).toHex
  }
}
