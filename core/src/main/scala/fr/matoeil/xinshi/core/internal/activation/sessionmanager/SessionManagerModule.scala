/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.activation.sessionmanager

import com.google.inject.{TypeLiteral, AbstractModule}
import fr.matoeil.xinshi.core.internal.ServiceLifeCycleListener
import fr.matoeil.xinshi.core.internal.sessionmanager.SessionManager
import fr.matoeil.xinshi.core.internal.sessionmanager.actor.{SessionRouteeActor, SessionManagerSupervisorActor, SessionManagerActor}
import org.ops4j.peaberry.Peaberry._

class SessionManagerModule
  extends AbstractModule
{
  def configure()
  {
    configureImportedServices()
    configureActorSystem()
    configureListeners()
  }

  private def configureImportedServices()
  {
    bind(classOf[SessionManager]).toProvider(service(classOf[SessionManager]).single).asEagerSingleton()
  }

  private def configureActorSystem()
  {
    bind(classOf[SessionManagerSupervisorActor])
    bind(classOf[SessionManagerActor])
    bind(classOf[SessionRouteeActor])
  }

  private def configureListeners()
  {
    bind(new
             TypeLiteral[ServiceLifeCycleListener[SessionManager]]
         {}).to(classOf[SessionManagerServiceLifeCycleListener]).asEagerSingleton()
  }
}
