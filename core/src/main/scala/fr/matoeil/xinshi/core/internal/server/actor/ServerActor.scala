/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.server.actor

import akka.actor.TypedActor._
import akka.actor.{TypedActor, ActorRef}
import akka.event.Logging
import fr.matoeil.xinshi.api.Router
import fr.matoeil.xinshi.core.internal.server.Server
import fr.matoeil.xinshi.xmpp.core.address.Addresses
import fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants.Type
import fr.matoeil.xinshi.xmpp.core.content._
import fr.matoeil.xinshi.xmpp.disco._
import javax.inject.Inject
import org.w3c.dom.Element

//TODO to refactor
class ServerActor @Inject()(private val router_ : Router)
  extends Server with PreStart with PostStop with PreRestart with PostRestart with Receiver
{
  private val context_ = TypedActor.context

  private val logger_ = Logging.getLogger(context_.system, this)

  //TODO verify routing (stanza kind first)
  def handleStanza(aStanza: Element)
  {
    //TODO check namespace => not found => feature-not-implemented (ejabberd behavior)
    logger_.debug("handleStanza {}", aStanza)
    aStanza.getLocalName match
    {
      case ClientContentConstants.IQ =>
      {
        val child = aStanza.getFirstChild
        if ( child != null )
        {
          child.getNamespaceURI match
          {
            case DiscoItemsConstants.DISCO_ITEMS_NAMESPACE_URI =>
            {
              logger_.debug("discoItems {}", child)
              val id = aStanza.getAttribute(ClientContentConstants.Attributes.ID)
              val from = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.TO))
              val to = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.FROM))
              val iq = ClientContentFactory.newIq(Type.ERROR, id, from, to)
              iq.appendChild(iq.getOwnerDocument.adoptNode(aStanza.getFirstChild))
              val error = StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                                      StanzaErrorConstants.Condition.FEATURE_NOT_IMPLEMENTED)
              iq.appendChild(iq.getOwnerDocument.adoptNode(error))
              router_.route(iq)
            }
            case DiscoInfoConstants.DISCO_INFO_NAMESPACE_URI =>
            {
              logger_.debug("discoInfo {}", child)
              val id = aStanza.getAttribute(ClientContentConstants.Attributes.ID)
              val from = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.TO))
              val to = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.FROM))
              val iq = ClientContentFactory.newIq(Type.ERROR, id, from, to)
              iq.appendChild(iq.getOwnerDocument.adoptNode(aStanza.getFirstChild))
              val error = StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                                      StanzaErrorConstants.Condition.FEATURE_NOT_IMPLEMENTED)
              iq.appendChild(iq.getOwnerDocument.adoptNode(error))
              router_.route(iq)
            }
            case _ =>
            {

            }
          }
        }
        else
        {
          //TODO error
        }
      }
      case ClientContentConstants.MESSAGE =>
      {
        //TODO to finish
        //TODO if messsage => not implemented
      }
      case ClientContentConstants.PRESENCE =>
      {
        //TODO to finish
        //TODO if presence => no op
      }
      case _ =>
      {
        //TODO to finish
      }
    }


  }

  override def preStart()
  {
    logger_.debug("preStart\n" + "\t for:{}\n", context_.self)
  }

  override def postRestart(reason: Throwable)
  {
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context_.self)
  }

  override def postStop()
  {
    logger_.debug("postStop\n" + "\t for:{}\n", context_.self)
  }

  override def preRestart(reason: Throwable, message: Option[Any])
  {
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{}\n" + "\t for:{}\n", reason, message,
                  context_.self)
  }

  override def onReceive(message: Any, sender: ActorRef)
  {
    logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", message, sender, context_.self)
  }
}
