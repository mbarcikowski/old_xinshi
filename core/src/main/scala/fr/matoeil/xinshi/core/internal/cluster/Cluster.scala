package fr.matoeil.xinshi.core.internal.cluster

import org.jgroups.{View, ReceiverAdapter, JChannel}

class Cluster
  extends ReceiverAdapter
{
  private val CLUSTER_NAME: String = "xinshi"

  private var channel_ : JChannel = null

  def start()
  {
    channel_ = new
        JChannel
    channel_.setReceiver(this)
    channel_.connect(CLUSTER_NAME)
  }

  def stop()
  {
    channel_.close()
  }

  override def viewAccepted(view: View)
  {
    System.out.println("** view: " + view)
  }
}
