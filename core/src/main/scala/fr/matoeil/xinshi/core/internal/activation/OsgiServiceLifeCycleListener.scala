/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.activation

import fr.matoeil.xinshi.core.internal.ServiceLifeCycleListener
import java.util
import javax.inject.Inject
import org.osgi.framework.{ServiceRegistration, BundleContext}

abstract class OsgiServiceLifeCycleListener[S] @Inject()(private final val bundleContext_ : BundleContext)
  extends ServiceLifeCycleListener[S]
{
  private var serviceRegistration_ : ServiceRegistration[_] = null

  protected def getInterfaces: Array[String]

  protected def getProperties: util.Dictionary[String, _] =
  {
    null
  }

  def handleStart(aService: S)
  {
    serviceRegistration_ = bundleContext_.registerService(getInterfaces, aService, getProperties)
  }

  def handleStop()
  {
    if ( serviceRegistration_ != null )
    {
      serviceRegistration_.unregister()
      serviceRegistration_ = null
    }
  }
}
