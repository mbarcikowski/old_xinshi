/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import akka.actor.ActorContext
import akka.event.Logging
import fr.matoeil.xinshi.core.internal.connectionmanager.IqProcessor.StanzaType
import fr.matoeil.xinshi.xmpp.core.address.{Addresses, Address}
import fr.matoeil.xinshi.xmpp.core.content.{StanzaErrorConstants, StanzaErrorFactory}
import fr.matoeil.xinshi.xmpp.roster.RosterConstants.Subscription
import fr.matoeil.xinshi.xmpp.roster.{RosterFactory, Item}
import org.w3c.dom.Element
import scala.Array

class RosterIqProcessor(context_ : ActorContext)
{
  private val logger_ = Logging.getLogger(context_.system, this)

  def processStanza(aFrom: Address, aElement: Element): StanzaType =
  {
    aElement.getLocalName match
    {
      case "query" =>
      {
        new
            IqProcessor.Result(handleRosterQuery(aFrom, aElement))
      }
      case _ =>
      {
        //TODO error
        logger_.error("error \n" + "\t for:{}", context_.self)
        new
            IqProcessor.Error(StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                                          StanzaErrorConstants.Condition.FEATURE_NOT_IMPLEMENTED))
      }
    }
  }

  //TODO get domain from config
  def handleRosterQuery(aFrom: Address, aStanza: Element): Element =
  {
    val items = aFrom.node match
    {
      case "test" =>
      {
        Array(new
                  Item(Addresses.fromString("test2@localhost.machin.truc"), "test2", Subscription.BOTH))
      }
      case "test2" =>
      {
        Array(new
                  Item(Addresses.fromString("test@localhost.machin.truc"), "test", Subscription.BOTH))
      }
      case _ =>
      {
        Array(new
                  Item(Addresses.fromString("test@localhost.machin.truc"), "test", Subscription.BOTH), new
                  Item(Addresses.fromString("test2@localhost.machin.truc"), "test2", Subscription.BOTH))
      }
    }
    RosterFactory.newQuery(items: _*)
  }
}
