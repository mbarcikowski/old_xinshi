/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.router

import akka.actor.TypedActor._
import akka.actor.{ActorRef, TypedActor}
import akka.event.Logging
import com.google.common.collect.Maps
import fr.matoeil.xinshi.api.Router
import fr.matoeil.xinshi.core.internal.sessionmanager.SessionManager
import fr.matoeil.xinshi.spi.Route
import fr.matoeil.xinshi.xmpp.core.address.{Addresses, Address}
import fr.matoeil.xinshi.xmpp.core.content.{StanzaErrorConstants, StanzaErrorFactory, ClientContentFactory, ClientContentConstants}
import javax.inject.Inject
import org.w3c.dom.Element

class RouterActor @Inject()(private val sessionManager_ : SessionManager)
  extends Router with PreStart with PostStop with PreRestart with PostRestart with Receiver
{
  private val context_ = TypedActor.context

  private val logger_ = Logging.getLogger(context_.system, this)

  private val subDomainToRoute_ = Maps.newTreeMap[String, Route]()

  //TODO verify routing (stanza kind first)
  //TODO verify from
  override def route(aStanza: Element)
  {
    logger_.debug("route \n" + "\t stanza:{} \n" + "\t for:{}", aStanza, context_.self)
    val toAtt = aStanza.getAttribute(ClientContentConstants.Attributes.TO)
    logger_.debug("toAtt:{}", toAtt)
    //TODO StringIndexOutOfBoundsException if to = ""
    val to = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.TO))
    logger_.debug("to:{}", to.fullAddress())
    to.domain match
    {
      case null => replyWithError(to, aStanza)
      case domain if domain.endsWith("localhost.machin.truc") => route(to, aStanza)
      case _ => replyWithError(to, aStanza)
    }
  }

  override def registerRoute(aRoute: Route, aSubDomain: String)
  {
    logger_.debug("registerRoute \n" + "\t route:{}\n" + "\t subDomain:{}\n" + "\t for:{}\n", aRoute, aSubDomain,
                  context_.self)
    subDomainToRoute_.put(aSubDomain + ".localhost.machin.truc", aRoute)
  }

  override def unregisterRoute(aSubDomain: String)
  {
    logger_.debug("unregisterRoute \n" + "\t subDomain:{}\n" + "\t for:{}\n", aSubDomain, context_.self)
    subDomainToRoute_.remove(aSubDomain + ".localhost.machin.truc")
  }

  override def getRoutes = null

  override def preStart()
  {
    logger_.debug("preStart\n" + "\t for:{}\n", context_.self)
  }

  override def postRestart(reason: Throwable)
  {
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context_.self)
  }

  override def postStop()
  {
    logger_.debug("postStop")
  }

  override def preRestart(reason: Throwable, message: Option[Any])
  {
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{}\n" + "\t for:{}\n", reason, message,
                  context_.self)
  }

  override def onReceive(message: Any, sender: ActorRef)
  {
    logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", message, sender, context_.self)
  }

  //TODO
  private def replyWithError(aTo: Address, aStanza: Element)
  {
    val from = Addresses.fromString(aStanza.getAttribute(ClientContentConstants.Attributes.FROM))
    val iq = ClientContentFactory.newIq(ClientContentConstants.Type.ERROR,
                                        aStanza.getAttribute(ClientContentConstants.Attributes.ID), aTo, from)
    val error = StanzaErrorFactory.newError(StanzaErrorConstants.Type.CANCEL,
                                            StanzaErrorConstants.Condition.ITEM_NOT_FOUND)
    iq.appendChild(iq.getOwnerDocument.adoptNode(aStanza.getFirstChild))
    iq.appendChild(iq.getOwnerDocument.adoptNode(error))
    sessionManager_.route(iq)
    //TODO no external domain route
    logger_.error("no external domain route")
    //TODO ejabberd / gtalk => iq => remote-server-not-found
    //TODO ejabberd / gtalk => message => remote-server-not-found
    //TODO ejabberd / gtalk => presence => remote-server-not-found
  }

  private def route(aTo: Address, aStanza: Element)
  {

    //TODO get domain from config
    aTo.node match
    {
      case "" =>
      {
        subDomainToRoute_.get(aTo.domain) match
        {
          case route: Route =>
          {
            route.handleStanza(aStanza)
          }
          case _ =>
          {
            //TODO error no route
            logger_.error("no route")
            //TODO error
            // TODO ejabberd => iq => service-unavailable / gtalk => remote-server-not-found
            // TODO ejabberd => message => service-unavailable  / gtalk => remote-server-not-found
            // TODO ejabberd => presence => service-unavailable  / gtalk => noop
          }
        }
      }
      case _ =>
      {
        sessionManager_.route(aStanza)
      }
    }

  }

}
