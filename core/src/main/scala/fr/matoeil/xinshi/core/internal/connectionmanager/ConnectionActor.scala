/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import akka.actor.TypedActor._
import akka.actor.{PoisonPill, ReceiveTimeout, ActorRef, TypedActor}
import akka.event.Logging
import fr.matoeil.xinshi.api.Connection
import fr.matoeil.xinshi.api.Router
import fr.matoeil.xinshi.core.internal.connectionmanager.ConnectionActor._
import fr.matoeil.xinshi.core.internal.connectionmanager.IqProcessor.StanzaType
import fr.matoeil.xinshi.core.internal.sessionmanager.SessionManager
import fr.matoeil.xinshi.xmpp.core.address.{Addresses, Address}
import fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants.Type
import fr.matoeil.xinshi.xmpp.core.content._
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants.Error
import fr.matoeil.xinshi.xmpp.core.stream._
import fr.matoeil.xinshi.xmpp.core.stream.session.SessionConstants
import fr.matoeil.xinshi.xmpp.core.version.Version
import fr.matoeil.xinshi.xmpp.roster.RosterConstants
import javax.inject.Inject
import org.w3c.dom.{Attr, Element}
import scala.concurrent.duration._

private object ConnectionActor
{
  private val CONNECTION_TIMEOUT = 120.seconds

  private val VERSION = new
      Version(1, 0)

  private val LANG = "en"

  private sealed trait State

  private case object Opening
    extends State

  private case object Authenticating
    extends State

  private case object Restarting
    extends State

  private case object Binding
    extends State

  private case object Processing
    extends State

  private case object Closing
    extends State

  private case object Stopped
    extends State

}

//TODO to refactor
class ConnectionActor @Inject()(private val sessionManager_ : SessionManager, private val router_ : Router)
  extends Connection with PreStart with PostStop with PreRestart with PostRestart with Receiver
{


  private val context_ = TypedActor.context

  private val logger_ = Logging.getLogger(context_.system, this)

  private var streamChannel_ : Option[StreamChannel] = None

  context_.setReceiveTimeout(CONNECTION_TIMEOUT)


  private var state_ : State = Opening


  private var connectionId_ : String = null

  private var lang_ : String = LANG

  private var username_ : String = null

  private var clientAddress_ : Address = null

  private var serverAddress_ : Address = Addresses.fromString("localhost.machin.truc")

  private var saslEngine_ : SaslEngine = null

  private var bindEngine_ : BindEngine = null

  private var rosterIqProcessor_ : RosterIqProcessor = null

  private var sessionIqProcessor_ : SessionIqProcessor = null

  def init(aConnectionId: String, aStreamChannel: StreamChannel): ConnectionActor =
  {
    connectionId_ = aConnectionId
    streamChannel_ = Some(aStreamChannel)
    this
  }


  override def handleStreamOpening(aHeader: Header)
  {
    logger_.debug("handleStreamOpening \n" + "\t Header:{} \n" + "\t for:{}", aHeader, context_.self)
    state_ match
    {
      case Opening =>
      {
        //TODO validate from, to, ...
        serverAddress_ = aHeader.to()
        clientAddress_ = aHeader.from()
        lang_ = aHeader.lang()
        openStream()
        saslEngine_ = new
            SaslEngine()
        val features = ClientContentFactory.features()
        features.appendChild(features.getOwnerDocument.adoptNode(saslEngine_.feature))
        deliverToStream(features)
        nextState(Authenticating)
      }
      case Authenticating =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Restarting =>
      {
        //TODO validate from, to, generate new connection id...
        //connectionId_ = newConnectionId ?
        openStream()
        bindEngine_ = new
            BindEngine(username_, serverAddress_.domain(), connectionId_)
        sessionIqProcessor_ = new
            SessionIqProcessor(context_)
        val features = ClientContentFactory.features()
        features.appendChild(features.getOwnerDocument.adoptNode(bindEngine_.feature))
        features.appendChild(features.getOwnerDocument.adoptNode(sessionIqProcessor_.feature))
        deliverToStream(features)
        nextState(Binding)
      }
      case Binding =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Processing =>
      {
        unregisterSession()
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Closing =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Stopped =>
      {
        //no op
      }
    }
  }


  override def handleIncomingStanza(aStanza: Element)
  {
    logger_.debug("handleIncomingStanza \n" + "\t element:{} \n" + "\t for:{}", aStanza, context_.self)
    state_ match
    {
      case Opening =>
      {
        openStream()
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Authenticating =>
      {
        processAuthentication(aStanza)
      }
      case Restarting =>
      {
        openStream()
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Binding =>
      {
        processBinding(aStanza)
      }
      case Processing =>
      {
        processStanza(aStanza)
      }
      case Closing =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Stopped =>
      {
        //no op
      }
    }
  }

  override def handleStreamClosing()
  {
    logger_.debug("handleStreamClosing \n" + "\t for:{}", context_.self)
    state_ match
    {
      case Opening =>
      {
        openStream()
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Authenticating =>
      {
        closeStream()
        stop()
      }
      case Restarting =>
      {
        openStream()
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Binding =>
      {
        closeStream()
        stop()
      }
      case Processing =>
      {
        unregisterSession()
        context_.self.tell(PoisonPill)
        nextState(Closing)
      }
      case Closing =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.INVALID_XML))
        closeStream()
        stop()
      }
      case Stopped =>
      {
        //no op
      }
    }
  }

  override def handleStreamDisconnecting()
  {
    logger_.debug("handleStreamDisconnecting \n" + "\t for:{}", context_.self)
    state_ match
    {
      case Opening | Authenticating | Restarting | Binding =>
      {
        stop()
      }
      case Processing =>
      {
        unregisterSession()
        stop()
      }
      case Closing =>
      {
        stop()
      }
      case Stopped =>
      {
        //no op
      }
    }
  }

  override def handleException(aCause: Throwable)
  {
    logger_.debug("handleException \n" + "\t error:{}\n" + "\t for:{}", aCause, context_.self)
    state_ match
    {
      case Opening =>
      {
        openStream()
        deliverToStream(fromThrowable(aCause))
        closeStream()
        stop()
      }
      case Authenticating =>
      {
        deliverToStream(fromThrowable(aCause))
        closeStream()
        stop()
      }
      case Restarting =>
      {
        openStream()
        deliverToStream(fromThrowable(aCause))
        closeStream()
        stop()
      }
      case Binding =>
      {
        deliverToStream(fromThrowable(aCause))
        closeStream()
        stop()
      }
      case Processing =>
      {
        unregisterSession()
        deliverToStream(fromThrowable(aCause))
        closeStream()
        stop()
      }
      case Closing =>
      {
        deliverToStream(fromThrowable(aCause))
        closeStream()
        stop()
      }
      case Stopped =>
      {
        //no op
      }
    }
  }

  override def handleKeepAlive()
  {
    logger_.debug("keepalive detected\n" + "\t for:{}\n", context_.self)
  }

  override def deliver(aStanza: Element)
  {
    logger_.debug("deliver\n" + "\t stanza:{}\n" + "\t for:{}\n", aStanza, context_.self)
    state_ match
    {
      case Opening | Authenticating | Restarting | Binding =>
      {
        bounce(aStanza)
      }
      case Processing | Closing =>
      {
        deliverToStream(aStanza)
      }
      case Stopped =>
      {
        bounce(aStanza)
      }
    }
  }

  override def preStart()
  {
    logger_.debug("preStart\n" + "\t for:{}\n", context_.self)
  }

  override def postRestart(reason: Throwable)
  {
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context_.self)
  }

  override def postStop()
  {
    logger_.debug("postStop\n" + "\t for:{}\n", context_.self)
    state_ match
    {
      case Opening =>
      {
        openStream()
        deliverToStream(StreamErrorFactory.newError(Error.SYSTEM_SHUTDOWN))
        closeStream()
        stop()
      }
      case Authenticating =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.SYSTEM_SHUTDOWN))
        closeStream()
        stop()
      }
      case Restarting =>
      {
        openStream()
        deliverToStream(StreamErrorFactory.newError(Error.SYSTEM_SHUTDOWN))
        closeStream()
        stop()
      }
      case Binding =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.SYSTEM_SHUTDOWN))
        closeStream()
        stop()
      }
      case Processing =>
      {
        unregisterSession()
        deliverToStream(StreamErrorFactory.newError(Error.SYSTEM_SHUTDOWN))
        closeStream()
        stop()
      }
      case Closing =>
      {
        deliverToStream(StreamErrorFactory.newError(Error.SYSTEM_SHUTDOWN))
        closeStream()
        stop()
      }
      case Stopped =>
      {
        //no op
      }
    }
  }

  override def preRestart(reason: Throwable, message: Option[Any])
  {
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{} \n", reason, message)
  }

  override def onReceive(message: Any, sender: ActorRef)
  {
    logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", message, sender, context_.self)
    message match
    {
      //TODO not use handleException
      case ReceiveTimeout => handleException(new
                                                 StreamException(Error.CONNECTION_TIMEOUT))
    }
  }

  private def processAuthentication(aStanza: Element)
  {
    saslEngine_.handleStanza(aStanza) match
    {
      case continue: SaslEngine.Continue =>
      {
        deliverToStream(continue.stanza)
      }
      case success: SaslEngine.Success =>
      {
        username_ = success.username
        deliverToStream(success.stanza)
        resetStream()
        nextState(Restarting)
      }
      case failure: SaslEngine.Failure =>
      {
        deliverToStream(failure.stanza)
        closeStream()
        stop()
      }
      case exception: SaslEngine.Exception =>
      {
        //TODO not use handleexception
        handleException(exception.exception)
      }
    }
  }


  private def processBinding(aStanza: Element)
  {
    bindEngine_.handleStanza(aStanza) match
    {
      case continue: BindEngine.Continue =>
      {
        deliverToStream(continue.stanza)
      }
      case success: BindEngine.Success =>
      {
        clientAddress_ = success.address
        registerSession()
        rosterIqProcessor_ = new
            RosterIqProcessor(context_)
        deliverToStream(success.stanza)
        nextState(Processing)
      }
      case failure: BindEngine.Failure =>
      {
        deliverToStream(failure.stanza)
        closeStream()
        stop()
      }
      case exception: BindEngine.Exception =>
      {
        //TODO not use handleException
        handleException(exception.exception)
      }
    }
  }

  private def processStanza(aStanza: Element)
  {
    validateFrom(aStanza) match
    {
      case e: StreamException =>
      {
        //TODO not use handleException
        handleException(e)
      }
      case _ =>
      {
        routeStanza(aStanza)
      }
    }
  }

  //TODO verify routing (stanza kind first)
  private def routeStanza(aStanza: Element)
  {
    //TODO validate stanza attribute
    //TODO if subscribe
    //aStanza.setAttribute(ClientContentConstants.Attributes.FROM, address_.bareAddress())
    aStanza.setAttribute(ClientContentConstants.Attributes.FROM, clientAddress_.fullAddress)

    //TODO to refactor
    val to = aStanza.getAttribute(ClientContentConstants.Attributes.TO)
    logger_.debug("stanza for connection \n" + "\t to:{} \n" + "\t for:{}", to, context_.self)



    //TODO verify routing if to = null, to = domain, to = bareaddress or to = fulladdress
    if ( ( "" equals to ) || ( clientAddress_.bareAddress() equals to ) )
    {
      aStanza.getNamespaceURI match
      {
        case ClientContentConstants.CLIENT_CONTENT_NAMESPACE_URI =>
        {
          aStanza.getLocalName match
          {
            case ClientContentConstants.IQ =>
            {
              logger_.debug("iq\n" + "\t for:{}", context_.self)
              handleIQ(aStanza)
            }
            case ClientContentConstants.PRESENCE =>
            {
              logger_.debug("presence\n" + "\t for:{}", context_.self)
              router_.route(aStanza)
            }
            case ClientContentConstants.MESSAGE =>
            {
              logger_.debug("message\n" + "\t for:{}", context_.self)
              router_.route(aStanza)
            }
            case _ =>
            {
              //TODO error
              logger_.error("error \n" + "\t for:{}", context_.self)
            }
          }
        }
        case _ =>
        {
          //TODO error
          logger_.error("error \n" + "\t for:{}", context_.self)
        }
      }
    }
    /*else if (address_.fullAddress() eq to) {
       //TODO  gtalk => iq => loopback
       //TODO  gtalk => message => loopback
       //TODO  gtalk => presence => no op
   } */
    else
    {
      //TODO manage ServiceUnavailableException... (as runtime exception)
      router_.route(aStanza)
    }
  }


  private def registerSession()
  {
    //TODO manage ServiceUnavailableException... (as runtime exception)
    sessionManager_.registerSession(clientAddress_, TypedActor.self[Connection])
  }

  private def unregisterSession()
  {
    //TODO manage ServiceUnavailableException... (as runtime exception)
    sessionManager_.unregisterSession(clientAddress_)
  }

  private def fromThrowable(aCause: Throwable): Element =
  {
    val error = aCause match
    {
      case e: StreamException =>
      {
        logger_.error(e.getCause, "handleException \n" + "\t error:{}\n" + "\t for:{}", e.error(), context_.self)
        e.error()
      }
      case e =>
      {
        logger_.error(e, "handleException \n" + "\t for:{}", context_.self)
        Error.INTERNAL_SERVER_ERROR
      }
    }
    StreamErrorFactory.newError(error)
  }

  /**
   * see http://xmpp.org/rfcs/rfc6120.html#stanzas-attributes-from-c2s
   * @param aStanza stanza to validate
   */
  private def validateFrom(aStanza: Element): StreamException =
  {
    val fromAttr = aStanza.getAttributeNode(ClientContentConstants.Attributes.FROM)
    var from: Address = null
    if ( fromAttr != null )
    {
      try
      {
        from = Addresses.fromString(fromAttr.getValue)
        if ( from.hasResource )
        {
          if ( !clientAddress_.fullAddress.equals(from.fullAddress) )
          {
            return new
                StreamException(Error.INVALID_FROM)
          }
        }
        else
        {
          if ( !clientAddress_.bareAddress.equals(from.bareAddress) )
          {
            return new
                StreamException(Error.INVALID_FROM)
          }
        }
      }
      catch
        {
          case e: Throwable =>
          {
            return new
                StreamException(Error.INVALID_FROM, e.getMessage, e)
          }
        }
    }
    null
  }


  //TODO to refactor
  //TODO validate iq
  private def handleIQ(aStanza: Element)
  {
    val stanzaType: StanzaType = aStanza.getFirstChild match
    {
      case element: Element =>
      {
        element.getNamespaceURI match
        {
          case RosterConstants.ROSTER_NAMESPACE_URI =>
          {
            rosterIqProcessor_.processStanza(clientAddress_, element)
          }
          case SessionConstants.SESSION_NAMESPACE_URI =>
          {
            sessionIqProcessor_.processStanza(clientAddress_, element)
          }
          case _ =>
          {
            //TODO error
            logger_.error("error \n" + "\t for:{}", context_.self)
            FallbackProcessor.processStanza(clientAddress_, aStanza)
          }
        }
      }
      case _ =>
      {
        //TODO error
        logger_.error("error \n" + "\t for:{}", context_.self)
        FallbackProcessor.processStanza(clientAddress_, aStanza)
      }
    }


    val iq = stanzaType match
    {
      case error: IqProcessor.Error =>
      {
        val iq = ClientContentFactory.newIq(ClientContentConstants.Type.ERROR,
                                            aStanza.getAttribute(ClientContentConstants.Attributes.ID), getTo(aStanza),
                                            clientAddress_)
        iq.appendChild(iq.getOwnerDocument.adoptNode(aStanza.getFirstChild))
        iq.appendChild(iq.getOwnerDocument.adoptNode(error.element))
        iq
      }
      case result: IqProcessor.Result =>
      {
        val iq = ClientContentFactory.newIq(Type.RESULT, aStanza.getAttribute(ClientContentConstants.Attributes.ID),
                                            getTo(aStanza), clientAddress_)
        result.element match
        {
          case element: Element =>
          {
            iq.appendChild(iq.getOwnerDocument.adoptNode(element))
          }
          case _ =>
          {
            //Nothing to append
          }
        }
        iq
      }
      case _ =>
      {
        null
      }
    }

    deliverToStream(iq)
  }

  //TODO to finish (to rules)
  private def getTo(aStanza: Element): Address =
  {
    aStanza.getAttributeNode(ClientContentConstants.Attributes.TO) match
    {
      case attr: Attr =>
      {
        Addresses.fromString(attr.getValue)
      }
      case _ =>
      {
        null
      }
    }

  }

  private def nextState(aNextState: State)
  {
    state_ = state_ match
    {
      case Stopped =>
      {
        state_
      }
      case _ =>
      {
        aNextState
      }
    }
  }

  private def stop()
  {
    state_ = state_ match
    {
      case Stopped =>
      {
        state_
      }
      case _ =>
      {
        streamChannel_.get.close()
        streamChannel_ = None
        context_.self.tell(PoisonPill)
        Stopped
      }
    }
  }

  private def openStream()
  {
    val header = new
        Header(VERSION, serverAddress_, clientAddress_, connectionId_, lang_)
    streamChannel_.get.writeHeader(header)
  }

  private def deliverToStream(element: Element)
  {
    streamChannel_.get.writeStanza(element)
  }

  private def closeStream()
  {
    streamChannel_.get.writeFooter()
  }

  private def resetStream()
  {
    streamChannel_.get.reset()
  }

  private def bounce(aStanza: Element)
  {
    //TODO bounce
  }
}
