/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import fr.matoeil.xinshi.xmpp.core.stream.{Header, StreamChannel}
import org.w3c.dom.Element

class NoOpStreamChannel
  extends StreamChannel
{


  override def writeStanza(aElement: Element)
  {
    //no op
  }

  override def writeHeader(aHeader: Header)
  {
    //no op
  }

  override def writeFooter()
  {
    //no op
  }

  override def reset()
  {
    //no op
  }

  override def close()
  {
    //no op
  }
}
