/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.activation.server

import fr.matoeil.xinshi.core.internal.activation.OsgiServiceLifeCycleListener
import fr.matoeil.xinshi.core.internal.server.Server
import fr.matoeil.xinshi.spi.Route
import java.util.{Dictionary, Hashtable}
import javax.inject.Inject
import org.osgi.framework.BundleContext

class ServerServiceLifeCycleListener @Inject()(aBundleContext_ : BundleContext)
  extends OsgiServiceLifeCycleListener[Server](aBundleContext_)
{


  protected def getInterfaces =
  {


    Array(classOf[Server].getName, classOf[Route].getName)


  }

  protected override def getProperties =
  {
    val properties: Dictionary[String, Object] = new
        Hashtable()
    properties.put(Route.ROUTE_SUBDOMAIN, "")
    properties
  }
}