/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.activation

import akka.actor.ActorSystem
import cluster.ClusterModule
import com.google.inject.Guice
import connectionmanager.ConnectionManagerModule
import fr.matoeil.xinshi.core.internal.cluster.Cluster
import javax.inject.Inject
import org.ops4j.peaberry.Peaberry._
import org.osgi.framework.{BundleActivator, BundleContext}
import router.RouterModule
import server.ServerModule
import sessionmanager.SessionManagerModule

class Activator
  extends BundleActivator
{
  @Inject
  private var actorSystem_ : ActorSystem = null

  @Inject
  private var cluster_ : Cluster = null

  def start(context: BundleContext)
  {
    Guice.createInjector(osgiModule(context), new
        XinshiModule, new
                             ConnectionManagerModule, new
                             RouterModule, new
                             ServerModule, new
                             SessionManagerModule, new
                             ClusterModule).injectMembers(this)

    cluster_.start()
  }

  def stop(context: BundleContext)
  {
    actorSystem_.shutdown()
    actorSystem_.awaitTermination()
    actorSystem_ = null
    cluster_.stop()
  }
}
