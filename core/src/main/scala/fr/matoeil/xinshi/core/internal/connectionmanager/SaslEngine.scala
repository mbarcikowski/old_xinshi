/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import fr.matoeil.xinshi.xmpp.core.stream.sasl.SaslConstants.Failure
import fr.matoeil.xinshi.xmpp.core.stream.sasl.{SaslFactory, SaslConstants}
import fr.matoeil.xinshi.xmpp.core.stream.{StreamException, StreamErrorConstants}
import java.io.ByteArrayOutputStream
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import org.apache.shiro.codec.{Hex, Base64}
import org.apache.shiro.crypto.SecureRandomNumberGenerator
import org.apache.shiro.crypto.hash.{Sha1Hash, Md5Hash}
import org.apache.shiro.util.ByteSource
import org.w3c.dom.Element
import scala.Predef._
import scala.{Array, Boolean}

object SaslEngine
{

  sealed trait Result

  case class Exception(exception: StreamException)
    extends Result

  case class Failure(stanza: Element)
    extends Result

  case class Continue(stanza: Element)
    extends Result

  case class Success(username: String, stanza: Element)
    extends Result

}

class SaslEngine
{

  sealed trait SaslState

  case object Initiation
    extends SaslState

  case class Response(mechanism: String)
    extends SaslState

  case object FinalResponse
    extends SaslState

  case class DigestMD5(username: String, realm: String, authzid: String, nonce: String, cnonce: String, nc: Int,
                       qop: String, digestUri: String, response: String, charset: String)

  case class CRAMMD5(username: String, digest: String)

  private var saslState: SaslState = Initiation

  private val secureRandomNumberGenerator_ = new
      SecureRandomNumberGenerator

  private val UTF_8_Charset_ = "UTF-8"

  private val ISO_8859_1_Charset_ = "8859_1"

  private val padding_ = "0"

  private val challengeCharset_ = "utf-8"

  private val qop_ = "auth"

  private val algorithm_ = "md5-sess"

  private var username_ : String = null

  private var realm_ : String = null

  private var nonce_ : String = null

  private var password_ : String = null

  def feature: Element =
  {
    SaslFactory.feature("DIGEST-MD5", "CRAM-MD5")
  }

  def handleStanza(aStanza: Element): SaslEngine.Result =
  {
    aStanza.getNamespaceURI match
    {
      case SaslConstants.SASL_NAMESPACE_URI =>
      {
        saslState match
        {
          case Initiation =>
          {
            processInitiation(aStanza)
          }
          case Response(mechanism) =>
          {
            processResponse(mechanism, aStanza)
          }
          case FinalResponse =>
          {
            processFinalResponse(aStanza)
          }
          case _ =>
          {
            SaslEngine.Exception(new
                                     StreamException(StreamErrorConstants.Error.INTERNAL_SERVER_ERROR))
          }
        }
      }
      case _ =>
      {
        SaslEngine.Exception(new
                                 StreamException(StreamErrorConstants.Error.NO_AUTHORIZED))
      }
    }
  }


  private def processInitiation(aStanza: Element): SaslEngine.Result =
  {
    aStanza.getLocalName match
    {
      case SaslConstants.AUTH =>
      {
        validateMechanism(aStanza)
      }
      case SaslConstants.ABORT =>
      {
        saslState = null
        SaslEngine.Failure(SaslFactory.failure(Failure.ABORTED))
      }
      case _ =>
      {
        saslState = null
        SaslEngine.Failure(SaslFactory.failure(Failure.NOT_AUTHORIZED))
      }
    }
  }


  private def validateMechanism(aStanza: Element): SaslEngine.Result =
  {
    aStanza.getAttribute(SaslConstants.MECHANISM) match
    {
      case "DIGEST-MD5" =>
      {
        saslState = Response("DIGEST-MD5")
        val challenge = generateDigestMD5InitialChallenge()
        SaslEngine.Continue(SaslFactory.fromChallenge(challenge))
      }
      case "CRAM-MD5" =>
      {
        saslState = Response("CRAM-MD5")
        val challenge = generateCRAMMD5InitialChallenge()
        SaslEngine.Continue(SaslFactory.fromChallenge(challenge))
      }
      case _ =>
      {
        saslState = null
        SaslEngine.Failure(SaslFactory.failure(Failure.INVALID_MECHANISM))
      }
    }
  }


  private def processResponse(aMechanism: String, aStanza: Element): SaslEngine.Result =
  {
    aStanza.getLocalName match
    {
      case SaslConstants.RESPONSE =>
      {
        validateResponse(aMechanism, aStanza)
      }
      case SaslConstants.ABORT =>
      {
        SaslEngine.Failure(SaslFactory.failure(Failure.ABORTED))
      }
      case _ =>
      {
        SaslEngine.Failure(SaslFactory.failure(Failure.NOT_AUTHORIZED))
      }
    }
  }


  def validateResponse(aMechanism: String, aResponse: Element): SaslEngine.Result =
  {
    aResponse.getTextContent match
    {
      case s: String =>
      {
        aMechanism match
        {
          case "DIGEST-MD5" =>
          {
            val digest = decodeDigestMD5Response(s)
            if ( validateDigestMD5(digest) )
            {
              saslState = FinalResponse
              val challenge = generateDigestMD5Challenge(digest)
              SaslEngine.Continue(SaslFactory.fromChallenge(challenge))
            }
            else
            {
              saslState = null
              SaslEngine.Failure(SaslFactory.failure(Failure.NOT_AUTHORIZED))
            }
          }
          case "CRAM-MD5" =>
          {
            val digest = decodeCRAMMD5Response(s)
            if ( validateCRAMMD5(digest) )
            {
              saslState = null
              SaslEngine.Success(username_, SaslFactory.success())
            }
            else
            {
              saslState = null
              SaslEngine.Failure(SaslFactory.failure(Failure.NOT_AUTHORIZED))
            }
          }
          case _ =>
          {
            saslState = null
            SaslEngine.Failure(SaslFactory.failure(Failure.INVALID_MECHANISM))
          }
        }

      }
      case _ =>
      {
        saslState = null
        SaslEngine.Failure(SaslFactory.failure(Failure.MALFORMED_REQUEST))
      }
    }
  }


  private def processFinalResponse(aStanza: Element): SaslEngine.Result =
  {
    aStanza.getLocalName match
    {
      case SaslConstants.RESPONSE =>
      {
        saslState = null
        SaslEngine.Success(username_, SaslFactory.success())
      }
      case SaslConstants.ABORT =>
      {
        saslState = null
        SaslEngine.Failure(SaslFactory.failure(Failure.ABORTED))
      }
      case _ =>
      {
        saslState = null
        SaslEngine.Failure(SaslFactory.failure(Failure.NOT_AUTHORIZED))
      }
    }
  }

  private def createNewNonce(): String =
  {
    val randomBytes: ByteSource = secureRandomNumberGenerator_.nextBytes
    new
        Sha1Hash(randomBytes).toHex
  }


  private def retrievePassword(aUsername: String): String =
  {
    aUsername
  }

  private def generateDigestMD5InitialChallenge(): String =
  {
    realm_ = "localhost.machin.truc"; //TODO get realm from config
    nonce_ = createNewNonce()
    val challenge = "realm=\"" + realm_ + "\",nonce=\"" + nonce_ + "\",qop=\"" + qop_ + "\",charset=" +
      challengeCharset_ + ",algorithm=" + algorithm_
    Base64.encodeToString(challenge.getBytes(UTF_8_Charset_))
  }


  private def decodeDigestMD5Response(s: String): DigestMD5 =
  {
    var username: String = null
    var realm: String = null
    var authzid: String = null
    var nonce: String = null
    var cnonce: String = null
    var nc: Int = 0
    var qop: String = null
    var digestUri: String = null
    var response: String = null
    var charset: String = null

    val decoded = Base64.decodeToString(s)
    val splitEntry = decoded.split(",")

    for ( entry <- splitEntry )
    {
      val index = entry.indexOf('=')
      val key: String = entry.substring(0, index)
      val value: String = entry.substring(index + 1)
      key match
      {
        case "username" =>
        {
          username = value.replace("\"", "")
        }
        case "realm" =>
        {
          realm = value.replace("\"", "")
        }
        case "authzid" =>
        {
          authzid = value.replace("\"", "")
        }
        case "nonce" =>
        {
          nonce = value.replace("\"", "")
        }
        case "cnonce" =>
        {
          cnonce = value.replace("\"", "")
        }
        case "nc" =>
        {
          nc = value.toInt
        }
        case "qop" =>
        {
          qop = value
        }
        case "digest-uri" =>
        {
          digestUri = value.replace("\"", "")
        }
        case "response" =>
        {
          response = value
        }
        case "charset" =>
        {
          charset = value
        }
        case _ =>
        {

        }
      }
    }

    DigestMD5(username, realm, authzid, nonce, cnonce, nc, qop, digestUri, response, charset)
  }


  private def validateDigestMD5(aDigest: DigestMD5): Boolean =
  {
    username_ = aDigest.username
    password_ = retrievePassword(aDigest.username)
    val responseValue = createDigestMD5(Some("AUTHENTICATE"), aDigest)
    responseValue equals aDigest.response
  }

  private def generateDigestMD5Challenge(aDigest: DigestMD5): String =
  {
    val responseValue = createDigestMD5(None, aDigest)
    val responseAuth = "rspauth=" + responseValue
    Base64.encodeToString(responseAuth.getBytes(UTF_8_Charset_))
  }


  private def createDigestMD5(aAuthMethod: Option[String], aDigest: DigestMD5): String =
  {
    val beginA1 = new
        ByteArrayOutputStream
    beginA1.write(stringToByte_8859_1(username_))
    beginA1.write(':')
    beginA1.write(stringToByte_8859_1(realm_))
    beginA1.write(':')
    beginA1.write(stringToByte_8859_1(password_))

    val A1 = new
        ByteArrayOutputStream()
    A1.write(new
                 Md5Hash(beginA1.toByteArray).getBytes)
    A1.write(':')
    A1.write(nonce_.getBytes(UTF_8_Charset_))
    A1.write(':')
    A1.write(aDigest.cnonce.getBytes(UTF_8_Charset_))

    if ( aDigest.authzid != null )
    {
      A1.write(':')
      A1.write(aDigest.authzid.getBytes(UTF_8_Charset_))
    }

    val A2 = new
        ByteArrayOutputStream()
    A2.write(( aAuthMethod.getOrElse("") + ":" + aDigest.digestUri ).getBytes(UTF_8_Charset_))

    val KD = new
        ByteArrayOutputStream()
    KD.write(new
                 Md5Hash(A1.toByteArray).toHex.getBytes(UTF_8_Charset_))
    KD.write(':')
    KD.write(nonce_.getBytes(UTF_8_Charset_))
    KD.write(':')
    KD.write(nonceCountToHex(aDigest.nc).getBytes(UTF_8_Charset_))
    KD.write(':')
    KD.write(aDigest.cnonce.getBytes(UTF_8_Charset_))
    KD.write(':')
    KD.write(qop_.getBytes(UTF_8_Charset_))
    KD.write(':')
    KD.write(new
                 Md5Hash(A2.toByteArray).toHex.getBytes(UTF_8_Charset_))

    new
        Md5Hash(KD.toByteArray).toHex
  }


  private def stringToByte_8859_1(str: String): Array[Byte] =
  {
    val buffer: Array[Char] = str.toCharArray
    for ( i <- 0 until buffer.length )
    {
      if ( buffer(i) > '\u00FF' )
      {
        return str.getBytes(UTF_8_Charset_)
      }
    }
    str.getBytes(ISO_8859_1_Charset_)
  }

  private def nonceCountToHex(count: Int): String =
  {
    val str: String = Integer.toHexString(count)
    val pad: StringBuffer = new
        StringBuffer
    if ( str.length < 8 )
    {
      for ( i <- 0 until 8 - str.length )
      {
        pad.append(padding_)
      }
    }
    pad.toString + str
  }

  private def generateCRAMMD5InitialChallenge(): String =
  {
    nonce_ = createNewNonce()
    val challenge = "<" + Base64.encodeToString(nonce_.getBytes(UTF_8_Charset_)) + ">"
    Base64.encodeToString(challenge.getBytes(UTF_8_Charset_))
  }

  private def decodeCRAMMD5Response(s: String): CRAMMD5 =
  {
    var username: String = null
    var digest: String = null

    val decoded = Base64.decodeToString(s)
    val splitEntry = decoded.split(" ")

    var index = 0

    for ( entry <- splitEntry )
    {
      index match
      {
        case 0 =>
        {
          username = entry
        }
        case 1 =>
        {
          digest = entry
        }
        case _ =>
        {

        }
      }
      index = index + 1
    }

    CRAMMD5(username, digest)
  }


  private def validateCRAMMD5(aDigest: CRAMMD5): Boolean =
  {
    username_ = aDigest.username
    password_ = retrievePassword(aDigest.username)
    val responseValue = createCRAMMD5()
    responseValue equals aDigest.digest
  }

  private def createCRAMMD5(): String =
  {
    val challenge = "<" + Base64.encodeToString(nonce_.getBytes(UTF_8_Charset_)) + ">"
    val challengeInBytes = challenge.getBytes(UTF_8_Charset_)
    val mac = Mac.getInstance("HmacMD5")
    val secretKeySpec = new
        SecretKeySpec(password_.getBytes(UTF_8_Charset_), "HmacMD5")
    mac.init(secretKeySpec)
    val digest = mac.doFinal(challengeInBytes)
    Hex.encodeToString(digest)
  }
}
