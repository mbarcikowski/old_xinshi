/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.sessionmanager.actor

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{Actor, OneForOneStrategy}
import akka.event.Logging
import com.google.common.collect.Maps
import fr.matoeil.xinshi.api.Connection
import fr.matoeil.xinshi.core.internal.sessionmanager.actor.SessionRouteeActor.{Route, Unregister, Register}
import fr.matoeil.xinshi.xmpp.core.address.Address
import fr.matoeil.xinshi.xmpp.core.content.ClientContentConstants
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants.Error
import fr.matoeil.xinshi.xmpp.core.stream.StreamException
import org.w3c.dom.Element

object SessionRouteeActor
{

  case class Register(address: Address, connection: Connection)

  case class Unregister(address: Address)

  case class Route(to: Address, stanza: Element)

}

class SessionRouteeActor
  extends Actor
{
  private val logger_ = Logging.getLogger(context.system, this)

  private val addressToConnection_ = Maps.newTreeMap[String, Connection]()

  override val supervisorStrategy = OneForOneStrategy()
  {
    case e ⇒
    {
      logger_.error(e, "Throwable detected \n" + "\t by:{}", context.self)
      Resume
    }
  }

  override def preStart()
  {
    super.preStart()
    logger_.debug("preStart\n" + "\t for:{}\n", context.self)
  }

  override def postRestart(reason: Throwable)
  {
    super.postRestart(reason)
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context.self)
  }

  override def postStop()
  {
    super.postStop()
    logger_.debug("postStop")
    import scala.collection.JavaConversions._
    for ( connection <- addressToConnection_.values() )
    {
      connection.handleException(new
                                     StreamException(Error.SYSTEM_SHUTDOWN))
    }
    addressToConnection_.clear()
  }

  override def preRestart(reason: Throwable, message: Option[Any])
  {
    super.preRestart(reason, message)
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{} \n" + "\t for:{}\n", reason, message,
                  context.self)
  }

  override def receive =
  {
    case register: Register =>
    {
      registerConnection(register)
    }
    case unregister: Unregister =>
    {
      unregisterConnection(unregister)
    }
    case route: Route =>
    {
      routeStanza(route)
    }
    case message =>
    {
      logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", message, sender,
                    context.self)
    }
  }


  private def registerConnection(register: SessionRouteeActor.Register)
  {
    logger_.debug("onReceive \n" + "\t register:{} \n" + "\t sender:{} \n" + "\t for:{}", register, sender,
                  context.self)
    addressToConnection_.put(register.address.fullAddress(), register.connection)
  }

  private def unregisterConnection(unregister: SessionRouteeActor.Unregister)
  {
    logger_.debug("onReceive \n" + "\t unregister:{} \n" + "\t sender:{} \n" + "\t for:{}", unregister, sender,
                  context.self)
    addressToConnection_.remove(unregister.address.fullAddress())
  }

  private def routeStanza(route: SessionRouteeActor.Route)
  {
    logger_.debug("onReceive \n" + "\t route:{} \n" + "\t sender:{} \n" + "\t for:{}", route, sender, context.self)

    val stanza = route.stanza
    stanza.getLocalName match
    {
      case ClientContentConstants.IQ =>
      {
        routeIq(route.to, stanza)
      }
      case ClientContentConstants.MESSAGE =>
      {
        routeMessage(route.to, stanza)
      }
      case ClientContentConstants.PRESENCE =>
      {
        routePresence(route.to, stanza)
      }
    }
  }

  private def routeIq(aTo: Address, aStanza: Element)
  {
    aTo.resource() match
    {
      case resource: String =>
      {
        addressToConnection_.get(aTo.fullAddress()) match
        {
          case connection: Connection =>
          {
            connection.deliver(aStanza)
          }
          case null =>
          {
            routeIqToBareAddress(aTo, aStanza)

          }
        }
      }
      case _ =>
      {
        routeIqToBareAddress(aTo, aStanza)
      }
    }
  }


  //TODO verify routing (stanza kind first, according xmpp im), handle By server
  //TODO see xmpp im => 8.5.2.1.3.  , 8.5.2.2.3. , 8.5.3.1. , 8.5.3.2.3.
  private def routeIqToBareAddress(aTo: Address, aStanza: Element)
  {

    logger_.debug("routeIqToBareAddress \n" + "\t to:{} \n" + "\t stanza:{} \n" + "\t sender:{} \n" + "\t for:{}", aTo,
                  aStanza, sender, context.self)
  }


  private def routeMessage(aTo: Address, aStanza: Element)
  {
    aTo.resource() match
    {
      case resource: String =>
      {
        addressToConnection_.get(aTo.fullAddress()) match
        {
          case connection: Connection =>
          {
            connection.deliver(aStanza)
          }
          case null =>
          {
            routeMessageToBareAddress(aTo, aStanza)

          }
        }
      }
      case _ =>
      {
        routeMessageToBareAddress(aTo, aStanza)

      }
    }
  }

  //TODO verify routing (stanza kind first, according xmpp im)
  //TODO see xmpp im => 8.5.2.1.1., 8.5.2.2.1. , 8.5.3.2.1.
  //TODO gtalk => broadcast except from (move test into connection ? )
  //TODO test initialPresence ? (move test into connection ? )
  private def routeMessageToBareAddress(aTo: Address, aStanza: Element)
  {
    import scala.collection.JavaConversions._
    for ( connection <- addressToConnection_.values() )
    {
      connection.deliver(aStanza)
    }
  }

  private def routePresence(aTo: Address, aStanza: Element)
  {
    aTo.resource() match
    {
      case resource: String =>
      {
        addressToConnection_.get(aTo.fullAddress()) match
        {
          case connection: Connection =>
          {
            connection.deliver(aStanza)
          }
          case null =>
          {
            routePresenceToBareAddress(aTo, aStanza)

          }
        }
      }
      case _ =>
      {
        routePresenceToBareAddress(aTo, aStanza)

      }
    }
  }

  //TODO verify routing (stanza kind first, according xmpp im)
  // TODO see xmpp im => 8.5.2.1.2. , 8.5.2.2.2., 8.5.3.2.2.
  //TODO gtalk => presence =>  broadcast except from (move test into connection ? )
  //TODO test initialPresence ? (move test into connection ? )
  private def routePresenceToBareAddress(aTo: Address, aStanza: Element)
  {
    import scala.collection.JavaConversions._
    for ( connection <- addressToConnection_.values() )
    {
      connection.deliver(aStanza)
    }
  }
}
