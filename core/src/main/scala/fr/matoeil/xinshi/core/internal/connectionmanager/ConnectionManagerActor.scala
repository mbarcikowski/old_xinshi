/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.connectionmanager

import akka.actor.SupervisorStrategy.Stop
import akka.actor.TypedActor._
import akka.actor.{TypedProps, ActorRef, OneForOneStrategy, TypedActor}
import akka.event.Logging
import fr.matoeil.xinshi.api.{ConnectionManager, Connection}
import fr.matoeil.xinshi.xmpp.core.stream.StreamChannel
import javax.inject.{Inject, Provider}
import org.apache.shiro.crypto.SecureRandomNumberGenerator
import org.apache.shiro.crypto.hash.Sha1Hash
import org.apache.shiro.util.ByteSource

class ConnectionManagerActor @Inject()(private val connectionActorProvider_ : Provider[ConnectionActor])
  extends ConnectionManager with Supervisor with PreStart with PostStop with PreRestart with PostRestart with Receiver
{

  val context_ = TypedActor.context

  private val secureRandomNumberGenerator_ = new
      SecureRandomNumberGenerator

  private val logger_ = Logging.getLogger(context_.system, this)

  override def provideConnection(streamChannel: StreamChannel): Connection =
  {
    val connectionId: String = createNewConnectionId

    val connection: Connection = TypedActor(context_).typedActorOf(
      TypedProps(classOf[Connection], connectionActorProvider_.get().init(connectionId, streamChannel)),
      "connection-" + connectionId)
    connection
  }

  private def createNewConnectionId: String =
  {
    val randomBytes: ByteSource = secureRandomNumberGenerator_.nextBytes
    new
        Sha1Hash(randomBytes).toHex
  }

  override val supervisorStrategy = OneForOneStrategy()
  {
    case e: Throwable ⇒
    {
      logger_.error(e, "Throwable detected \n" + "\t by:{}", context_.self)
      Stop
    }
  }


  override def preStart()
  {
    logger_.debug("preStart\n" + "\t for:{}\n", context_.self)
  }

  override def postRestart(reason: Throwable)
  {
    logger_.error(reason, "postRestart \n" + "\t reason:{}\n" + "\t for:{}\n", reason, context_.self)
  }

  override def postStop()
  {
    logger_.debug("postStop\n" + "\t for:{}\n", context_.self)
  }


  def preRestart(reason: Throwable, message: Option[Any])
  {
    logger_.error(reason, "preRestart \n" + "\t reason:{} \n" + "\t message:{}\n" + "\t for:{}\n", reason, message,
                  context_.self)
  }

  def onReceive(message: Any, sender: ActorRef)
  {
    logger_.debug("onReceive \n" + "\t message:{} \n" + "\t sender:{} \n" + "\t for:{}", message, sender, context_.self)
  }


}
