/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.core.internal.activation.router

import fr.matoeil.xinshi.api.Router
import fr.matoeil.xinshi.core.internal.activation.OsgiServiceLifeCycleListener
import javax.inject.Inject
import org.osgi.framework.BundleContext

class RouterServiceLifeCycleListener @Inject()(aBundleContext_ : BundleContext)
  extends OsgiServiceLifeCycleListener[Router](aBundleContext_)
{


  protected def getInterfaces =
  {
    Array(classOf[Router].getName, classOf[Router].getName)
  }

}