/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.spi;

import org.w3c.dom.Element;

/**
 * Define a route.
 * The address of the route is determined by the subdomain of the route and the server domain.
 */
public interface Route
{
    /**
     * Route property identifying subdomain of a route.
     */
    static final String ROUTE_SUBDOMAIN = "xinshi.router.route.subdomain";

    /**
     * Handle stanza whose destination is the route.
     *
     * @param aStanza
     */
    void handleStanza( Element aStanza );
}
