/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.api;

import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamListener;
import org.w3c.dom.Element;

public interface Connection
    extends StreamListener
{

    @Override
    void handleStreamOpening( Header aHeader );

    @Override
    void handleIncomingStanza( Element aElement );

    @Override
    void handleStreamClosing();

    void handleException( Throwable aCause );

    void handleStreamDisconnecting();

    void deliver( Element aStanza );

    void handleKeepAlive();
}
