/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.roster;

import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import fr.matoeil.xinshi.xmpp.core.stream.StreamConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.dom.DOMResult;
import java.util.Collection;

//TODO migrate to a builder
public class RosterFactory
{

    private final static XMLOutputFactory XML_OUTPUT_FACTORY = XMLOutputFactory.newFactory();

    private final static DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();

    private static DocumentBuilder DOCUMENT_BUILDER;

    static
    {
        try
        {
            DOCUMENT_BUILDER = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
        }
        catch ( ParserConfigurationException e )
        {
            throw Throwables.propagate( e );
        }
    }

    public static Element newQuery( Item... items )
        throws XMLStreamException
    {
        Document doc = DOCUMENT_BUILDER.newDocument();
        XMLStreamWriter streamWriter = XML_OUTPUT_FACTORY.createXMLStreamWriter( new DOMResult( doc ) );
        streamWriter.writeStartDocument( StreamConstants.XML_ENCODING, StreamConstants.XML_VERSION );
        streamWriter.writeStartElement( "", RosterConstants.QUERY, RosterConstants.ROSTER_NAMESPACE_URI );
        streamWriter.writeDefaultNamespace( RosterConstants.ROSTER_NAMESPACE_URI );
        for ( Item item : items )
        {
            streamWriter.writeStartElement( "", RosterConstants.Item.ITEM, RosterConstants.ROSTER_NAMESPACE_URI );
            streamWriter.writeAttribute( RosterConstants.Item.Attributes.JID, item.jid().bareAddress() );
            streamWriter.writeAttribute( RosterConstants.Item.Attributes.NAME, item.name() );
            streamWriter.writeAttribute( RosterConstants.Item.Attributes.SUBSCRIPTION, item.subscription().toString() );
            streamWriter.writeEndElement();
        }
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();

        Collection<String> tot = Lists.newArrayList( "a", "b" );
        for ( String a : tot )
        {
            a.trim();
        }

        return doc.getDocumentElement();
    }


}
