/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.roster;

public class RosterConstants
{
    public static final String ROSTER_NAMESPACE_URI = "jabber:iq:roster";

    public static final String QUERY = "query";

    public static class Attributes
    {
        public static final String VER = "ver";
    }

    public static class Item
    {
        public static final String ITEM = "item";

        public static final String GROUP = "group";

        public static class Attributes
        {
            public static final String JID = "jid";

            public static final String NAME = "name";

            public static final String SUBSCRIPTION = "subscription";

            public static final String APPROVED = "approved";

            public static final String ASK = "ask";
        }
    }

    public static enum Ask
    {
        SUBSCRIBE
            {
                @Override
                public String toString()
                {
                    return "subscribe";
                }
            }
    }

    public static enum Subscription
    {
        BOTH
            {
                @Override
                public String toString()
                {
                    return "both";
                }
            },
        FROM
            {
                @Override
                public String toString()
                {
                    return "from";
                }
            },
        NONE
            {
                @Override
                public String toString()
                {
                    return "none";
                }
            },
        REMOVE
            {
                @Override
                public String toString()
                {
                    return "remove";
                }
            },
        TO
            {
                @Override
                public String toString()
                {
                    return "to";
                }
            }
    }

}
