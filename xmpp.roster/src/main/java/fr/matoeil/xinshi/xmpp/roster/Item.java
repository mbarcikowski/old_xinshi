/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.roster;

import fr.matoeil.xinshi.xmpp.core.address.Address;

import static fr.matoeil.xinshi.xmpp.roster.RosterConstants.Subscription;

//TODO finish to add missing attributes
public class Item
{
    private final Address jid_;

    private final String name_;

    private final Subscription subscription_;

    public Item( Address aJid, String aName, Subscription aSubscription )
    {
        jid_ = aJid;
        name_ = aName;
        subscription_ = aSubscription;
    }

    public Address jid()
    {
        return jid_;
    }

    public String name()
    {
        return name_;
    }

    public Subscription subscription()
    {
        return subscription_;
    }
}
