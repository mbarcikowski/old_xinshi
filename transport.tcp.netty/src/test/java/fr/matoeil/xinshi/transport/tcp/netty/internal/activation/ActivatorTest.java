/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal.activation;

import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * test Activator class.
 */
public class ActivatorTest
{
    private Activator activator_;

    private BundleContext mockBundleContext_;

    @Before
    public void setUp()
        throws Exception
    {
        activator_ = new Activator();
        mockBundleContext_ = mock( BundleContext.class );
        Bundle mockBundle = mock( Bundle.class );
        when( mockBundleContext_.getBundle() ).thenReturn( mockBundle );
    }

    @Test
    public void testStop_after_start()
        throws Exception
    {
        activator_.start( mockBundleContext_ );

        activator_.stop( mockBundleContext_ );
    }
}
