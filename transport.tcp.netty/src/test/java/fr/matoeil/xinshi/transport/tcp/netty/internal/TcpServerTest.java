/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class TcpServerTest
{
    private TcpServer tcpServer_;

    private ChannelGroup mockChannelGroup_;


    @Before
    public void setUp()
        throws Exception
    {
        mockChannelGroup_ = spy( new DefaultChannelGroup() );

        ChannelPipelineFactory mockChannelPipelineFactory = mock( ChannelPipelineFactory.class );

        tcpServer_ = new TcpServer( mockChannelGroup_, mockChannelPipelineFactory );
    }

    @Test
    public void testStart_then_Stop()
        throws Exception
    {
        tcpServer_.start();

        verify( mockChannelGroup_, times( 1 ) ).add( notNull( Channel.class ) );

        tcpServer_.stop();
    }
}
