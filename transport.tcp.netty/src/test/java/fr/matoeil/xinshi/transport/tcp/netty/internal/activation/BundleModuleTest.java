/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal.activation;

import com.google.inject.Guice;
import com.google.inject.Injector;
import fr.matoeil.xinshi.transport.tcp.netty.internal.TcpServer;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.ops4j.peaberry.Peaberry.osgiModule;

public class BundleModuleTest
{

    private Injector injector_;

    @Before
    public void setUp()
        throws Exception
    {
        BundleContext mockBundleContext = mock( BundleContext.class );
        Bundle mockBundle = mock( Bundle.class );
        when( mockBundleContext.getBundle() ).thenReturn( mockBundle );
        injector_ = Guice.createInjector( osgiModule( mockBundleContext ), new BundleModule() );
    }

    @Test
    public void testConfigure()
        throws Exception
    {
        TcpServer target = injector_.getInstance( TcpServer.class );

        assertThat( target, is( notNullValue() ) );
    }
}
