/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal;

import fr.matoeil.xinshi.api.Connection;
import fr.matoeil.xinshi.api.ConnectionManager;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.group.ChannelGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.CheckForNull;
import javax.inject.Inject;

public class XmppStreamHandler
    extends SimpleChannelUpstreamHandler
{
    private static final Logger LOGGER = LoggerFactory.getLogger( XmppStreamHandler.class );

    private final ChannelGroup channelGroup_;

    private final ConnectionManager connectionManager_;

    @CheckForNull
    private Connection connection_;

    @Inject
    public XmppStreamHandler( ChannelGroup aChannelGroup, ConnectionManager aConnectionManager )
    {
        channelGroup_ = aChannelGroup;
        connectionManager_ = aConnectionManager;
    }

    @Override
    public void channelConnected( ChannelHandlerContext aContext, ChannelStateEvent aEvent )
        throws Exception
    {
        Channel channel = aEvent.getChannel();
        LOGGER.debug( "channelConnected : {}", channel );
        channelGroup_.add( channel );
        connection_ = connectionManager_.provideConnection( new ChannelAdapter( channel ) );
        StreamCodec streamCodec = new StreamCodec( connection_ );
        aContext.getPipeline().addFirst( "xmpp_stream_codec", streamCodec );
    }

    @Override
    public void channelDisconnected( ChannelHandlerContext ctx, ChannelStateEvent aEvent )
        throws Exception
    {
        Channel channel = aEvent.getChannel();
        LOGGER.debug( "channelDisconnected : {}", channel );
        channelGroup_.remove( channel );
        if ( connection_ != null )
        {
            connection_.handleStreamDisconnecting();
        }
    }

    @Override
    public void messageReceived( ChannelHandlerContext aContext, MessageEvent aEvent )
    {
        Object message = aEvent.getMessage();
        LOGGER.debug( "messageReceived : {}", message );
        if ( message instanceof KeepAlive )
        {
            connection_.handleKeepAlive();
        }
    }

    @Override
    public void exceptionCaught( ChannelHandlerContext aContext, ExceptionEvent aExceptionEvent )
    {
        LOGGER.debug( "exceptionCaught : {}", aExceptionEvent );
        if ( connection_ == null )
        {
            aContext.getChannel().close();
        }
        else
        {
            connection_.handleException( aExceptionEvent.getCause() );
        }
    }
}
