/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import javax.inject.Inject;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * tcp server.
 */
public class TcpServer
{


    public static final int DEFAULT_PORT = 5222;

    private final ChannelPipelineFactory channelPipelineFactory_;

    private final ChannelGroup channelGroup_;

    private ChannelFactory channelFactory_;

    private int port_ = DEFAULT_PORT;

    @Inject
    public TcpServer( ChannelGroup aChannelGroup, ChannelPipelineFactory aChannelPipelineFactory )
    {
        channelGroup_ = aChannelGroup;
        channelPipelineFactory_ = aChannelPipelineFactory;
    }

    public void start()
    {
        channelFactory_ =
            new NioServerSocketChannelFactory( Executors.newCachedThreadPool(), Executors.newCachedThreadPool() );

        ServerBootstrap bootstrap = new ServerBootstrap( channelFactory_ );

        bootstrap.setPipelineFactory( channelPipelineFactory_ );

        bootstrap.setOption( "child.tcpNoDelay", true );
        bootstrap.setOption( "child.keepAlive", true );

        Channel channel = bootstrap.bind( new InetSocketAddress( port_ ) );
        channelGroup_.add( channel );
    }

    public void stop()
    {
        ChannelGroupFuture future = channelGroup_.close();
        future.awaitUninterruptibly();
        channelFactory_.releaseExternalResources();
    }
}
