/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal;

import fr.matoeil.xinshi.api.ConnectionManager;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.group.ChannelGroup;

import javax.inject.Inject;

public class XmppChannelPipelineFactory
    implements ChannelPipelineFactory
{

    private final ChannelGroup channelGroup_;

    private final ConnectionManager connectionManager_;

    @Inject
    public XmppChannelPipelineFactory( ChannelGroup aChannelGroup, ConnectionManager aConnectionManager )
    {
        channelGroup_ = aChannelGroup;
        connectionManager_ = aConnectionManager;
    }

    @Override
    public ChannelPipeline getPipeline()
        throws Exception
    {
        ChannelPipeline pipeline = Channels.pipeline();
        pipeline.addLast( "xmpp_client_handler", new XmppStreamHandler( channelGroup_, connectionManager_ ) );
        return pipeline;
    }


}
