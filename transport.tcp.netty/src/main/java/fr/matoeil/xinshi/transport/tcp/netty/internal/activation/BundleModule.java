/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal.activation;

import com.google.inject.AbstractModule;
import fr.matoeil.xinshi.api.ConnectionManager;
import fr.matoeil.xinshi.transport.tcp.netty.internal.TcpServer;
import fr.matoeil.xinshi.transport.tcp.netty.internal.XmppChannelPipelineFactory;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;

import static org.ops4j.peaberry.Peaberry.service;

/**
 * tcp server bundle configuration module.
 */
public class BundleModule
    extends AbstractModule
{
    @Override
    protected void configure()
    {
        configureImportedServices();

        bind( ChannelGroup.class ).toInstance( new DefaultChannelGroup( "tcp-server" ) );
        bind( ChannelPipelineFactory.class ).to( XmppChannelPipelineFactory.class ).asEagerSingleton();
        bind( TcpServer.class ).asEagerSingleton();
    }

    private void configureImportedServices()
    {
        //import
        bind( ConnectionManager.class ).toProvider( service( ConnectionManager.class ).single() ).asEagerSingleton();
    }

}
