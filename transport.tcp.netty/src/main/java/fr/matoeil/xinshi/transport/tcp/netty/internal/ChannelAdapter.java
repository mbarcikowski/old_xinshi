/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal;

import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamChannel;
import org.jboss.netty.channel.Channel;
import org.w3c.dom.Element;

public class ChannelAdapter
    implements StreamChannel
{
    private final Channel channel_;

    public ChannelAdapter( Channel aChannel )
    {
        channel_ = aChannel;
    }

    @Override
    public void writeHeader( Header aHeader )
    {
        channel_.write( aHeader );
    }

    @Override
    public void writeStanza( Element aElement )
    {
        channel_.write( aElement );
    }

    @Override
    public void writeFooter()
    {
        channel_.write( new Footer() );
    }

    @Override
    public void reset()
    {
        channel_.write( new Reset() );
    }

    @Override
    public void close()
    {
        channel_.close();
        channel_.write( new Close() );
    }
}
