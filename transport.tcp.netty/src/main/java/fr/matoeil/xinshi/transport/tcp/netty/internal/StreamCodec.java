/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.transport.tcp.netty.internal;

import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.stream.StreamListener;
import fr.matoeil.xinshi.xmpp.core.stream.StreamReader;
import fr.matoeil.xinshi.xmpp.core.stream.StreamWriter;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferOutputStream;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelDownstreamHandler;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelUpstreamHandler;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import java.nio.ByteBuffer;

import static org.jboss.netty.channel.Channels.fireMessageReceived;
import static org.jboss.netty.channel.Channels.write;

public class StreamCodec
    implements ChannelUpstreamHandler, ChannelDownstreamHandler
{
    private final static Logger LOGGER = LoggerFactory.getLogger( StreamCodec.class );

    private static final byte WHITESPACE_KEEPALIVE_CHAR = '\u0020';

    private final ChannelBufferOutputStream channelBufferOutputStream_;

    private final StreamReader streamReader_;

    private final StreamWriter streamWriter_;


    public StreamCodec( StreamListener aStreamListener )
    {
        channelBufferOutputStream_ = new ChannelBufferOutputStream( ChannelBuffers.dynamicBuffer() );
        streamReader_ = new StreamReader( aStreamListener );
        streamWriter_ = new StreamWriter( channelBufferOutputStream_ );
    }

    public void handleUpstream( ChannelHandlerContext ctx, ChannelEvent evt )
        throws Exception
    {
        if ( !( evt instanceof MessageEvent ) )
        {
            ctx.sendUpstream( evt );
            return;
        }

        MessageEvent e = (MessageEvent) evt;
        Object originalMessage = e.getMessage();
        Object decodedMessage = decode( ctx, e.getChannel(), originalMessage );
        if ( originalMessage == decodedMessage )
        {
            ctx.sendUpstream( evt );
        }
        else if ( decodedMessage != null )
        {
            fireMessageReceived( ctx, decodedMessage, e.getRemoteAddress() );
        }
    }

    public void handleDownstream( ChannelHandlerContext ctx, ChannelEvent evt )
        throws Exception
    {
        if ( !( evt instanceof MessageEvent ) )
        {
            ctx.sendDownstream( evt );
            return;
        }

        MessageEvent e = (MessageEvent) evt;
        Object originalMessage = e.getMessage();
        Object encodedMessage;
        encodedMessage = encode( ctx, e.getChannel(), originalMessage );
        if ( originalMessage == encodedMessage )
        {
            ctx.sendDownstream( evt );
        }
        else if ( encodedMessage != null )
        {
            write( ctx, e.getFuture(), encodedMessage, e.getRemoteAddress() );
        }
    }

    protected Object decode( ChannelHandlerContext ctx, Channel channel, Object msg )
        throws Exception
    {
        if ( msg instanceof ChannelBuffer )
        {
            ChannelBuffer channelBuffer = (ChannelBuffer) msg;
            LOGGER.debug( "decode '{}'", channelBuffer.toString( CharsetUtil.UTF_8 ) );
            ByteBuffer byteBuffer = channelBuffer.toByteBuffer();
            byte[] byteArray = byteBuffer.array();
            if ( byteArray.length == 1 && WHITESPACE_KEEPALIVE_CHAR == byteArray[0] )
            {
                return new KeepAlive();
            }
            else
            {
                streamReader_.read( byteBuffer );
            }
            return null;
        }
        return msg;
    }

    protected Object encode( ChannelHandlerContext ctx, Channel channel, Object msg )
        throws Exception
    {
        if ( msg instanceof Header )
        {
            Header header = (Header) msg;
            channelBufferOutputStream_.buffer().clear();
            streamWriter_.writeStreamOpening( header );
            ChannelBuffer copy = channelBufferOutputStream_.buffer().copy();
            LOGGER.debug( "encode '{}'", copy.toString( CharsetUtil.UTF_8 ) );
            return copy;
        }
        else if ( msg instanceof Element )
        {
            Element stanza = (Element) msg;
            channelBufferOutputStream_.buffer().clear();
            streamWriter_.writeStanza( stanza );
            ChannelBuffer copy = channelBufferOutputStream_.buffer().copy();
            LOGGER.debug( "encode '{}'", copy.toString( CharsetUtil.UTF_8 ) );
            return copy;
        }
        else if ( msg instanceof Footer )
        {
            channelBufferOutputStream_.buffer().clear();
            streamWriter_.writeStreamClosing();
            ChannelBuffer copy = channelBufferOutputStream_.buffer().copy();
            LOGGER.debug( "encode '{}'", copy.toString( CharsetUtil.UTF_8 ) );
            return copy;
        }
        else if ( msg instanceof Reset )
        {
            streamReader_.reset();
            streamWriter_.reset( channelBufferOutputStream_ );
            return null;
        }
        else if ( msg instanceof Close )
        {
            boolean failed = false;
            try
            {
                streamReader_.close();
            }
            catch ( Exception e )
            {
                failed = true;
                LOGGER.error( "error while closing streamReader", e );
            }
            try
            {
                streamWriter_.close();
            }
            catch ( Exception e )
            {
                failed = true;
                LOGGER.error( "error while closing streamWriter", e );
            }
            if ( failed )
            {
                throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR );
            }
            return null;
        }
        return msg;
    }
}
