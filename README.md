Xinshi aims to be a cross-platform, fault-tolerant, clusterable and modular technology that allows the creation of large-scale instant messaging applications.

Xinshi has a website at [http://www.matoeil.fr/xinshi][0]. Follow [@XinshiXMPP][1] on Twitter for updates on the project.

## ABOUT Xinshi and XMPP

This project aims at delivering a server implementation of both the core and IM parts of the XMPP protocol
(as specified in [RFC 6120][2] + [RFC 6121][3]).

[XMPP][4] is an open, secure and extensible instant messaging protocol which has evolved from Jabber.
It provides interoperability features for communication with other XMPP and non-XMPP servers.
It is used and supported by many IM applications, both client and server.

The protocol and its many extensions (called XEPs) are maintained by the XMPP Standards Foundation (XSF) [XMPP][5].

## Documentation

Documentation and tutorials can be found on the [Xinshi site][0].

## Getting help

Feel free to ask questions on Xinshi's mailing lists: [xinshi-developer][5] & [xinshi-user][6]

## License

The use and distribution terms for this software are covered by the
[the Apache Software License, Version 2.0][7]
which can be found in the file LICENSE.html at the root of this distribution.
By using this software in any fashion, you are agreeing to be bound by
the terms of this license.
You must not remove this notice, or any other, from this software.

## Project lead

* Mathieu Barcikowski ([@mbarcikowski][8])

## Acknowledgements

YourKit is kindly supporting open source projects with its full-featured Java Profiler. YourKit, LLC is the creator of innovative and intelligent tools for profiling Java and .NET applications. Take a look at YourKit's leading software products: [YourKit Java Profiler][9] and [YourKit .NET Profiler][10].

[0]: http://www.matoeil.fr/xinshi "xinshi"
[1]: https://twitter.com/XinshiXMPP "XinshiXMPP"
[2]: http://xmpp.org/rfcs/rfc6120.html "RFC 6120"
[3]: http://xmpp.org/rfcs/rfc6121.html "RFC 6121"
[4]: http://en.wikipedia.org/wiki/XMPP "XMPP"
[5]: http://www.xmpp.org/ "XMPP"
[5]: https://groups.google.com/d/forum/xinshi-developer "xinshi-developer"
[6]: https://groups.google.com/d/forum/xinshi-user "xinshi-user"
[7]: http://www.apache.org/licenses/LICENSE-2.0.txt "the Apache Software License, Version 2.0"
[8]: http://twitter.com/mbarcikowski "@mbarcikowski"
[9]: http://www.yourkit.com/java/profiler/index.jsp "YourKit Java Profiler"
[10]: http://www.yourkit.com/.net/profiler/index.jsp "YourKit .NET Profiler"
